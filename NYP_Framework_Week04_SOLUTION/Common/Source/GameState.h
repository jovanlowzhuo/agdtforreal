#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "Vector3.h"

class GameState
{
public:
	GameState() 
	{

	}
	~GameState()
	{

	}

	Vector3 minScreenAABB = Vector3(0.1, 0.1); //screen min x,y (Range is 0-1)
	Vector3 maxScreenAABB = Vector3(0.9, 0.9); //screen max x,y (Range is 0-1)
	bool CurrentScreen = false;
	bool IsMainApplication = false; //There can always only be one main application, but infinite sub-applications
	bool AllowMouseToAppear = false;

	float subScene_elevation = 0;

};

#endif