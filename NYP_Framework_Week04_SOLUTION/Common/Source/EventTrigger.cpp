#include "EventTrigger.h"

CEventTrigger::CEventTrigger()
	: EventActive(false)
{
}

CEventTrigger::~CEventTrigger()
{
}

void CEventTrigger::SetEvent(const bool EventStatus)
{
	this->EventActive = EventStatus;
}

bool CEventTrigger::GetEvent(void) const
{
	return EventActive;
}