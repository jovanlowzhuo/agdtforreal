#ifndef SCENE_H
#define SCENE_H

#include "GameState.h"

class Scene : public GameState
{
public:
	Scene() {}
	virtual ~Scene() {}

	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual void Exit() = 0;
};

#endif // SCENE_H