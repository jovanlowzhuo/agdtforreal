#ifndef EVENT_TRIGGER_H
#define EVENT_TRIGGER_H

class CEventTrigger
{
public:
	CEventTrigger();
	~CEventTrigger();

	void SetEvent(const bool EventStatus);

	bool GetEvent(void) const;

protected:
	bool EventActive;
};
#endif