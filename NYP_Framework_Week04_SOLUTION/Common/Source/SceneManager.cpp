#include "SceneManager.h"
#include "Scene.h"
//#include "../../Base/Source/Application.h"
#include <GLFW/glfw3.h>
#include "RenderHelper.h"
#include "GraphicsManager.h"
#include "MeshBuilder.h"
#include "CameraBase.h"
#include "FPSCamera.h"
#include "MouseController.h"

SceneManager::SceneManager() : activeScene(nullptr), nextScene(nullptr)
{
}

SceneManager::~SceneManager()
{
}

void SceneManager::Update(double _dt)
{
	for (std::vector<std::string>::iterator it = SubscenesToRemove.begin(); it != SubscenesToRemove.end(); ++it)
	{
		sceneMap[(*it)]->Exit();
		RemoveSubScene(*it);
	}
	SubscenesToRemove.clear();

	// Check for change of scene
	if (nextScene != activeScene)
	{
		if (activeScene)
		{
			for (std::vector<Scene*>::iterator it = SubScenes.begin(); it != SubScenes.end(); ++it)
			{
				(*it)->Exit();
			}
			SubScenes.clear();
			activeScene->Exit();
		}
		
		activeScene = nextScene;
		if (focusedScene != NULL)
			focusedScene->CurrentScreen = false;

		focusedScene = activeScene;
		focusedScene->CurrentScreen = true;
		activeScene->Init();

	}

	//Application::GetInstance().Resize(activeScene->minScreenAABB.x, activeScene->minScreenAABB.y, activeScene->maxScreenAABB.x, activeScene->maxScreenAABB.y);
	
	//glViewport(activeScene->minScreenAABB.x * windowWidth, activeScene->minScreenAABB.y * windowHeight, activeScene->maxScreenAABB.x * windowWidth, activeScene->maxScreenAABB.y * windowHeight);

	if (focusedScene != NULL)
	{

		if (activeScene)
			activeScene->Update(_dt);

		for (int i = 0; i < SubScenes.size(); ++i)
		{
			//Application::GetInstance().Resize(SubScenes[i]->minScreenAABB.x, SubScenes[i]->minScreenAABB.y, SubScenes[i]->maxScreenAABB.x, SubScenes[i]->maxScreenAABB.y);

			//glViewport(SubScenes[i]->minScreenAABB.x * windowWidth, SubScenes[i]->minScreenAABB.y * windowHeight, SubScenes[i]->maxScreenAABB.x * windowWidth, SubScenes[i]->maxScreenAABB.y * windowHeight);
			SubScenes[i]->Update(_dt);
		}

		if (focusedScene->AllowMouseToAppear)
			showcursor = true;
		else
			showcursor = false;

	}
	else
	{
		showcursor = true;

	}

}

void SceneManager::SetFocusScene(double _x, double _y)
{
	double MousePositionX = _x, MousePositionY = windowHeight - _y;
	MousePositionX /= windowWidth;
	MousePositionY /= windowHeight;

	for (int i = 0; i < SubScenes.size(); ++i)
	{
		if (MousePositionX >= SubScenes[i]->minScreenAABB.x && MousePositionY >= SubScenes[i]->minScreenAABB.y && MousePositionX <= SubScenes[i]->maxScreenAABB.x && MousePositionY <= SubScenes[i]->maxScreenAABB.y)
		{
			if (focusedScene != NULL)
				focusedScene->CurrentScreen = false;

			focusedScene = SubScenes[i];
			focusedScene->CurrentScreen = true;
			return;
		}
	}

	//std::cout << MousePositionX << " " << MousePositionY << std::endl;

	if (focusedScene != NULL)
		focusedScene->CurrentScreen = false;

	focusedScene = activeScene;
	focusedScene->CurrentScreen = true;
	return;
}

void SceneManager::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//glViewport(activeScene->minScreenAABB.x * windowWidth, activeScene->minScreenAABB.y * windowHeight, activeScene->maxScreenAABB.x * windowWidth, activeScene->maxScreenAABB.y * windowHeight);

	if (activeScene)
		activeScene->Render();

	for (int i = 0; i < SubScenes.size(); ++i)
	{
		float boxoffset = 50;
		Vector3 windowMinAABB = /*-Vector3(boxoffset, boxoffset) +*/ Vector3(SubScenes[i]->minScreenAABB.x * 1000 - 500, SubScenes[i]->minScreenAABB.y * 1000 - 500);
		Vector3 windowMaxAABB = /*Vector3(boxoffset, boxoffset) +*/ Vector3(SubScenes[i]->maxScreenAABB.x * 1000 - 500, SubScenes[i]->maxScreenAABB.y * 1000 - 500);
		Vector3 windowCenter = windowMinAABB + (windowMaxAABB - windowMinAABB) * 0.5;

		glEnable(GL_BLEND);
		RenderHelper::PreRenderMesh();
		GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, -1000, 1000);
		FPSCamera orthoCam;
		orthoCam.Init(Vector3(10, 0, 0), Vector3(-10, 0, 0), Vector3(0, 1, 0));
		GraphicsManager::GetInstance()->AttachCamera(&orthoCam);
		MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
		modelStack.PushMatrix();
		modelStack.Translate(SubScenes[i]->subScene_elevation, windowCenter.y, -windowCenter.x);
		modelStack.Rotate(90, 0, 1, 0);
		modelStack.Scale(1000 + boxoffset, 1000 + boxoffset, 1);
		modelStack.Scale(SubScenes[i]->maxScreenAABB.x - SubScenes[i]->minScreenAABB.x/* + boxoffset*/, SubScenes[i]->maxScreenAABB.y - SubScenes[i]->minScreenAABB.y /*+ boxoffset*/, 1);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("bluebox"));
		modelStack.PopMatrix();
		RenderHelper::PostRenderMesh();
		glDisable(GL_BLEND);


		//std::cout << SubScenes[i]->minScreenAABB.x * windowWidth << " " << SubScenes[i]->minScreenAABB.y * windowHeight << " " << SubScenes[i]->maxScreenAABB.x * windowWidth << " " << SubScenes[i]->maxScreenAABB.y * windowHeight << std::endl;
		
		//glViewport(SubScenes[i]->minScreenAABB.x * windowWidth, SubScenes[i]->minScreenAABB.y * windowHeight, SubScenes[i]->maxScreenAABB.x * windowWidth, SubScenes[i]->maxScreenAABB.y * windowHeight);
		
		
		SubScenes[i]->Render();
	}

}

void SceneManager::EditSubSceneElevation(const std::string& _name, const float& _elevation)
{
	sceneMap[_name]->subScene_elevation = _elevation;
}

void SceneManager::Exit()
{
	// Delete all scenes stored and empty the entire map
	for (int i = 0; i < SubScenes.size(); ++i)
	{
		SubScenes[i]->Exit();
	}
	SubScenes.clear();

	activeScene->Exit();
	activeScene = nullptr;
	std::map<std::string, Scene*>::iterator it, end;
	end = sceneMap.end();
	for (it = sceneMap.begin(); it != end; ++it)
	{
		delete it->second;
	}
	sceneMap.clear();
}

void SceneManager::AddScene(const std::string& _name, Scene* _scene)
{
	if (CheckSceneExist(_name))
	{
		// Scene Exist, unable to proceed
		throw std::exception("Duplicate scene name provided");
	}

	if (_scene == nullptr)
	{
		throw std::invalid_argument("Invalid scene pointer");
	}

	// Nothing wrong, add the scene to our map
	sceneMap[_name] = _scene;
}

void SceneManager::AddSubScene(const std::string& _name)
{
	if (!CheckSceneExist(_name))
	{
		// Scene does not exist
		throw std::exception("Scene does not exist");
	}
	
	for (int i = 0; i < SubScenes.size(); ++i)
	{
		if (sceneMap[_name] == SubScenes[i])
		{
			std::cout << "Scene of "<< _name << " already exists (note: change this such that some subscenes still can be duplicated)" << std::endl;
			return;
		}
	}

	std::cout << _name << std::endl;

	sceneMap[_name]->Init();

	if (focusedScene != NULL)
	focusedScene->CurrentScreen = false;

	focusedScene = sceneMap[_name];
	focusedScene->CurrentScreen = true;
	SubScenes.push_back(sceneMap[_name]);

}

void SceneManager::RemoveSubScene_Queue(const std::string& _name)
{
	SubscenesToRemove.push_back(_name);
}

void SceneManager::RemoveSubScene(const std::string& _name)
{
	for (std::vector<Scene*>::iterator it = SubScenes.begin(); it != SubScenes.end(); ++it)
	{
		if (sceneMap[_name] == *it) {

			if (focusedScene == sceneMap[_name])
			{
				if (focusedScene != NULL)
					focusedScene->CurrentScreen = false;

				focusedScene = activeScene;
				focusedScene->CurrentScreen = true;
			}

			SubScenes.erase(it);

			break;
		}
	}
}
void SceneManager::RemoveScene(const std::string& _name)
{
	// Does nothing if it does not exist
	if (!CheckSceneExist(_name))
		return;

	Scene* target = sceneMap[_name];
	if (target == activeScene || target == nextScene)
	{
		throw std::exception("Unable to remove active/next scene");
	}

	// Delete and remove from our map
	delete target;
	sceneMap.erase(_name);
}

void SceneManager::SetActiveScene(const std::string& _name)
{
	if (!CheckSceneExist(_name))
	{
		// Scene does not exist
		throw std::exception("Scene does not exist");
	}

	// Scene exist, set the next scene pointer to that scene
	nextScene = sceneMap[_name];
}

bool SceneManager::CheckSceneExist(const std::string& _name)
{
	return sceneMap.count(_name) != 0;
}

void SceneManager::EditSceneAABB(const std::string& _name, float w0, float h0, float w1, float h1)
{
	sceneMap[_name]->minScreenAABB.Set(w0, h0);
	sceneMap[_name]->maxScreenAABB.Set(w1, h1);
}

void SceneManager::EditSceneMouseable(const std::string& _name, const bool& canmouse)
{
	sceneMap[_name]->AllowMouseToAppear = canmouse;
}