#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include "SingletonTemplate.h"
#include <map>
#include <string>
#include <vector>

class Scene;

class SceneManager : public Singleton<SceneManager>
{
	friend Singleton<SceneManager>;
public:
	// System Interface
	void Update(double _dt);
	void Render();
	void Exit();

	// User Interface
	void AddScene(const std::string& _name, Scene* _scene);
	void RemoveScene(const std::string& _name);
	void SetActiveScene(const std::string& _name);
	bool CheckSceneExist(const std::string& _name);

	void RemoveSubScene_Queue(const std::string& _name);
	void RemoveSubScene(const std::string& _name);

	void AddSubScene(const std::string& _name);

	void EditSceneAABB(const std::string& _name, float w0, float h0, float w1, float h1);
	void EditSceneMouseable(const std::string& _name, const bool& canmouse);
	void EditSubSceneElevation(const std::string& _name, const float& _elevation);

	void SetFocusScene(double _x, double _y); //call whenever mouse is clicked, to check which scene window you are on

	int windowWidth = 0, windowHeight = 0;

	Scene* focusedScene = NULL;
	bool showcursor = true;
	int totalNormalEnemy = 0;

private:
	SceneManager();
	~SceneManager();

	std::map<std::string, Scene*> sceneMap; //upon init this is already filled up
	std::vector<Scene*> SubScenes; //cleared whenever main scene changes, think of it like separate windows in a computer
	Scene* activeScene, *nextScene;

	std::vector<std::string> SubscenesToRemove;

};

#endif // SCENE_MANAGER_H