#ifndef ENTITY_BASE_H
#define ENTITY_BASE_H

#include "Vector3.h"
#include "EventTrigger.h"

class EntityBase
{
public:

	enum EntityType
	{
		T_NORMAL,
		T_DESTRUCTIBLE,
		T_ENEMY,
		T_TARGET,
		T_TRIGGER,
		T_ALL
	};

	enum CollideDestroyCondition
	{
		CDC_NONE, //can destroy regardless of condition
		CDC_NOCHILD, //only can destroy if all child are gone

	};
	CollideDestroyCondition doneCondition = CDC_NONE;
	bool CanItBeSetToDone()
	{
		return true;
	}

	CEventTrigger* AssociatedTrigger;

	EntityBase();
	virtual ~EntityBase();

	virtual void Update(double _dt);
	virtual void Render(bool LODMap);
	virtual void RenderUI();

	inline void SetPosition(const Vector3& _value){ position = _value; };
	inline Vector3 GetPosition(){ return position; };

	inline void SetScale(const Vector3& _value){ scale = _value; };
	inline Vector3 GetScale(){ return scale; };

	bool IsDone();
	void SetIsDone(const bool _value);

	bool GetRenderCondition();
	void SetRenderCondition(const bool _value);

	void SetIsLaser(const bool _bLaser)
	{
		bLaser = _bLaser;
	}
	bool GetIsLaser() const
	{
		return bLaser;
	}

	// Check if this entity has a collider class parent
	virtual bool HasCollider(void) const;
	// Set the flag to indicate if this entity has a collider class parent
	virtual void SetCollider(const bool _value);

	EntityType typeofent = T_NORMAL;

	//float rotationAngle = 0;
	//Vector3 rotationAxis = Vector3(0, 0, 0);

protected:
	Vector3 position;
	Vector3 scale;

	bool isDone;
	bool DoNotRender;
	bool m_bCollider;
	bool bLaser = false;
};

#endif // ENTITY_BASE_H