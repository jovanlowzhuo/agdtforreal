#include "GenericEntity.h"
#include "MeshBuilder.h"
#include "EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "SceneGraph/SceneGraph.h"

GenericEntity::GenericEntity(Mesh* _modelMesh)
	: modelMesh(_modelMesh)
{
}

GenericEntity::~GenericEntity()
{
}

void GenericEntity::Update(double _dt)
{
	// Does nothing here, can inherit & override or create your own version of this class :D
}

void GenericEntity::Render(bool LODMap)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	
	if (CSceneGraph::GetInstance()->GetNode(this) == NULL)
	{
		modelStack.Translate(position.x, position.y, position.z);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Scale(scale.x, scale.y, scale.z);
	}

	if (GetLODStatus())
	{
		if (theDetailLevel != NO_DETAILS)
		{
			if (!LODMap)
			{
				RenderHelper::RenderMesh(GetLODMesh());
			}
			else if (theDetailLevel == HIGH_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("HighLOD"));
			}
			else if (theDetailLevel == MID_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("MidLOD"));
			}
			else if (theDetailLevel == LOW_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("LowLOD"));
			}
		}
	}
	else
	{
		RenderHelper::RenderMesh(modelMesh);
	}
	modelStack.PopMatrix();
}

// Set the maxAABB and minAABB
void GenericEntity::SetAABB(Vector3 maxAABB, Vector3 minAABB)
{
	this->maxAABB = maxAABB;
	this->minAABB = minAABB;
}

GenericEntity* Create::Entity(	const std::string& _meshName, 
								const Vector3& _position,
								const Vector3& _scale,
								const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	GenericEntity* result = new GenericEntity(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(false);

	// Add to Entity Manager if bAddToEntityManager==true
	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

bool GenericEntity::CanItBeSetToDone()
{
	if (doneCondition == CDC_NOCHILD)
	{
		if (CSceneGraph::GetInstance()->GetNode(this))
		{
			if (CSceneGraph::GetInstance()->GetNode(this)->childMap.size() > 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	else if (doneCondition == CDC_NONE)
	{
		return true;
	}
	else return false;
}