#ifndef SCENE_MENU_H
#define SCENE_MENU_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "SpriteEntity.h"
#include "Enemy/Enemy3D.h"
#include "HardwareAbstraction\Keyboard.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "HardwareAbstraction\Mouse.h"
#include "Collision/EventBox.h"
#include "Collision/Target.h"
#include "WaypointGraph.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneMenu : public Scene
{
public:
	SceneMenu();
	~SceneMenu();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	SceneMenu(SceneManager* _sceneMgr); // This is used to register to SceneManager

	ShaderProgram* currProg;
	FPSCamera camera;
	FPSCamera orthoCam;
	TextEntity* textObj[3];
	Light* lights[2];

	SpriteEntity* Buttons[3];

	CKeyboard* theKeyboard;
	CMouse* theMouse;
	ostringstream DisplayText;

	static SceneMenu* sInstance; // The pointer to the object that gets registered
};

#endif