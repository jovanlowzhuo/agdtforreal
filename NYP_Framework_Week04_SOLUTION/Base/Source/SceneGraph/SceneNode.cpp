#include "SceneNode.h"
#include "../EntityManager.h"
#include <algorithm>

#include "SceneGraph.h"
#include "GraphicsManager.h"
#include "../GenericEntity.h"
#include "../SpatialPartition/SpatialPartition.h"
#include "../Enemy/Enemy3D.h"

CSceneNode::CSceneNode(void)
	: ID(-1)
	, theEntity(NULL)
	, theParent(NULL)
{
}

CSceneNode::~CSceneNode()
{
}

// Release all memory for this node and its children
void CSceneNode::Destroy(void)
{
	// Destroy the children first
	//std::vector<CSceneNode*>::iterator it;
	//for (it = theChildren.begin(); it != theChildren.end(); ++it)
	//{
	//	(*it)->Destroy();
	//	delete *it;
	//	theChildren.erase(it);
	//}
	childMap.clear();
	// Reset the pointers for this node
	theEntity = NULL;
	theParent = NULL;
}

// Set the ID for this node
void CSceneNode::SetID(const int ID)
{
	this->ID = ID;
}

// Get the ID for this node
int CSceneNode::GetID(void) const
{
	return ID;
}

// Set the entity for this node
bool CSceneNode::SetEntity(EntityBase* theEntity)
{
	if (theEntity)
	{
		this->theEntity = theEntity;
		return true;
	}
	return false;
}
// Get the ID for this node
EntityBase* CSceneNode::GetEntity(void) const
{
	return theEntity;
}

// Set a parent to this node
void CSceneNode::SetParent(CSceneNode* theParent)
{
	this->theParent = theParent;
}

// Get parent of this node
CSceneNode* CSceneNode::GetParent(void) const
{
	return theParent;
}

// Add a child to this node
CSceneNode* CSceneNode::AddChild(EntityBase* theEntity)
{
	if (theEntity)
	{
		// Create a new Scene Node
		CSceneNode* aNewNode = new CSceneNode();
		// Set the entity to this new scene node
		aNewNode->SetEntity(theEntity);
		// Store the pointer to the parent
		aNewNode->SetParent(this);
		// Assign an ID to this node
		aNewNode->SetID(CSceneGraph::GetInstance()->GenerateID());

		//// Add to vector list
		//this->theChildren.push_back(aNewNode);
		childMap.insert(std::pair<EntityBase*, CSceneNode*>(theEntity, aNewNode));


		// Return this new scene node
		return aNewNode;
	}

	cout << "CSceneNode::AddChild: Unable to add to this scene node, " << this->GetID() << endl;
	return NULL;
}
// Delete a child from this node using the pointer to the entity
bool CSceneNode::DeleteChild(EntityBase* theEntity)
{
	// if this node contains theEntity, then we proceed to delete all its children
	if (this->theEntity == theEntity)
	{
		// If this node has children, then we proceed to delete them.
		if (childMap.size() != 0)
		{
			//// Iterate through all the children and delete all of their children and grand children etc
			//vector <CSceneNode*>::iterator it = theChildren.begin();
			//while (it != theChildren.end())
			//{
			//	if ((*it)->DeleteAllChildren())
			//	{
			//		cout << "CSceneNode::DeleteChild: Deleted child nodes for theEntity." << endl;
			//	}
			//	(*it)->GetEntity()->SetIsDone(true);
			//	delete *it;
			//	it = theChildren.erase(it);
			//}

			std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
			while (it != childMap.end())
			{
				cout << "xdd" << endl;
				if (it->second->DeleteAllChildren())
				{
					cout << "CSceneNode::DeleteChild: Deleted child nodes for theEntity." << endl;
				}
				it->second->GetEntity()->SetIsDone(true);
				CSpatialPartition::GetInstance()->Remove(it->second->GetEntity());
				delete it->second;
				it = childMap.erase(it);
			}
		}
		return true;	// return true to say that this Node contains theEntity
	}
	else
	{
		// Search the children for this particular theEntity
		if (childMap.size() != 0)
		{
			//std::vector<CSceneNode*>::iterator it;
			//for (it = theChildren.begin(); it != theChildren.end(); ++it)
			//{
			//	// Check if this child is the one containing theEntity
			//	if ((*it)->DeleteChild(theEntity))
			//	{
			//		// If DeleteChild method call above DID remove theEntity
			//		// Then we should proceed to removed this child from our vector of children
			//		(*it)->GetEntity()->SetIsDone(true);
			//		delete *it;
			//		theChildren.erase(it);
			//		break;	// Stop deleting since we have already found and deleted theEntity
			//	}
			//}

			for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
			{
				if (it->second->DeleteChild(theEntity))
				{
					cout << "Secondary Delete" << endl;
					it->second->GetEntity()->SetIsDone(true);
					CSpatialPartition::GetInstance()->Remove(it->second->GetEntity());
					delete it->second;
					childMap.erase(it);
					break;
				}
			}

		}
	}

	return false;
}

// Delete a child from this node using its ID
bool CSceneNode::DeleteChild(const int ID)
{
	// if this node contains theEntity, then we proceed to delete all its children
	if (this->ID == ID)
	{
		// If this node has children, then we proceed to delete them.
		if (childMap.size() != 0)
		{
			// Iterate through all the children and delete all of their children and grand children etc
			//vector <CSceneNode*>::iterator it = .begin();
			//while (it != theChildren.end())
			//{
			//	if ((*it)->DeleteAllChildren())
			//	{
			//		cout << "CSceneNode::DeleteChild: Deleted child nodes for ID=" << ID << endl;
			//	}
			//	(*it)->GetEntity()->SetIsDone(true);
			//	delete *it;
			//	it = theChildren.erase(it);
			//}


			std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
			while (it != childMap.end())
			{
				if (it->second->DeleteAllChildren())
				{
					cout << "CSceneNode::DeleteChild: Deleted child nodes for ID=" << ID << endl;
				}
				it->second->GetEntity()->SetIsDone(true);
				CSpatialPartition::GetInstance()->Remove(it->second->GetEntity());
				delete it->second;
				it = childMap.erase(it);
			}
		}
		return true;	// return true to say that this Node contains theEntity
	}
	else
	{
		// Search the children for this particular theEntity
		if (childMap.size() != 0)
		{
			//std::vector<CSceneNode*>::iterator it;
			//for (it = theChildren.begin(); it != theChildren.end(); ++it)
			//{
			//	// Check if this child is the one containing theEntity
			//	if ((*it)->DeleteChild(theEntity))
			//	{
			//		// If DeleteChild method call above DID remove theEntity
			//		// Then we should proceed to removed this child from our vector of children
			//		(*it)->GetEntity()->SetIsDone(true);
			//		delete *it;
			//		theChildren.erase(it);
			//		break;	// Stop deleting since we have already found and deleted theEntity
			//	}
			//}

			for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
			{
				if (it->second->DeleteChild(theEntity))
				{
					it->second->GetEntity()->SetIsDone(true);
					CSpatialPartition::GetInstance()->Remove(it->second->GetEntity());
					delete it->second;
					childMap.erase(it);
					break;
				}
			}

		}
	}

	return false;
}
// Delete all children from this node using its ID
bool CSceneNode::DeleteAllChildren(void)
{
	bool bResult = false;

	//vector <CSceneNode*>::iterator it = theChildren.begin();
	//while (it != theChildren.end())
	//{
	//	if ((*it)->DeleteAllChildren())
	//	{
	//		cout << "CSceneNode::DeleteChild: Delete child nodes." << endl;
	//	}
	//	(*it)->GetEntity()->SetIsDone(true);
	//	delete *it;
	//	it = theChildren.erase(it);
	//	bResult = true;
	//}

	std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
	while (it != childMap.end())
	{
		if (it->second->DeleteAllChildren())
		{
			cout << "CSceneNode::DeleteChild: Delete child nodes." << endl;
		}
		it->second->GetEntity()->SetIsDone(true);
		CSpatialPartition::GetInstance()->Remove(it->second->GetEntity());
		delete it->second;
		it = childMap.erase(it);
		bResult = true;
	}

	return bResult;
}
// Detach a child from this node using the pointer to the node
CSceneNode* CSceneNode::DetachChild(EntityBase* theEntity)
{
	if (childMap.size() != 0)
	{
		//vector <CSceneNode*>::iterator it = theChildren.begin();
		//while (it != theChildren.end())
		//{
		//	if ((*it)->GetEntity() == theEntity)
		//	{
		//		CSceneNode* theNode = (*it)->DetachChild(theEntity);
		//		if (theNode)
		//		{
		//			// Remove this node from the children
		//			theChildren.erase(it);
		//			return theNode;
		//		}
		//	}
		//	it++;
		//}
		std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
		while (it != childMap.end())
		{
			if (it->second->GetEntity() == theEntity)
			{
				CSceneNode* theNode = it->second->DetachChild(theEntity);
				if (theNode)
				{
					childMap.erase(it);
					return theNode;
				}
			}
			it++;
		}
	}
	return NULL;
}
// Detach a child from this node using its ID
CSceneNode* CSceneNode::DetachChild(const int ID)
{
	//// if it is inside this node, then return this node
	//if (this->ID == ID)
	//	return this;

	if (childMap.size() != 0)
	{
		//vector <CSceneNode*>::iterator it = theChildren.begin();
		//while (it != theChildren.end())
		//{
		//	if ((*it)->GetID() == ID)
		//	{
		//		CSceneNode* theNode = (*it)->DetachChild(ID);
		//		if (theNode)
		//		{
		//			// Remove this node from the children
		//			theChildren.erase(it);
		//			return theNode;
		//		}
		//	}
		//	it++;
		//}
		std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
		while (it != childMap.end())
		{
			if (it->second->GetID() == ID)
			{
				CSceneNode* theNode = it->second->DetachChild(ID);
				if (theNode)
				{
					childMap.erase(it);
					return theNode;
				}
			}
			it++;
		}

	}
	return NULL;
}
// Get the entity inside this Scene Graph
CSceneNode* CSceneNode::GetEntity(EntityBase* theEntity)
{
	// if it is inside this node, then return this node
	if (this->theEntity == theEntity)
		return this;

	if (childMap.size() != 0)
	{
		//std::vector<CSceneNode*>::iterator it;
		//for (it = theChildren.begin(); it != theChildren.end(); ++it)
		//{
		//	CSceneNode* theNode = (*it)->GetEntity(theEntity);
		//	if (theNode)
		//	{
		//		return theNode;
		//	}
		//}

		for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
		{
			CSceneNode* theNode = it->second->GetEntity(theEntity);
			if (theNode) return theNode;
		}

	}
	return NULL;
}
// Get a child from this node using its ID
CSceneNode* CSceneNode::GetEntity(const int ID)
{
	// if it is inside this node, then return this node
	if (this->ID == ID)
		return this;

	// Check the children
	if (childMap.size() != 0)
	{
		//std::vector<CSceneNode*>::iterator it;
		//for (it = theChildren.begin(); it != theChildren.end(); ++it)
		//{
		//	CSceneNode* theNode = (*it)->GetEntity(ID);
		//	if (theNode)
		//	{
		//		return theNode;
		//	}
		//}

		for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
		{
			CSceneNode* theNode = it->second->GetEntity(ID);
			if (theNode) return theNode;
		}

	}
	return NULL;
}
// Return the number of children in this group
int CSceneNode::GetNumOfChild(void)
{
	// Start with this node's children
	int NumOfChild = childMap.size();

	// Ask the children to feedback how many children they have
	//std::vector<CSceneNode*>::iterator it;
	//for (it = theChildren.begin(); it != theChildren.end(); ++it)
	//{
	//	NumOfChild += (*it)->GetNumOfChild();
	//}

	for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
	{
		NumOfChild += it->second->GetNumOfChild();
	}

	return NumOfChild;
}

void CSceneNode::LuaModifyFSMEnemy()
{
	if (theEntity)
	{
		//if (theEntity->typeofent == EntityBase::T_ENEMY)
		{
			CEnemy3D* tempent = dynamic_cast<CEnemy3D*>(theEntity);
			if (tempent)
			{
				tempent->ModifyFSM();
			}
		}
	}

	for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
	{
		it->second->LuaModifyFSMEnemy();
	}
}

// Update the Scene Graph
void CSceneNode::Update(const float dt)
{
	//if (theEntity != NULL)
	//{
	//	std::cout << theEntity->GetPosition() << std::endl;
	//}

	// Update the Transformation between this node and its children
	if (theUpdateTransformation)
	{
		ApplyTransform(GetUpdateTransform());
	}
	if (theEntity)
	{
		// Update this Scene Node
		theEntity->Update(dt);
	}

	//// Count the number of children in this node
	int NumOfChild = childMap.size();

	//// Update the children
	//std::vector<CSceneNode*>::iterator it;
	//for (it = theChildren.begin(); it != theChildren.end(); ++it)
	//{
	//	(*it)->Update(dt);
	//}

	for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
	{
		it->second->Update(dt);
	}
	if (GetNumOfChild() > 1)
	{
		EntityBase* temp = NULL;
		for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
		{
			if ((it->first->typeofent == EntityBase::T_TRIGGER) && (it->second->GetNumOfChild() == 0))
			{
				temp = it->first;
			}
		}
		if (temp != NULL)
		{
			DeleteChild(temp);
		}
	}


}

// Render the Scene Graph
void CSceneNode::Render(bool LODMap)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();

	//if (theEntity)
	//{
	//	modelStack.MultMatrix(GetTransform());

	//	// Render the entity
	//	theEntity->Render();
	//}

	//// Render the children
	//std::vector<CSceneNode*>::iterator it;
	//for (it = theChildren.begin(); it != theChildren.end(); ++it)
	//{
	//	(*it)->Render();
	//}


	if (theEntity)
	{
		// Update the information of this Scene Node from the Entity
		// Dot 5(not sure if needed here)
		modelStack.Translate(theEntity->GetPosition().x,
			theEntity->GetPosition().y,
			theEntity->GetPosition().z);
		modelStack.MultMatrix(GetTransform());

		//if (theParent->theParent == NULL)
		//GetTransform().PrintSelf();

	}
	// Render the children
	//std::vector<CSceneNode*>::iterator it;
	//for (it = theChildren.begin(); it != theChildren.end(); ++it)
	//{
	//	(*it)->Render();
	//}
	for (std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin(); it != childMap.end(); ++it)
	{
		it->second->Render(LODMap);
	}
	if (theEntity)
	{
		Vector3 PScale = GetPScale();
		if (theEntity->typeofent == EntityBase::T_TARGET || theEntity->typeofent == EntityBase::T_TRIGGER)
		{
			PScale = theEntity->GetScale();
		}
		modelStack.Scale(PScale.x, PScale.y, PScale.z);
		// Render the entity

		//std::cout << modelStack.Top().a[13] << std::endl;

		theEntity->Render(LODMap);
	}

	modelStack.PopMatrix();
}

// PrintSelf for debug purposes
//void CSceneNode::PrintSelf(const int numTabs)
//{
//	if (numTabs == 0)
//	{
//		cout << "============================" << endl << "Start of theRoot::PrintSelf()" << endl;
//		GetTransform().PrintSelf();
//		cout << "CSceneNode::PrintSelf: ID=" << ID << "/Children=" << theChildren.size() << endl;
//		cout << "Printing out the children:" << endl;
//		vector <CSceneNode*>::iterator it = theChildren.begin();
//		while (it != theChildren.end())
//		{
//			(*it)->PrintSelf((numTabs + 1));
//			it++;
//		}
//		cout << "End of theRoot::PrintSelf()" << endl << "============================" << endl;
//	}
//	else
//	{
//		for (int i = 0; i < numTabs; i++)
//			cout << "\t";
//		cout << "CSceneNode::PrintSelf: ID=" << ID << "/theEntity=" << theEntity <<
//			"/Children=" << theChildren.size() << endl;
//		for (int i = 0; i < numTabs; i++)
//			cout << "\t";
//		cout << "Printing out the children:" << endl;
//		vector <CSceneNode*>::iterator it = theChildren.begin();
//		while (it != theChildren.end())
//		{
//			(*it)->PrintSelf((numTabs + 1));
//			it++;
//		}
//	}
//}

void CSceneNode::ReCalc_AABB()
{
	if (!theEntity)
	{
		std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
		while (it != childMap.end())
		{
			(*it).second->ReCalc_AABB();
			++it;
		}
	}
	else if (recalculate)
	{
		Vector3 theEntityPos, theMaxAABB, theMinAABB;

		GenericEntity* theGenericEntity = dynamic_cast<GenericEntity*>(theEntity);
		theGenericEntity->GetAABB(theMaxAABB, theMinAABB);
		theEntityPos = theGenericEntity->GetPosition();

		std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
		while (it != childMap.end())
		{
			(*it).second->ReCalc_AABB(theEntityPos, theMaxAABB, theMinAABB);
			++it;
		}
		theGenericEntity->SetAABB(theMaxAABB, theMinAABB);

	}
}

void CSceneNode::ReCalc_AABB(Vector3 theParentPos, Vector3& maxAABB, Vector3& minAABB)
{
	if (theEntity)
	{
		Vector3 theEntityPos, theMaxAABB, theMinAABB;

		GenericEntity* theGenericEntity = dynamic_cast<GenericEntity*>(theEntity);
		theGenericEntity->GetAABB(theMaxAABB, theMinAABB);
		Vector3 theOffsetFromParent;
		GetTranslate(theOffsetFromParent.x, theOffsetFromParent.y, theOffsetFromParent.z);
		theEntityPos = theParentPos + theOffsetFromParent;

		if (theEntityPos.x + theMaxAABB.x > theParentPos.x + maxAABB.x)
			maxAABB.x = (theEntityPos + theMaxAABB).x - theParentPos.x;
		if (theEntityPos.y + theMaxAABB.y > theParentPos.y + maxAABB.y)
			maxAABB.y = (theEntityPos + theMaxAABB).y - theParentPos.y;
		if (theEntityPos.z + theMaxAABB.z > theParentPos.z + maxAABB.z)
			maxAABB.z = (theEntityPos + theMaxAABB).z - theParentPos.z;
		if (theEntityPos.x + theMinAABB.x < theParentPos.x + minAABB.x)
			minAABB.x = (theEntityPos + theMinAABB).x - theParentPos.x;
		if (theEntityPos.y + theMinAABB.y < theParentPos.y + minAABB.y)
			minAABB.y = (theEntityPos + theMinAABB).y - theParentPos.y;
		if (theEntityPos.z + theMinAABB.z < theParentPos.z + minAABB.z)
			minAABB.z = (theEntityPos + theMinAABB).z - theParentPos.z;

		std::map<EntityBase*, CSceneNode*>::iterator it = childMap.begin();
		while (it != childMap.end())
		{
			(*it).second->ReCalc_AABB(theEntityPos, theMaxAABB, theMinAABB);
			++it;
		}

		if (theEntityPos.x + theMaxAABB.x > theParentPos.x + maxAABB.x)
			maxAABB.x = (theEntityPos + theMaxAABB).x - theParentPos.x;
		if (theEntityPos.y + theMaxAABB.y > theParentPos.y + maxAABB.y)
			maxAABB.y = (theEntityPos + theMaxAABB).y - theParentPos.y;
		if (theEntityPos.z + theMaxAABB.z > theParentPos.z + maxAABB.z)
			maxAABB.z = (theEntityPos + theMaxAABB).z - theParentPos.z;
		if (theEntityPos.x + theMinAABB.x < theParentPos.x + minAABB.x)
			minAABB.x = (theEntityPos + theMinAABB).x - theParentPos.x;
		if (theEntityPos.y + theMinAABB.y < theParentPos.y + minAABB.y)
			minAABB.y = (theEntityPos + theMinAABB).y - theParentPos.y;
		if (theEntityPos.z + theMinAABB.z < theParentPos.z + minAABB.z)
			minAABB.z = (theEntityPos + theMinAABB).z - theParentPos.z;

		theGenericEntity->SetAABB(theMaxAABB, theMinAABB);
	}
}

