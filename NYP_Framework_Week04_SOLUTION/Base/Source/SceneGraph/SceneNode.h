#pragma once

#include "Vector3.h"
#include <vector>
#include <map>
using namespace std;

#include "EntityBase.h"
#include "Transform.h"

class CSceneNode : public CTransform
{
public:
	CSceneNode(void);
	virtual ~CSceneNode();

	// Release all memory for this node and its children
	void Destroy(void);

	// Set the ID for this node
	void SetID(const int ID);
	// Get the ID for this node
	int GetID(void) const;

	// Set the entity for this node
	bool SetEntity(EntityBase* theEntity);
	// Get the ID for this node
	EntityBase* GetEntity(void) const;

	// Set a parent to this node
	void SetParent(CSceneNode* theParent);
	// Get parent of this node
	CSceneNode* GetParent(void) const;

	// Add a child to this node
	CSceneNode* AddChild(EntityBase* theEntity = NULL);
	// Delete a child from this node using the pointer to the entity
	bool DeleteChild(EntityBase* theEntity = NULL);
	// Delete a child from this node using its ID
	bool DeleteChild(const int ID);
	// Delete all children from this node using its ID
	bool DeleteAllChildren(void);
	// Detach a child from this node using the pointer to the node
	CSceneNode* DetachChild(EntityBase* theEntity = NULL);
	// Detach a child from this node using its ID
	CSceneNode* DetachChild(const int ID);
	// Get the entity inside this Scene Graph
	CSceneNode* GetEntity(EntityBase* theEntity);
	// Get a child from this node using its ID
	CSceneNode* GetEntity(const int ID);
	// Return the number of children in this group
	int GetNumOfChild(void);

	// Update the Scene Graph
	void Update(const float dt);
	// Render the Scene Graph
	void Render(bool LODMap);

	// PrintSelf for debug purposes
	//void PrintSelf(const int numTabs = 0);

	void ReCalc_AABB();

	void GetCompleteTranslate(float& x, float& y, float& z)
	{
		float xtemp = 0, ytemp = 0, ztemp = 0;

		if (theParent != NULL)
		{
			theParent->GetCompleteTranslate(xtemp, ytemp, ztemp);
		}

		xtemp += Mtx.a[12];
		ytemp += Mtx.a[13];
		ztemp += Mtx.a[14];

		if (theEntity != NULL)
		{
			xtemp += theEntity->GetPosition().x;
			ytemp += theEntity->GetPosition().y;
			ztemp += theEntity->GetPosition().z;
		}

		x = xtemp;
		y = ytemp;
		z = ztemp;
	}

	Mtx44 TheUltimateTransformation()
	{
		Mtx44 FinalMatrix(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);

		if (theParent != NULL)
		{
			FinalMatrix = FinalMatrix * theParent->TheUltimateTransformation();
		}

		FinalMatrix = FinalMatrix * GetTransform();

		if (theEntity != NULL)
		{
			FinalMatrix.a[12] += theEntity->GetPosition().x;
			FinalMatrix.a[13] += theEntity->GetPosition().y;
			FinalMatrix.a[14] += theEntity->GetPosition().z;
		}

		return FinalMatrix;
	}
	std::map<EntityBase*, CSceneNode*> childMap;

	bool recalculate = true;

	void LuaModifyFSMEnemy();

protected:

	void ReCalc_AABB(Vector3 theParentPos, Vector3& maxAABB, Vector3& minAABB);

	int			ID;
	EntityBase* theEntity;
	CSceneNode* theParent;

	//vector<CSceneNode*> theChildren;
	//std::map<EntityBase*, CSceneNode*> childMap;


};
