#include "LaserBlaster.h"
#include "../Projectile/Laser.h"
#include "MeshBuilder.h"
#include "../EntityManager.h"

CLaserBlaster::CLaserBlaster()
{
}


CLaserBlaster::~CLaserBlaster()
{
}

// Initialise this instance to default values
void CLaserBlaster::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 5000;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 5000;
	// The current total number of rounds currently carried by this player
	totalRounds = 10000;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 10000;

	// The time between shots
	timeBetweenShots = 0.1667;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
}

// Discharge this weapon
void CLaserBlaster::Discharge(Vector3 position, Vector3 target, CPlayerInfo* _source)
{
	if (bFire)
	{
		// If there is still ammo in the magazine, then fire
		if (magRounds > 0)
		{
			Vector3 _direction = (target - position).Normalized();
			// Create a laser with a laser mesh. The length is 10.0f, mesh is also set at 10.0f
			CLaser* aLaser = Create::Laser("laser",
				position,
				_direction,
				1.0f,		// Length of laser
				5.0f,		// Life of laser
				200.0f,		// Speed of laser
				_source);
			aLaser->SetIsLaser(true);
			aLaser->SetLength(10.0f);
			aLaser->SetCollider(true);
			aLaser->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
			//aLaser->SetAABB(Vector3(1.0f, 1.0f, 1.0f), Vector3(-1.0f, -1.0f, -1.0f));
			bFire = false;
			magRounds--;
		}
	}
}
