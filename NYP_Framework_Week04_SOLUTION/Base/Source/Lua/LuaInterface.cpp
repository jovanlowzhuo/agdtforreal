#include "LuaInterface.h"
#include <fstream> 
#include <vector>

//Allocating & initialising CLuaInterface's static data member
//The pointer is allocated but not the object's constructor
CLuaInterface *CLuaInterface::s_instance = 0;

CLuaInterface::CLuaInterface()
	:theLuaState(nullptr)
{
	theLuaState = luaL_newstate();
}

CLuaInterface::~CLuaInterface()
{
}

CLuaInterface* CLuaInterface::GetInstance()
{
	if (!s_instance)
	{
		s_instance = new CLuaInterface;
	}

	return s_instance;
}

bool CLuaInterface::DropInstance()
{
	if (s_instance)
	{
		//Drop the Lua Interface class
		s_instance->DropAll();

		delete s_instance;
		s_instance = nullptr;
		return true;
	}
	return false;
}

bool CLuaInterface::Init()
{
	bool result = false;

	//Create the lua state
	theLuaState = lua_open();
	if (theLuaState)
	{
		//Load lua auxiliary libraries
	//	luaL_openlibs(theLuaState);

		//Load lua scripts
		luaL_dofile(theLuaState, "Image//SP3.lua");

		CurrentFileName = "Image//SP3.lua";

		result = true;
	}

	theErrorState = lua_open();
	if (theLuaState && theErrorState)
	{
		//Load lua auxiliary libraries
	//	luaL_openlibs(theLuaState);

		//Load the error lua script
		luaL_dofile(theErrorState, "Image//errorLookup.lua");
	}
	return result;
}

void CLuaInterface::Run()
{
	if (!theLuaState)
	{
		return;
	}

	//Read the variables
	lua_getglobal(theLuaState, "title");
	//Extract value, index -1 as variable is already retrieved using lua_getglobal
	const char *title = lua_tostring(theLuaState, -1);

	lua_getglobal(theLuaState, "width");
	int width = lua_tointeger(theLuaState, -1);

	lua_getglobal(theLuaState, "height");
	int height = lua_tointeger(theLuaState, -1);

	//Display on screen
	cout << title << "\n";
	cout << "-------------------------------------------------\n";
	cout << "Width of screen: " << width << "\n";
	cout << "Height of screen: " << height << "\n";
}

void CLuaInterface::Drop(lua_State* &luaStateToDrop)
{
	if (luaStateToDrop)
	{
		if (luaStateToDrop == theLuaState)
		{
			CurrentFileName.clear();
		}
		//close Lua State
//		lua_close(luaStateToDrop);
		luaStateToDrop = nullptr;
	}
}

void CLuaInterface::DropAll()
{
	if (theLuaState)
	{
		CurrentFileName.clear();
		lua_close(theLuaState);
		theLuaState = nullptr;
	}

	if (theErrorState)
	{
		lua_close(theErrorState);
		theErrorState = nullptr;
	}
}

int CLuaInterface::getIntValue(const string &Name)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return -1;
	}

	lua_getglobal(theLuaState, Name.c_str());
	return lua_tointeger(theLuaState, -1);
}

float CLuaInterface::getFloatValue(const string &Name)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return -1;
	}

	lua_getglobal(theLuaState, Name.c_str());
	return (float)lua_tonumber(theLuaState, -1);
}

char CLuaInterface::getCharValue(const string &Name)
{
	lua_getglobal(theLuaState, Name.c_str());

	size_t length;
	const string string = lua_tolstring(theLuaState, -1, &length);

	//If the string is not empty, then return the first char
	if (length > 0)
	{
		return string[0];
	}
	else //else return a default value of a white space
	{
		return ' ';
	}
}

Vector3 CLuaInterface::getVector3Values(const string &Name)
{
	//Find the name in lua file
	lua_getglobal(theLuaState, Name.c_str());

	//Extract the first value from the variable container and places it into the lua stack
	//The third parameter of rawgeti specifies the index of the container (starts from index 1, not 0)
	lua_rawgeti(theLuaState, -1, 1);
	//Assign the value which is at the top of stack to Pos x
	int x = (int)lua_tonumber(theLuaState, -1);
	//Pop the value from lua stack
	lua_pop(theLuaState, 1);
	lua_rawgeti(theLuaState, -1, 2);
	int y = (int)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
	lua_rawgeti(theLuaState, -1, 3);
	int z = (int)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);

	return Vector3((float)x, (float)y, (float)z);
}

float CLuaInterface::getDistanceSquareValue(const string & Name, const Vector3 & source, const Vector3 & destination)
{
	lua_getglobal(theLuaState, Name.c_str());

	//Push position vertices onto lua stack
	lua_pushnumber(theLuaState, source.x);
	lua_pushnumber(theLuaState, source.y);
	lua_pushnumber(theLuaState, source.z);

	lua_pushnumber(theLuaState, destination.x);
	lua_pushnumber(theLuaState, destination.y);
	lua_pushnumber(theLuaState, destination.z);

	//Calls a function
	//lua_call 2nd Parameter -> The number of arguments pushed onto stack 
	//and are popped automatically after being passed as arguments
	//3rd parameter -> the number of results
	//The returned value is pushed onto the stack after function call
	lua_call(theLuaState, 6, 1);

	//Take the retured value from the stack and assign it to local variable
	const float distanceSquared = (float)lua_tonumber(theLuaState, -1);
	//Pop the value from stack
	lua_pop(theLuaState, 1);

	return distanceSquared;
}

float CLuaInterface::getFieldFloat(const char * key)
{
	//Error Check - whether the variables in the stack belong to a table
	if (!lua_istable(theLuaState, -1))
	{
		error("error100");
		return 0;
	}

	lua_pushstring(theLuaState, key);
	lua_gettable(theLuaState, -2);

	//Error check - returned value is not a number
	if (!lua_isnumber(theLuaState, -1))
	{
		error("error101");
		return 0;
	}

	const float result = static_cast<float>(lua_tonumber(theLuaState, -1));
	lua_pop(theLuaState, 1);

	return result;
}

string CLuaInterface::getFieldString(const char * key)
{
	//Error Check - whether the variables in the stack belong to a table
	if (!lua_istable(theLuaState, -1))
	{
		error("error100");
		return "";
	}

	lua_pushstring(theLuaState, key);
	lua_gettable(theLuaState, -2);

	std::string result = lua_tostring(theLuaState, -1);
	lua_pop(theLuaState, 1);

	return result;
}

Vector3 CLuaInterface::getFieldVector(const char * key)
{
	//Error Check - whether the variables in the stack belong to a table
	if (!lua_istable(theLuaState, -1))
	{
		error("error100");
		return 0;
	}
	lua_pushstring(theLuaState, key);	
	lua_gettable(theLuaState, -2);		// jumps to the defined array inside the table,

	if (lua_isnil(theLuaState, -1)) { // array is not found in the defined array area,
		return 1.0f;
	}

	std::vector<float> v;
	lua_pushnil(theLuaState);	// pushing all the values of the defined array into the stack

	while (lua_next(theLuaState, -2)) {		// if the stack becomes empty etc
		/* ADD CODES HERE */
		v.push_back((float)lua_tonumber(theLuaState, -1));
		lua_pop(theLuaState, 1);
	}
	lua_pop(theLuaState, 1);

	Vector3 result;
	result.Set(v[0], v[1], v[2]);
	
	//lua_

	return result;
}

string CLuaInterface::getStringValue(const string & Name)
{
	lua_getglobal(theLuaState, Name.c_str());

	printf("%s\n", CurrentFileName);

	const string string = lua_tostring(theLuaState, -1);

	return string;
}

void CLuaInterface::saveIntValue(string Name, const int &value, const string &FunctionName, const bool &overwrite)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return;
	}

	lua_getglobal(theLuaState, FunctionName.c_str());
	Name += " = " + to_string(value) + '\n';
	lua_pushstring(theLuaState, Name.c_str());
	lua_pushinteger(theLuaState, overwrite);
	lua_call(theLuaState, 2, 0);
}

void CLuaInterface::saveFloatValue(string Name, const float &value, const bool &overwrite)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return;
	}

	lua_getglobal(theLuaState, "SaveToLuaFile");
	Name += " = " + to_string(value) + '\n';
	lua_pushstring(theLuaState, Name.c_str());
	lua_pushinteger(theLuaState, overwrite);
	lua_call(theLuaState, 2, 0);
}

void CLuaInterface::saveStringValue(string Name, const string & value, const string &FunctionName, const bool & overwrite)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return;
	}

	lua_getglobal(theLuaState, FunctionName.c_str());
	Name += "=";
	Name.push_back('"');
	Name += value;
	Name.push_back('"');
	Name.push_back('\n');

	lua_pushstring(theLuaState, Name.c_str());
	lua_pushinteger(theLuaState, overwrite);
	lua_call(theLuaState, 2, 0);
}

void CLuaInterface::saveVector3Value(string Name, const Vector3 & positionVector, const string & FunctionName, const bool & overwrite)
{
	if (!theLuaState)
	{
		cout << "Lua State does not exist!\n";
		return;
	}

	lua_getglobal(theLuaState, FunctionName.c_str());

	Name += " = {";
	Name += to_string((int)positionVector.x) + ',';
	Name += to_string((int)positionVector.y) + ',';
	Name += to_string((int)positionVector.z) + "}\n";

	lua_pushstring(theLuaState, Name.c_str());
	lua_pushinteger(theLuaState, overwrite);
	lua_call(theLuaState, 2, 0);
}

Vector3 CLuaInterface::GetNearestWaypoint_Lua(const Vector3& _pos)
{
	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_WAYPOINTDATA.lua", CLuaInterface::GetInstance()->theLuaState);

	lua_getglobal(theLuaState, "LuaGetNearestWaypoint");

	//Name += " = {";
	//Name += to_string((int)positionVector.x) + ',';
	//Name += to_string((int)positionVector.y) + ',';
	//Name += to_string((int)positionVector.z) + "}\n";

	//lua_pushstring(theLuaState, Name.c_str());
	//lua_pushinteger(theLuaState, overwrite);
	lua_pushinteger(theLuaState, _pos.x);
	lua_pushinteger(theLuaState, _pos.y);
	lua_pushinteger(theLuaState, _pos.z);
	lua_call(theLuaState, 3, 1);

	const std::string stringvector = lua_tostring(theLuaState, -1);
	std::cout << stringvector << std::endl;
	Vector3 _vectoroutput = Vector3();
	std::string throwawaystring = "";
	int whichdimen = 0;
	for (int i = 0; i < stringvector.size(); ++i)
	{
		if (stringvector[i] == '|')
		{
			if (whichdimen == 0)
			{
				_vectoroutput.x = std::stof(throwawaystring);
			}
			else if (whichdimen == 1)
			{
				_vectoroutput.y = std::stof(throwawaystring);
			}
			else if (whichdimen == 2)
			{
				_vectoroutput.z = std::stof(throwawaystring);
			}
			throwawaystring = "";

			whichdimen++;
		}
		else
		{
			//std::cout << stringvector[i] << std::endl;
			throwawaystring += stringvector[i];
			if (i == stringvector.size() - 1 && whichdimen == 2)
			{
				//std::cout << throwawaystring << std::endl;
				_vectoroutput.z = std::stof(throwawaystring);
			}
		}
	}
	//std::cout << _vectoroutput << std::endl;
	lua_pop(theLuaState, 1);

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);

	return _vectoroutput;
}

void CLuaInterface::error(const string & errorCode)
{
	if (!theErrorState)
	{
		return;
	}

	lua_getglobal(theErrorState, errorCode.c_str());

	const string errorMsg = lua_tostring(theErrorState, -1);

	if (!errorMsg.empty())
	{
		printf_s("%s\n", errorMsg.c_str());
	}
	else
	{
		printf_s("Error Code: %s is an unkown error!\n", errorCode.c_str());
	}
}

#include <assert.h>
void CLuaInterface::SetLuaFile(const string &NewLuaFileName, lua_State* &luaState)
{
//	assert(luaState != nullptr); //The lua state passed in is a null object

	//Close the lua interface
//	Drop(luaState);

	//Create the lua state
	luaState = lua_open();

	if (luaState)
	{
		//Load lua auxiliary libraries
		luaL_openlibs(luaState);

		//Load lua scripts
		luaL_dofile(luaState, NewLuaFileName.c_str());

		if (luaState == theLuaState)
		{
			CurrentFileName = NewLuaFileName;
		}
	}
}

string CLuaInterface::GetTheLuaStateCurrentFilename()
{
	return CurrentFileName;
}

void CLuaInterface::WriteVector(string Name, const Vector3 & positionVector, const string & FunctionName, const bool & overwrite)
{
	Name += " = {";
	Name += to_string((int)positionVector.x) + ',';
	Name += to_string((int)positionVector.y) + ',';
	Name += to_string((int)positionVector.z) + "}\n";

	if (overwrite == 1)
	{
		std::ofstream ofs;
		ofs.open(GetTheLuaStateCurrentFilename(), std::ofstream::out | std::ofstream::trunc);
		ofs << Name;
		ofs.close();
	}
	else if (overwrite == 0)
	{
		std::ofstream ofs;
		ofs.open(GetTheLuaStateCurrentFilename(), std::ofstream::out | std::ofstream::app);
		ofs << Name;
		ofs.close();
	}
}

void CLuaInterface::WriteInt(string Name, const int &value, const string &FunctionName, const bool &overwrite)
{
	Name += " = " + to_string(value) + '\n';
	if (overwrite == 1)
	{
		std::ofstream ofs;
		ofs.open(GetTheLuaStateCurrentFilename(), std::ofstream::out | std::ofstream::trunc);
		ofs << Name;
		ofs.close();
	}
	else if (overwrite == 0)
	{
		std::ofstream ofs;
		ofs.open(GetTheLuaStateCurrentFilename(), std::ofstream::out | std::ofstream::app);
		ofs << Name;
		ofs.close();
	}
}