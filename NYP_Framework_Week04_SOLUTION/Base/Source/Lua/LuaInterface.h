#pragma once
//Include lua headers
#include "lua.hpp"
#include "lua.h"
#include "luaconf.h"
#include "lualib.h"
#include <string>
#include "Vector3.h"
#include <iostream>
using namespace std;

class CLuaInterface
{
protected:
	string CurrentFileName;

public:
	static CLuaInterface *GetInstance();
	static CLuaInterface *s_instance;
	static bool DropInstance();

	CLuaInterface();
	virtual ~CLuaInterface();

	Vector3 GetNearestWaypoint_Lua(const Vector3& _pos);

	//Init Lua Interface Class
	bool Init();

	//Run Lua Interface Class
	void Run();

	string GetFileName() { return CurrentFileName; }
	//Drop a specific lua interface
	void Drop(lua_State* &luaStateToDrop);
	//Drop all interfaces
	void DropAll();

	int getIntValue(const string &Name);
	float getFloatValue(const string &Name);
	char getCharValue(const string &Name);
	Vector3 getVector3Values(const string &Name);
	float getDistanceSquareValue(const string &Name, const Vector3 &source, const Vector3 &destination);
	float getFieldFloat(const char *key);
	string getFieldString(const char *key);
	Vector3 getFieldVector(const char * key);
	string getStringValue(const string &Name);

	void saveIntValue(string Name, const int &value, const string &FunctionName, const bool &overwrite = false);
	void saveFloatValue(string Name, const float &value, const bool &overwrite = false);
	void saveStringValue(string Name, const string &value, const string &FunctionName, const bool &overwrite = false);

	void saveVector3Value(string Name, const Vector3 &positionVector, const string &FunctionName, const bool &overwrite = false);

	void error(const string &errorCode);
	void SetLuaFile(const string &NewLuaFileName, lua_State* &luaState);

	string GetTheLuaStateCurrentFilename();

	void WriteVector(string Name, const Vector3 & positionVector, const string & FunctionName, const bool & overwrite);
	void WriteInt(string Name, const int & value, const string & FunctionName, const bool & overwrite);
	
	//Pointers to the Lua State
	lua_State *theErrorState;
	lua_State *theLuaState;
};