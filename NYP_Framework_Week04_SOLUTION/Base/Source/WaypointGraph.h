#ifndef GRAPH_H
#define GRAPH_H

#include "Vector3.h"
#include <vector>
#include <list>
#include <map>
#include "SingletonTemplate.h"

struct Edge
{
	Edge()
	{

	};

	std::pair<int, Vector3> from; //int for ID
	std::pair<int, Vector3> to; //int for ID
	float cost;
};

struct Node
{
	Node(Vector3 _pos)
	{
		pos = _pos;
	};

	Vector3 pos;

	//clear but do not delete
	std::vector<Edge*> edges;

	//do not delete
	int nextNode = 0;
};

class Graph : public Singleton<Graph>
{
	friend Singleton<Graph>;
public:
	std::map<int, Node*> m_nodes; //delete all that
	std::vector<Edge*> m_edges; //delete all that
	Graph();
	virtual ~Graph();
	void AddNode(Vector3 pos);
	void AddNode_File(int ID, Node* _node);
	void AddEdge(std::pair<int, Node*> start, std::pair<int, Node*> end);
	void Generate(unsigned key, int allNodes, int totalEdgesPerNode);

	void LoadCSV(const std::string filename);

	//should add more things
	Node* GetNearestNode(const Vector3& objPos)
	{
		Node* returnNode = NULL;
		for (std::map<int, Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
		{
			if (returnNode == NULL)
			{
				returnNode = (*it).second;
			}
			else
			{
				if (((*it).second->pos - objPos).Length() < (returnNode->pos - objPos).Length())
				{
					//std::cout << "the " << std::endl;
					returnNode = (*it).second;
				}
			}
		}
		
		return returnNode;
	}

	std::vector<Vector3> PathFinding(const Vector3& curr, const Vector3& destination); //returns a vector path of where to head to

	void Generate_Lua();

};

#endif