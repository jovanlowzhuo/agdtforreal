#ifndef SCENE_TEXT2_H
#define SCENE_TEXT2_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "Enemy/Enemy3D.h"
#include "HardwareAbstraction\Keyboard.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "HardwareAbstraction\Mouse.h"
#include "Collision/EventBox.h"
#include "Collision/Target.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneText2 : public Scene
{
public:
	SceneText2();
	~SceneText2();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	SceneText2(SceneManager* _sceneMgr); // This is used to register to SceneManager

	ShaderProgram* currProg;
	CPlayerInfo* playerInfo;
	GroundEntity* groundEntity;
	FPSCamera camera;
	FPSCamera orthoCam;
	ostringstream DisplayText;
	TextEntity* textObj[3];
	Light* lights[2];

	CKeyboard* theKeyboard;
	CMouse* theMouse;
	CMinimap* theMinimap;
	CCameraEffects* theCameraEffects;

	CEnemy3D* anEnemy3D_1;

	CEventBox* theEventTrigger;			// Event Trigger

	GenericEntity* theCube;
	//	CEnemy3D* anEnemy3D;	// This is the CEnemy class for 3D use.

	static SceneText2* sInstance; // The pointer to the object that gets registered

	bool showQuadtreemap = false;

	bool loadShader = false;
};

#endif