#include "SceneText.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"
#include "Lua/LuaInterface.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include "ShopThings.h"

#include <iostream>
using namespace std;

SceneText* SceneText::sInstance = new SceneText(SceneManager::GetInstance());

SceneText::SceneText()
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
}

SceneText::SceneText(SceneManager* _sceneMgr)
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Start", this);
}

SceneText::~SceneText()
{
	//if (theCameraEffects)
	//{
	//	delete theCameraEffects;
	//	theCameraEffects = NULL;
	//}
	///*if (theMinimap)
	//{
	//	delete theMinimap;
	//	theMinimap = NULL;
	//}*/
	if (theMouse)
	{
		delete theMouse;
		theMouse = NULL;
	}
	if (theKeyboard)
	{
		delete theKeyboard;
		theKeyboard = NULL;
	}
	// Delete the scene graph
	CSceneGraph::GetInstance()->Destroy();
	// Delete the Spatial Partition
	CSpatialPartition::GetInstance()->Destroy();
	EntityManager::GetInstance()->Destroy();
}

void SceneText::Init()
{
	//srand(NULL);

	currProg = GraphicsManager::GetInstance()->GetActiveShader();

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("textEnabled", 0);
	
	// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();

	// Create and attach the camera to the scene
	//camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	fakeorthoCam.Init(Vector3(0, 0, 0), Vector3(0, 0, -1), Vector3(0, 1, 0));
	//orthoCam.Init(Vector3(0, 10, 0), Vector3(0, 15, 10), Vector3(0, 1, 0));
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Set up the Spatial Partition and pass it to the EntityManager to manage
	//CSpatialPartition::GetInstance()->Init(100, 100, 10, 10);
	CSpatialPartition::GetInstance()->Init(1000, 1000, 1, 1);
	CSpatialPartition::GetInstance()->SetMeshRenderMode(CGrid::FILL);
	CSpatialPartition::GetInstance()->SetMesh("GRIDMESH");
	CSpatialPartition::GetInstance()->SetCamera(&camera);
	CSpatialPartition::GetInstance()->SetLevelOfDetails(powf(150, 2), powf(400, 2)); //these are squared values of actual distance threshold

	// Create entities into the scene
	//Create::Entity("reference", Vector3(0.0f, 0.0f, 0.0f)); // Reference
	//CSceneNode* testNode = CSceneGraph::GetInstance()->AddNode(Create::Entity("lightball", Vector3(lights[0]->position.x, lights[0]->position.y + 25, lights[0]->position.z), Vector3(), false));
	
	//GenericEntity* aCube = Create::Entity("cube", Vector3(-20.0f, 0.0f, -20.0f));
	Create::Entity("ring", Vector3(0.0f, 0.0f, 0.0f)); // Reference

	//GenericEntity* fixedObj = Create::Entity("Chair", Vector3(-20.0f, 0.0f, 80.0f), Vector3(1, 1, 1), false); // Reference
	//fixedObj->SetCollider(true);
	//fixedObj->SetAABB(Vector3(15, 20, 15), Vector3(-15, -20, -15));
	//fixedObj->typeofent = EntityBase::T_DESTRUCTIBLE;
	////NOTE : ADD INTO SCENE GRAPH
	//CSpatialPartition::GetInstance()->Add(fixedObj);
	//CSceneNode* chairNode = CSceneGraph::GetInstance()->AddNode(fixedObj);

	groundEntity = Create::Ground("GRASS_DARKGREEN", "GEO_GRASS_LIGHTGREEN");
//	Create::Text3DObject("text", Vector3(0.0f, 0.0f, 0.0f), "DM2210", Vector3(10.0f, 10.0f, 10.0f), Color(0, 1, 1));
	Create::Sprite2DObject("crosshair", Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f));

	SkyBoxEntity* theSkyBox = Create::SkyBox("SKYBOX_FRONT", "SKYBOX_BACK",
											 "SKYBOX_LEFT", "SKYBOX_RIGHT",
											 "SKYBOX_TOP", "SKYBOX_BOTTOM");

	// Customise the ground entity
	groundEntity->SetPosition(Vector3(0, -10, 0));
	groundEntity->SetScale(Vector3(100.0f, 100.0f, 100.0f));
	groundEntity->SetGrids(Vector3(10.0f, 1.0f, 10.0f));
	playerInfo->SetTerrain(groundEntity);

	// Create entities such as NPC etc
	this->CreateEntities();
	//this->CreateMantis();
	CreateBoss();
	CreateStaticSpider();

	// Setup the 2D entities
	float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	float fontSize = 25.0f;
	float halfFontSize = fontSize / 2.0f;
	for (int i = 0; i < 3; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight + fontSize*i + halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f,1.0f,0.0f));
	}
	textObj[0]->SetText("HELLO WORLD");

	// Activate the Blend Function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Minimap
	theMinimap = Create::Minimap(false);
	theMinimap->SetBackground(MeshBuilder::GetInstance()->GetMesh("MINIMAP"));
	theMinimap->SetBorder(MeshBuilder::GetInstance()->GetMesh("MINIMAPBORDER"));
	theMinimap->SetAvatar(MeshBuilder::GetInstance()->GetMesh("MINIMAPAVATAR"));
	theMinimap->SetStencil(MeshBuilder::GetInstance()->GetMesh("MINIMAP_STENCIL"));

	// CameraEffects
	theCameraEffects = Create::CameraEffects(false);
	theCameraEffects->SetBloodScreen(MeshBuilder::GetInstance()->GetMesh("CAMERAEFFECTS_BLOODSCREEN"));
	//theCameraEffects->GetBloodScreen()->textureID = LoadTGA("Image//CameraEffects_Blood.tga");
	theCameraEffects->SetStatus_BloodScreen(false);

	theCameraEffects->SetScopeScreen(MeshBuilder::GetInstance()->GetMesh("CAMERAEFFECTS_SCOPESCREEN"));
	//theCameraEffects->GetScopeScreen()->textureID = LoadTGA("Image//scope.tga");
	theCameraEffects->SetStatus_ScopeScreen(false);

	//Vector3 HorizontalTrigger(45.0f, 20.0f, 10.0f);
	//theEventTrigger = Create::EventBox("cube", Vector3(10.0f, -5.0f, -10.0f), HorizontalTrigger, false);
	//theEventTrigger->SetRenderCondition(true);
	//anEnemy3D_1 = Create::Enemy3D("cube", Vector3(15.0f, -20.0f, -10.0f), theEventTrigger->AssociatedTrigger, groundEntity, Vector3(5.0f, 5.0f, 5.0f), false);
	//CSceneNode* enemyNode = CSceneGraph::GetInstance()->AddNode(anEnemy3D_1);
	//CSceneNode* eventNode = enemyNode->AddChild(theEventTrigger);
	//CSpatialPartition::GetInstance()->Add(anEnemy3D_1);

	Vector3 Offset(50.0f, 0, 50.0f);
	Vector3 VerticalTarget(1.0f, 10.0f, 10.0f);
	Vector3 HorizontalTarget(10.0f, 10.0f, 1.0f);
	Vector3 VerticalTrigger(10.0f, 20.0f, 45.0f);
	Vector3 HorizontalTrigger(45.0f, 20.0f, 10.0f);
	Vector3 MoveVerticalMin(0, 0, -20);
	Vector3 MoveVerticalMax(0, 0, 20);
	Vector3 MoveHorizontalMin(-20, 0, 0);
	Vector3 MoveHorizontalMax(20, 0, 0);

	//theEventTrigger = Create::EventBox("Target", Vector3(25.0f, -5.0f, -200.0f), HorizontalTrigger);
	//theEventTrigger->SetAABB(HorizontalTrigger, -HorizontalTrigger);
	//theEventTrigger->SetCollider(true);
	//CTarget* tempTest;
	//tempTest = Create::Target("Target", Vector3(75.0f, -20.0f, -175.0f), theEventTrigger->AssociatedTrigger, groundEntity, MoveVerticalMin, MoveVerticalMax, false, VerticalTarget);
	//tempTest->SetAABB(VerticalTarget, -VerticalTarget);
	//tempTest->SetCollider(true);
	//CSceneNode* eventNode = CSceneGraph::GetInstance()->AddNode(theEventTrigger);
	//eventNode->recalculate = false;
	//CSceneNode* enemyNode = eventNode->AddChild(tempTest);
	//enemyNode->recalculate = false;
	//CSpatialPartition::GetInstance()->Add(theEventTrigger);
	//CSpatialPartition::GetInstance()->Add(tempTest);

	// Hardware Abstraction
	theKeyboard = new CKeyboard();
	theKeyboard->Create(playerInfo);

	theMouse = new CMouse();
	theMouse->Create(playerInfo);

	Graph::GetInstance()->Generate_Lua();
	//Graph::GetInstance()->LoadCSV("Image//graph.csv");

	////Graph::GetInstance()->Generate(0, 50, 8);
	//std::vector<Node*> test = Graph::GetInstance()->PathFinding(Vector3(0, 0, 0), Vector3(0, 90, 0));
	////if (test.size() > 0)
	////{
	////	for (int i = 0; i < test.size(); ++i)
	////	{
	////		std::cout << test[i]->pos << std::endl;
	////	}
	////}
}

//void SceneText::Update(double dt)
void SceneText::Update(double _dt)
{
	double dt = _dt;
	//if (!CurrentScreen)
	//{
	//	dt = 0;
	//}
	// Update our entities
	EntityManager::GetInstance()->Update(dt);
	
	// THIS WHOLE CHUNK TILL <THERE> CAN REMOVE INTO ENTITIES LOGIC! Or maybe into a scene function to keep the update clean
	if(KeyboardController::GetInstance()->IsKeyDown('1'))
		glEnable(GL_CULL_FACE);
	if(KeyboardController::GetInstance()->IsKeyDown('2'))
		glDisable(GL_CULL_FACE);
	if(KeyboardController::GetInstance()->IsKeyDown('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if(KeyboardController::GetInstance()->IsKeyDown('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//if(KeyboardController::GetInstance()->IsKeyDown('5'))
	//{
	//	lights[0]->type = Light::LIGHT_POINT;
	//}
	//else if(KeyboardController::GetInstance()->IsKeyDown('6'))
	//{
	//	lights[0]->type = Light::LIGHT_DIRECTIONAL;
	//}
	//else if(KeyboardController::GetInstance()->IsKeyDown('7'))
	//{
	//	lights[0]->type = Light::LIGHT_SPOT;
	//}

	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyPause))
	{
		//if (CurrentScreen)
		SceneManager::GetInstance()->AddSubScene("Pause");
	}

	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyShop))
	{
		//if (CurrentScreen)
		SceneManager::GetInstance()->AddSubScene("Shop");
	}

	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyScene2On))
	{
		////if (CurrentScreen)
		SceneManager::GetInstance()->AddSubScene("Start2");
	}
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyCreateBoss))
		CreateBoss();
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyCreateEnemies))
	{
		if (true)
		{
			for (int i = 0; i < 20; ++i)
			{
				CEnemy3D* anEnemy3D = Create::Enemy3D("cube", Vector3(Math::RandFloatMinMax(-250, 250), 0.0f, Math::RandFloatMinMax(-250, 250)), Vector3(1.0f, 1.0f, 1.0f), false);
				anEnemy3D->InitLOD("Enemy_HighLOD", "Enemy_MidLOD", "Enemy_LowLOD");
				anEnemy3D->Init();
				//anEnemy3D->SetPos(Vector3(0, 0, 0));
				anEnemy3D->SetSpeed(4.0);
				anEnemy3D->SetCollider(true);
				anEnemy3D->SetAABB(Vector3(10, 10, 10), Vector3(-10, -1.5, -10));


				anEnemy3D->SetTerrain(groundEntity);

				// Add the entity into the Spatial Partition
				CSpatialPartition::GetInstance()->Add(anEnemy3D);

				CSceneNode* pNPCSceneNode = CSceneGraph::GetInstance()->AddNode(anEnemy3D);
				pNPCSceneNode->SetTranslate(Vector3(0.0f, 0.0f, 0.0f));
			}
		}

		CSceneGraph::GetInstance()->ReCalc_AABB();
	}
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyToggleSpeed)) //toggle enemy movement speed
	{
		if (CSpatialPartition::GetInstance()->EnemySpeedMultiplier == -1)
		{
			CSpatialPartition::GetInstance()->EnemySpeedMultiplier = 1;
		}
		else if (CSpatialPartition::GetInstance()->EnemySpeedMultiplier == 1)
		{
			CSpatialPartition::GetInstance()->EnemySpeedMultiplier = 0;
		}
		else if (CSpatialPartition::GetInstance()->EnemySpeedMultiplier == 0)
		{
			CSpatialPartition::GetInstance()->EnemySpeedMultiplier = -1;
		}
	}
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyToggleTopView)) //toggle top-down view
		if (ShopThings::GetInstance()->ScannerBought)
		showQuadtreemap = !showQuadtreemap;
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyToggleQuadtree)) //toggle quadtree
		if (ShopThings::GetInstance()->ScannerBought)
		CSpatialPartition::GetInstance()->quadTreeEnable = !CSpatialPartition::GetInstance()->quadTreeEnable;
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyToggleQuadMarker)) //toggle quadtree node indicators
		if (ShopThings::GetInstance()->ScannerBought)
		CSpatialPartition::GetInstance()->orangeTileEnable = !CSpatialPartition::GetInstance()->orangeTileEnable;
	//if (KeyboardController::GetInstance()->IsKeyPressed('O')) //toggle Level of Detail adjustments and culling
		//CSpatialPartition::GetInstance()->LevelOfDetailEnable = !CSpatialPartition::GetInstance()->LevelOfDetailEnable;

	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyGenerateLua))
	{
		Graph::GetInstance()->Generate_Lua();
	}
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyLuaModifyFSM))
	{
		CSceneGraph::GetInstance()->LuaModifyFSMEnemy();
	}

	//if(KeyboardController::GetInstance()->IsKeyDown('I'))
	//	lights[0]->position.z -= (float)(10.f * dt);
	//if(KeyboardController::GetInstance()->IsKeyDown('K'))
	//	lights[0]->position.z += (float)(10.f * dt);
	//if(KeyboardController::GetInstance()->IsKeyDown('J'))
	//	lights[0]->position.x -= (float)(10.f * dt);
	//if(KeyboardController::GetInstance()->IsKeyDown('L'))
	//	lights[0]->position.x += (float)(10.f * dt);
	//if(KeyboardController::GetInstance()->IsKeyDown('O'))
	//	lights[0]->position.y -= (float)(10.f * dt);
	//if(KeyboardController::GetInstance()->IsKeyDown('P'))
	//	lights[0]->position.y += (float)(10.f * dt);

	// if the left mouse button was released

	//if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
	//{
	//	cout << "hey its me" << endl;
	//}
	//if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
	//{
	//	cout << "reggie!" << endl;
	//}

	if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB))
	{
		cout << "Left Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::RMB))
	{
		cout << "Right Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::MMB))
	{
		cout << "Middle Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in X-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in Y-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) << endl;
	}
	// <THERE>

	// Hardware Abstraction
	theKeyboard->Read((float)dt);
	theMouse->Read((float)dt);

	// Update the player position and other details based on keyboard and mouse inputs

	//if (CurrentScreen)
	playerInfo->Update(dt);

	//camera.Update(dt); // Can put the camera into an entity rather than here (Then we don't have to write this)

	// Update the Scene Graph
	CSceneGraph::GetInstance()->Update((float)dt);

	// Update the Spatial Partition
	CSpatialPartition::GetInstance()->Update();

	// Update NPC
	//enemyInfo->Update(dt);

	for (int i = 0; i < CSpatialPartition::GetInstance()->GridDisplayer.size(); ++i)
	{
		CSpatialPartition::GetInstance()->GridDisplayer[i].second -= dt;
		if (CSpatialPartition::GetInstance()->GridDisplayer[i].second <= 0 || !CSpatialPartition::GetInstance()->orangeTileEnable)
		{
			CSpatialPartition::GetInstance()->GridDisplayer[i].first->SetIsDone(true);
		}
	}

	for (auto iter = CSpatialPartition::GetInstance()->GridDisplayer.begin();
		iter != CSpatialPartition::GetInstance()->GridDisplayer.end();
		iter++)
	{
		if ((*iter).first->IsDone())
		{
			iter = CSpatialPartition::GetInstance()->GridDisplayer.erase(iter);
			if (CSpatialPartition::GetInstance()->GridDisplayer.size() == 0) break;

			if (iter != CSpatialPartition::GetInstance()->GridDisplayer.begin())
			{
				iter = std::prev(iter);
				continue;
			}
		}

	}

	GraphicsManager::GetInstance()->UpdateLights(dt);

	// Update the 2 text object values
	// Eg. FPSRenderEntity or inside RenderUI for LightEntity
	DisplayText.str("");
	DisplayText.clear();
	DisplayText << "FPS: " << CFPSCounter::GetInstance()->GetFrameRate() << endl;
	textObj[1]->SetText(DisplayText.str());

	DisplayText.str("");
	DisplayText.clear();
	DisplayText << "Sway:" << playerInfo->m_fCameraSwayAngle;
	textObj[2]->SetText(DisplayText.str());

	// Update camera effects
	theCameraEffects->Update((float)dt);
}

void SceneText::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	Mesh* crosshair = MeshBuilder::GetInstance()->GetMesh("crosshair");

	modelStack.PushMatrix();
	//Vector3 tempdirplayer = (camera.GetCameraTarget() - camera.GetCameraPos()).Normalized();
	//Vector3 temprightplayer = (tempdirplayer.Cross(camera.GetCameraUp())).Normalized();
	//Vector3 tempupplayer = (camera.GetCameraUp()).Normalized();
	//modelStack.Translate(temprightplayer.x * 100, tempupplayer.y * 100, temprightplayer.z * 100); //weird thing

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -6.0f);
	modelStack.Scale(0.25, 0.25f, 0.25f);
	RenderHelper::RenderMeshGun(crosshair);
	modelStack.PopMatrix();

	// PreRenderMesh
	RenderHelper::PreRenderMesh();
		EntityManager::GetInstance()->Render(); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
		CSceneGraph::GetInstance()->Render(); //All active entities/moving things
		CSpatialPartition::GetInstance()->Render(); //Spatial partitioning grid

		//for (int i = 0; i < Graph::GetInstance()->m_edges.size(); ++i)
		//{
		//	modelStack.PushMatrix();
		//	modelStack.Translate(Graph::GetInstance()->m_edges[i]->from.second.x, CSpatialPartition::GetInstance()->yOffset, Graph::GetInstance()->m_edges[i]->from.second.y);
		//	modelStack.Rotate(Math::RadianToDegree(atan2(Graph::GetInstance()->m_edges[i]->to.second.y - Graph::GetInstance()->m_edges[i]->from.second.y, Graph::GetInstance()->m_edges[i]->to.second.x - Graph::GetInstance()->m_edges[i]->from.second.x)), 0, 1, 0);
		//	modelStack.Scale(Graph::GetInstance()->m_edges[i]->cost, 1, 1);
		//	RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("line"));
		//	modelStack.PopMatrix();
		//}


		for (int i = 0; i < Graph::GetInstance()->m_nodes.size(); ++i)
		{
			modelStack.PushMatrix();
			modelStack.Translate(Graph::GetInstance()->m_nodes[i]->pos.x, CSpatialPartition::GetInstance()->yOffset + 2, Graph::GetInstance()->m_nodes[i]->pos.z);
			modelStack.Rotate(-90, 1, 0, 0);
			modelStack.Scale(100, 100, 100);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("waypoint"));
			modelStack.PopMatrix();
		}

		if (ShopThings::GetInstance()->RobotBought)
		{
			GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, 0.1, 1000);
			GraphicsManager::GetInstance()->AttachCamera(&fakeorthoCam);
			modelStack.PushMatrix();
			modelStack.Translate(0, -1120, 120);
			modelStack.Scale(50, 50, 50);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Enemy_HighLOD"));
			modelStack.PopMatrix();
		}
		GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
		GraphicsManager::GetInstance()->AttachCamera(&camera);

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Enable blend mode
	glEnable(GL_BLEND);

		GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, 0.1, 1000);
		GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

		if (showQuadtreemap)
		{
			// PreRenderMesh
			RenderHelper::PreRenderMesh();
			MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
			modelStack.PushMatrix();
			modelStack.Translate(0, 0, 0);
			modelStack.Scale(1, 1, 1);
			EntityManager::GetInstance()->Render(true); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
			CSceneGraph::GetInstance()->Render(true); //All active entities/moving things
			CSpatialPartition::GetInstance()->Render(); //Spatial partitioning grid

			for (int i = 0; i < Graph::GetInstance()->m_nodes.size(); ++i)
			{
				modelStack.PushMatrix();
				modelStack.Translate(Graph::GetInstance()->m_nodes[i]->pos.x, CSpatialPartition::GetInstance()->yOffset + 2, Graph::GetInstance()->m_nodes[i]->pos.z);
				modelStack.Rotate(-90, 1, 0, 0);
				modelStack.Scale(20, 20, 20);
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("waypoint"));
				modelStack.PopMatrix();
			}

			modelStack.PushMatrix();
			modelStack.Translate(CPlayerInfo::GetInstance()->GetPos().x, CPlayerInfo::GetInstance()->GetPos().y, CPlayerInfo::GetInstance()->GetPos().z);
			modelStack.Scale(4, 1, 4);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("PlayerPoint"));

			modelStack.PopMatrix();

			modelStack.PopMatrix();
		}

		// Setup 2D pipeline then render 2D
		int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
		int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
		GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
		GraphicsManager::GetInstance()->DetachCamera();

		// PreRenderText
		RenderHelper::PreRenderText();

			EntityManager::GetInstance()->RenderUI();

			if (KeyboardController::GetInstance()->IsKeyDown('9'))
				theCameraEffects->SetStatus_BloodScreen(true);
			// Render Camera Effects
			theCameraEffects->RenderUI();

			// Render Minimap
			theMinimap->RenderUI();

		// PostRenderText
		RenderHelper::PostRenderText();

	// PostRenderMesh
		RenderHelper::PostRenderMesh();

		modelStack.PopMatrix();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneText::Exit()
{
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	if (playerInfo->DropInstance() == false)
	{
#if _DEBUGMODE==1
		cout << "Unable to drop PlayerInfo class" << endl;
#endif
	}

	// Delete the lights
	GraphicsManager::GetInstance()->RemoveLight("lights[0]");
	GraphicsManager::GetInstance()->RemoveLight("lights[1]");
	//delete lights[0];
	//delete lights[1];
	CSceneGraph::GetInstance()->Destroy();
	// Delete the Spatial Partition
	CSpatialPartition::GetInstance()->Destroy();
	EntityManager::GetInstance()->Destroy();
}

/********************************************************************************
Create Entities to display in this game
********************************************************************************/
void SceneText::CreateEntities(void)
{
	srand(time(NULL));
	if (true)
	{

		for (int i = 0; i < SceneManager::GetInstance()->totalNormalEnemy; ++i)
		{
			CEnemy3D* anEnemy3D = Create::Enemy3D("cube", Vector3(Math::RandFloatMinMax(-250, 250), 0.0f, Math::RandFloatMinMax(-250, 250)), Vector3(10.0f, 10.0f, 10.0f), false);
			anEnemy3D->InitLOD("Enemy_HighLOD", "Enemy_MidLOD", "Enemy_LowLOD");
			anEnemy3D->Init();
			anEnemy3D->SetSpeed(4.0);
			anEnemy3D->SetCollider(true);
			anEnemy3D->SetAABB(Vector3(10, 10, 10), Vector3(-10, -1.5, -10));
			anEnemy3D->SetTerrain(groundEntity);
			CSpatialPartition::GetInstance()->Add(anEnemy3D);
			CSceneNode* pNPCSceneNode = CSceneGraph::GetInstance()->AddNode(anEnemy3D);
			pNPCSceneNode->SetTranslate(Vector3(0.0f, 0.0f, 0.0f));
		}

	}

	CSceneGraph::GetInstance()->ReCalc_AABB();

}

void SceneText::CreateMantis()
{
	CEnemy3D* mantis_Body0 = Create::Enemy3D("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	mantis_Body0->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
	mantis_Body0->Init();
	mantis_Body0->SetPos(Vector3(15, 0, 15));
	//mantis_Body0->SetTerrain(groundEntity[0]);
	mantis_Body0->SetTerrain(groundEntity);
	mantis_Body0->SetSpeed(0);

	mantis_Body0->SetCollider(true); 
	//mantis_Body0->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
	mantis_Body0->SetAABB(Vector3(10.0f, 10.0f, 10.0f), Vector3(-10.0f, -10.0f, -10.0f));
	//CSceneNode* mantis_SceneNode = CSceneGraph::GetInstance()->AddNode(CSceneNode::MANTIS_BODY, mantis_Body0);
	CSceneNode* mantis_SceneNode = CSceneGraph::GetInstance()->AddNode(mantis_Body0);
	//mantis_SceneNode->SetTranslate(Vector3(0, 6, -40));
	mantis_SceneNode->SetRotate(45, 0, 1, 0);
	mantis_SceneNode->SetScale(3, 3, 3); //ONLY SCALE THIS FOR LINEAR SCALING
	mantis_SceneNode->SetPScale(Vector3(1.5, 6.5, 1.5)); //PScale does not affect children!
	CUpdateTransformation* aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1.0f, 0.0f, 1.0f, 0.0f);
	aRotateMtx->SetSteps(-80, 80);
	mantis_SceneNode->SetUpdateTransformation(aRotateMtx);
	CSpatialPartition::GetInstance()->Add(mantis_Body0);

	GenericEntity* mantis_Head = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	mantis_Head->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
	mantis_Head->SetCollider(true);
	mantis_Head->SetAABB(Vector3(10.5f, 10.5f, 10.5f), Vector3(-10.5f, -10.5f, -10.5f));
	mantis_Head->typeofent = EntityBase::T_ENEMY;
	//CSceneNode* mantisPart_SceneNode = mantis_SceneNode->AddChild(CSceneNode::MANTIS_HEAD, mantis_Head);
	CSceneNode* mantisPart_SceneNode = mantis_SceneNode->AddChild(mantis_Head);
	mantisPart_SceneNode->SetTranslate(Vector3(50, 3.5, 1));
	mantisPart_SceneNode->SetRotate(15, 1, 0, 0);
	mantisPart_SceneNode->SetPScale(Vector3(1.5*1.5, 0.25*6.25, 2.5*1.5));
	CSpatialPartition::GetInstance()->Add(mantis_Head);


	{//ARMS IN HERE
		GenericEntity* mantis_LArm0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_LArm0->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_LArm0->SetCollider(true);
		mantis_LArm0->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//mantisPart_SceneNode = mantis_SceneNode->AddChild(CSceneNode::MANTIS_LARM0, mantis_LArm0);
		mantisPart_SceneNode = mantis_SceneNode->AddChild(mantis_LArm0);
		mantisPart_SceneNode->SetTranslate(Vector3(1, -1, 0.5));
		mantisPart_SceneNode->SetRotate(40, 0, 1, 0);
		mantisPart_SceneNode->SetRotate(50, 1, 0, 0);
		mantisPart_SceneNode->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(1.0f, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-30, 30);
		mantisPart_SceneNode->SetUpdateTransformation(aRotateMtx);

		GenericEntity* mantis_LArm1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_LArm1->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_LArm1->SetCollider(true);
		mantis_LArm1->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//CSceneNode* mantisPart_SceneNode_LArm0 = mantisPart_SceneNode->AddChild(CSceneNode::MANTIS_LARM1, mantis_LArm1);
		CSceneNode* mantisPart_SceneNode_LArm0 = mantisPart_SceneNode->AddChild(mantis_LArm1);
		//mantisPart_SceneNode_LArm0->SetRotate(50, 1, 0, 0);
		mantisPart_SceneNode_LArm0->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_LArm0->SetRotate(-140, 1, 0, 0);
		mantisPart_SceneNode_LArm0->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_LArm0->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));

		GenericEntity* mantis_LArm2 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_LArm2->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_LArm2->SetCollider(true);
		mantis_LArm2->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//CSceneNode* mantisPart_SceneNode_LArm1 = mantisPart_SceneNode_LArm0->AddChild(CSceneNode::MANTIS_LARM2, mantis_LArm2);
		CSceneNode* mantisPart_SceneNode_LArm1 = mantisPart_SceneNode_LArm0->AddChild(mantis_LArm2);
		mantisPart_SceneNode_LArm1->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_LArm1->SetRotate(140, 1, 0, 0);
		mantisPart_SceneNode_LArm1->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_LArm1->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));

		//////

		GenericEntity* mantis_RArm0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_RArm0->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_RArm0->SetCollider(true);
		mantis_RArm0->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//mantisPart_SceneNode = mantis_SceneNode->AddChild(CSceneNode::MANTIS_RARM0, mantis_RArm0);
		mantisPart_SceneNode = mantis_SceneNode->AddChild(mantis_RArm0);
		mantisPart_SceneNode->SetTranslate(Vector3(-1, -1, 0.5));
		mantisPart_SceneNode->SetRotate(-40, 0, 1, 0);
		mantisPart_SceneNode->SetRotate(50, 1, 0, 0);
		mantisPart_SceneNode->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-1.0f, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-30, 30);
		mantisPart_SceneNode->SetUpdateTransformation(aRotateMtx);

		GenericEntity* mantis_RArm1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_RArm1->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_RArm1->SetCollider(true);
		mantis_RArm1->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//CSceneNode* mantisPart_SceneNode_RArm0 = mantisPart_SceneNode->AddChild(CSceneNode::MANTIS_RARM1, mantis_RArm1);
		CSceneNode* mantisPart_SceneNode_RArm0 = mantisPart_SceneNode->AddChild(mantis_RArm1);
		mantisPart_SceneNode_RArm0->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_RArm0->SetRotate(-140, 1, 0, 0);
		mantisPart_SceneNode_RArm0->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_RArm0->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));

		GenericEntity* mantis_RArm2 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		mantis_RArm2->InitLOD("MantisSkin_HighLOD", "MantisSkin_MidLOD", "MantisSkin_LowLOD");
		mantis_RArm2->SetCollider(true);
		mantis_RArm2->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		//CSceneNode* mantisPart_SceneNode_RArm1 = mantisPart_SceneNode_RArm0->AddChild(CSceneNode::MANTIS_RARM2, mantis_RArm2);
		CSceneNode* mantisPart_SceneNode_RArm1 = mantisPart_SceneNode_RArm0->AddChild(mantis_RArm2);
		mantisPart_SceneNode_RArm1->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_RArm1->SetRotate(140, 1, 0, 0);
		mantisPart_SceneNode_RArm1->SetTranslate(Vector3(0, 0, 2.65));
		mantisPart_SceneNode_RArm1->SetPScale(Vector3(0.25*1.5, 0.25*1.5, 3.5*1.5));

	}


	CSceneGraph::GetInstance()->ReCalc_AABB();
}

void SceneText::CreateBoss()
{
	CEnemy3D* boss_Body0 = Create::Enemy3D("sphere", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	boss_Body0->InitLOD("BallMetal_HighLOD", "BallMetal_MidLOD", "BallMetal_LowLOD");
	boss_Body0->Init();
	boss_Body0->SetPos(Vector3(-100, 0, -100));
	//mantis_Body0->SetTerrain(groundEntity[0]);
	boss_Body0->SetTerrain(groundEntity);
	boss_Body0->SetSpeed(5);
	boss_Body0->doneCondition = EntityBase::CDC_NOCHILD;
	boss_Body0->SetCollider(true);
	//mantis_Body0->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
	boss_Body0->SetAABB(Vector3(15.0f, 10.0f, 15.0f), Vector3(-15.0f, -10.0f, -15.0f));
	//CSceneNode* mantis_SceneNode = CSceneGraph::GetInstance()->AddNode(CSceneNode::MANTIS_BODY, mantis_Body0);
	CSceneNode* boss_SceneNode = CSceneGraph::GetInstance()->AddNode(boss_Body0);
	//mantis_SceneNode->SetTranslate(Vector3(0, 6, -40));
	boss_SceneNode->SetRotate(45, 0, 1, 0);
	boss_SceneNode->SetScale(5, 5, 5); //ONLY SCALE THIS FOR LINEAR SCALING
	boss_SceneNode->SetPScale(Vector3(3, 1, 3)); //PScale does not affect children!
	CUpdateTransformation* aRotateMtx = new CUpdateTransformation();
	//aRotateMtx->ApplyUpdate(0.05, 0.0f, 0.0f, 1.0f);
	//aRotateMtx->SetSteps(-450, 450);
	//boss_SceneNode->SetUpdateTransformation(aRotateMtx);
	CSpatialPartition::GetInstance()->Add(boss_Body0);

	GenericEntity* boss_Head = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	boss_Head->InitLOD("Metal_HighLOD", "Metal_MidLOD", "Metal_LowLOD");
	boss_Head->SetCollider(true);
	boss_Head->SetAABB(Vector3(1.5f, 25.0f, 1.5f), Vector3(-1.5f, -5.0f, -1.5f));
	boss_Head->typeofent = EntityBase::T_ENEMY;
	boss_Head->doneCondition = EntityBase::CDC_NOCHILD;
	CSceneNode* bossPart_SceneNode = boss_SceneNode->AddChild(boss_Head);
	bossPart_SceneNode->SetTranslate(Vector3(0, 2.5, 0));
	bossPart_SceneNode->SetPScale(Vector3(0.5, 5, 0.5));
	aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(2, 0.0f, 1.0f, 0.0f);
	aRotateMtx->SetSteps(0, 360000);
	bossPart_SceneNode->SetUpdateTransformation(aRotateMtx);
	CSpatialPartition::GetInstance()->Add(boss_Head);

	{
		GenericEntity* boss_Fin0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_Fin0->InitLOD("Metal_HighLOD", "Metal_MidLOD", "Metal_LowLOD");
		boss_Fin0->SetCollider(true);
		boss_Fin0->SetAABB(Vector3(3, 1, 3), Vector3(-3, -1, -3));
		boss_Fin0->typeofent = EntityBase::T_ENEMY;
		CSceneNode* bossPartfin_SceneNode = bossPart_SceneNode->AddChild(boss_Fin0);
		bossPartfin_SceneNode->SetTranslate(Vector3(5, 2.5, 0));
		bossPartfin_SceneNode->SetPScale(Vector3(0.6, 0.2, 0.6));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(0, 360000);
		bossPartfin_SceneNode->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_Fin0);

		GenericEntity* boss_Fin1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_Fin1->InitLOD("Metal_HighLOD", "Metal_MidLOD", "Metal_LowLOD");
		boss_Fin1->SetCollider(true);
		boss_Fin1->SetAABB(Vector3(3, 1, 3), Vector3(-3, -1, -3));
		boss_Fin1->typeofent = EntityBase::T_ENEMY;
		bossPartfin_SceneNode = bossPart_SceneNode->AddChild(boss_Fin1);
		bossPartfin_SceneNode->SetTranslate(Vector3(0, 2.5, 5));
		bossPartfin_SceneNode->SetPScale(Vector3(0.6, 0.2, 0.6));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(0, 360000);
		bossPartfin_SceneNode->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_Fin1);

		GenericEntity* boss_Fin2 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_Fin2->InitLOD("Metal_HighLOD", "Metal_MidLOD", "Metal_LowLOD");
		boss_Fin2->SetCollider(true);
		boss_Fin2->SetAABB(Vector3(3, 1, 3), Vector3(-3, -1, -3));
		boss_Fin2->typeofent = EntityBase::T_ENEMY;
		bossPartfin_SceneNode = bossPart_SceneNode->AddChild(boss_Fin2);
		bossPartfin_SceneNode->SetTranslate(Vector3(0, 2.5, -5));
		bossPartfin_SceneNode->SetPScale(Vector3(0.6, 0.2, 0.6));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(0, 360000);
		bossPartfin_SceneNode->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_Fin2);

		GenericEntity* boss_Fin3 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_Fin3->InitLOD("Metal_HighLOD", "Metal_MidLOD", "Metal_LowLOD");
		boss_Fin3->SetCollider(true);
		boss_Fin3->SetAABB(Vector3(3, 1, 3), Vector3(-3, -1, -3));
		boss_Fin3->typeofent = EntityBase::T_ENEMY;
		bossPartfin_SceneNode = bossPart_SceneNode->AddChild(boss_Fin3);
		bossPartfin_SceneNode->SetTranslate(Vector3(-5, 2.5, 0));
		bossPartfin_SceneNode->SetPScale(Vector3(0.6, 0.2, 0.6));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(0, 360000);
		bossPartfin_SceneNode->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_Fin3);
	}

	CSceneGraph::GetInstance()->ReCalc_AABB();
}

void SceneText::CreateStaticSpider()
{
	CEnemy3D* boss_Body0 = Create::Enemy3D("SpiderSkin_LowLOD", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	boss_Body0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
	boss_Body0->Init();
	boss_Body0->SetPos(Vector3(200, 5, 0));
	boss_Body0->SetTarget(Vector3(200, 5, 400));
	//mantis_Body0->SetTerrain(groundEntity[0]);
	boss_Body0->SetTerrain(groundEntity);
	boss_Body0->SetSpeed(10);
	boss_Body0->typeofent = EntityBase::T_NORMAL;
	boss_Body0->doneCondition = EntityBase::CDC_NOCHILD;
	boss_Body0->SetCollider(true);
	//mantis_Body0->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
	boss_Body0->SetAABB(Vector3(20.0f, 20.0f, 20.0f), Vector3(-20.0f, -20.0f, -20.0f));
	//CSceneNode* mantis_SceneNode = CSceneGraph::GetInstance()->AddNode(CSceneNode::MANTIS_BODY, mantis_Body0);
	CSceneNode* boss_SceneNode = CSceneGraph::GetInstance()->AddNode(boss_Body0);
	//mantis_SceneNode->SetTranslate(Vector3(0, 6, -40));
	//boss_SceneNode->SetRotate(45, 0, 1, 0);
	boss_SceneNode->SetScale(5, 5, 5); //ONLY SCALE THIS FOR LINEAR SCALING
	boss_SceneNode->SetPScale(Vector3(4, 1, 3)); //PScale does not affect children!
	CUpdateTransformation* aRotateMtx = new CUpdateTransformation();
	CSpatialPartition::GetInstance()->Add(boss_Body0);
	//aRotateMtx->ApplyUpdate(0.05, 0.0f, 0.0f, 1.0f);
	//aRotateMtx->SetSteps(-450, 450);
	//boss_SceneNode->SetUpdateTransformation(aRotateMtx);

	GenericEntity* boss_Head = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
	boss_Head->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
	boss_Head->typeofent = EntityBase::T_NORMAL;
	boss_Head->doneCondition = EntityBase::CDC_NOCHILD;
	CSceneNode* bossPart_SceneNode = boss_SceneNode->AddChild(boss_Head);
	bossPart_SceneNode->SetTranslate(Vector3(0, 0, 2.0));
	bossPart_SceneNode->SetPScale(Vector3(2, 0.8, 1.8));
	aRotateMtx = new CUpdateTransformation();
	aRotateMtx->ApplyUpdate(1, 1.0f, 0.0f, 0.0f);
	aRotateMtx->SetSteps(-15, 15);
	bossPart_SceneNode->SetUpdateTransformation(aRotateMtx);
	CSpatialPartition::GetInstance()->Add(boss_Head);

	//spider front legs
	{
		GenericEntity* boss_FrontLegL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL0->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg0 = boss_SceneNode->AddChild(boss_FrontLegL0);
		bossPart_SceneNodeFLeg0->SetTranslate(Vector3(-2, 0, 3.0));
		bossPart_SceneNodeFLeg0->SetRotate(-30, 0, 1, 0);
		bossPart_SceneNodeFLeg0->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(2, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL0);


		GenericEntity* boss_FrontLegL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL1->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg1 = bossPart_SceneNodeFLeg0->AddChild(boss_FrontLegL1);
		bossPart_SceneNodeFLeg1->SetTranslate(Vector3(0, 0, 3.0));
		bossPart_SceneNodeFLeg1->SetRotate(50, 1, 0, 0);
		bossPart_SceneNodeFLeg1->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-3, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL1);

		GenericEntity* boss_FrontLegL2 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL2->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL2->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL2->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg2 = bossPart_SceneNodeFLeg1->AddChild(boss_FrontLegL2);
		bossPart_SceneNodeFLeg2->SetTranslate(Vector3(0, 0, 2.0));
		bossPart_SceneNodeFLeg2->SetRotate(-55, 1, 0, 0);
		bossPart_SceneNodeFLeg2->SetTranslate(Vector3(0, 0, 1.0));
		bossPart_SceneNodeFLeg2->SetPScale(Vector3(0.8, 0.8, 2));
		//aRotateMtx = new CUpdateTransformation();
		//aRotateMtx->ApplyUpdate(-2.0, 1.0f, 0.0f, 0.0f);
		//aRotateMtx->SetSteps(-20, 20);
		//bossPart_SceneNodeFLeg2->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL2);
	}
	{
		GenericEntity* boss_FrontLegL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL0->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg0 = boss_SceneNode->AddChild(boss_FrontLegL0);
		bossPart_SceneNodeFLeg0->SetTranslate(Vector3(2, 0, 3.0));
		bossPart_SceneNodeFLeg0->SetRotate(30, 0, 1, 0);
		bossPart_SceneNodeFLeg0->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL0);


		GenericEntity* boss_FrontLegL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL1->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg1 = bossPart_SceneNodeFLeg0->AddChild(boss_FrontLegL1);
		bossPart_SceneNodeFLeg1->SetTranslate(Vector3(0, 0, 3.0));
		bossPart_SceneNodeFLeg1->SetRotate(50, 1, 0, 0);
		bossPart_SceneNodeFLeg1->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(3, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL1);

		GenericEntity* boss_FrontLegL2 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL2->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL2->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL2->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg2 = bossPart_SceneNodeFLeg1->AddChild(boss_FrontLegL2);
		bossPart_SceneNodeFLeg2->SetTranslate(Vector3(0, 0, 2.0));
		bossPart_SceneNodeFLeg2->SetRotate(-55, 1, 0, 0);
		bossPart_SceneNodeFLeg2->SetTranslate(Vector3(0, 0, 1.0));
		bossPart_SceneNodeFLeg2->SetPScale(Vector3(0.8, 0.8, 2));
		//aRotateMtx = new CUpdateTransformation();
		//aRotateMtx->ApplyUpdate(2.0, 1.0f, 0.0f, 0.0f);
		//aRotateMtx->SetSteps(-20, 20);
		//bossPart_SceneNodeFLeg2->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL2);
	}

	//spider back legs
	{
		GenericEntity* boss_InvisiJoint = Create::Entity("sphere", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_InvisiJoint->typeofent = EntityBase::T_NORMAL;
		boss_InvisiJoint->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeJoint = boss_SceneNode->AddChild(boss_InvisiJoint);
		bossPart_SceneNodeJoint->SetPScale(Vector3(0, 0, 0));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeJoint->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_InvisiJoint);

		GenericEntity* boss_FrontLegL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL0->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg0 = bossPart_SceneNodeJoint->AddChild(boss_FrontLegL0);
		bossPart_SceneNodeFLeg0->SetTranslate(Vector3(-2, 0, -2.0));
		bossPart_SceneNodeFLeg0->SetRotate(-135, 0, 1, 0);
		bossPart_SceneNodeFLeg0->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-2, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL0);

		GenericEntity* boss_FrontLegL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL1->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg1 = bossPart_SceneNodeFLeg0->AddChild(boss_FrontLegL1);
		bossPart_SceneNodeFLeg1->SetTranslate(Vector3(0, 0, 3.0));
		bossPart_SceneNodeFLeg1->SetRotate(50, 1, 0, 0);
		bossPart_SceneNodeFLeg1->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(3, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL1);

	}
	{
		GenericEntity* boss_InvisiJoint = Create::Entity("sphere", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_InvisiJoint->typeofent = EntityBase::T_NORMAL;
		boss_InvisiJoint->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeJoint = boss_SceneNode->AddChild(boss_InvisiJoint);
		bossPart_SceneNodeJoint->SetPScale(Vector3(0, 0, 0));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(2, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeJoint->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_InvisiJoint);

		GenericEntity* boss_FrontLegL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL0->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg0 = bossPart_SceneNodeJoint->AddChild(boss_FrontLegL0);
		bossPart_SceneNodeFLeg0->SetTranslate(Vector3(2, 0, -2.0));
		bossPart_SceneNodeFLeg0->SetRotate(135, 0, 1, 0);
		bossPart_SceneNodeFLeg0->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(2, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL0);

		GenericEntity* boss_FrontLegL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_FrontLegL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_FrontLegL1->typeofent = EntityBase::T_NORMAL;
		boss_FrontLegL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNodeFLeg1 = bossPart_SceneNodeFLeg0->AddChild(boss_FrontLegL1);
		bossPart_SceneNodeFLeg1->SetTranslate(Vector3(0, 0, 3.0));
		bossPart_SceneNodeFLeg1->SetRotate(50, 1, 0, 0);
		bossPart_SceneNodeFLeg1->SetPScale(Vector3(0.8, 0.8, 5));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-3, 1.0f, 0.0f, 0.0f);
		aRotateMtx->SetSteps(-10, 10);
		bossPart_SceneNodeFLeg1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_FrontLegL1);

	}

	//spider mandibles
	{
		GenericEntity* boss_TeethL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_TeethL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_TeethL0->typeofent = EntityBase::T_NORMAL;
		boss_TeethL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNode_TeethL0 = bossPart_SceneNode->AddChild(boss_TeethL0);
		bossPart_SceneNode_TeethL0->SetTranslate(Vector3(-0.7, 0, 1));
		bossPart_SceneNode_TeethL0->SetPScale(Vector3(0.15, 0.15, 0.8));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(3, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(-5, 5);
		bossPart_SceneNode_TeethL0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_TeethL0);

		GenericEntity* boss_TeethL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_TeethL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_TeethL1->typeofent = EntityBase::T_NORMAL;
		boss_TeethL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNode_TeethL1 = bossPart_SceneNode_TeethL0->AddChild(boss_TeethL1);
		bossPart_SceneNode_TeethL1->SetTranslate(Vector3(0, 0, 0.6));
		bossPart_SceneNode_TeethL1->SetRotate(30, 0, 1, 0);
		bossPart_SceneNode_TeethL1->SetPScale(Vector3(0.15, 0.15, 0.6));
		//aRotateMtx = new CUpdateTransformation();
		//aRotateMtx->ApplyUpdate(3, 0.0f, 1.0f, 0.0f);
		//aRotateMtx->SetSteps(-5, 5);
		//bossPart_SceneNode_TeethL1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_TeethL1);
	}
	{
		GenericEntity* boss_TeethL0 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_TeethL0->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_TeethL0->typeofent = EntityBase::T_NORMAL;
		boss_TeethL0->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNode_TeethL0 = bossPart_SceneNode->AddChild(boss_TeethL0);
		bossPart_SceneNode_TeethL0->SetTranslate(Vector3(0.7, 0, 1));
		bossPart_SceneNode_TeethL0->SetPScale(Vector3(0.15, 0.15, 0.8));
		aRotateMtx = new CUpdateTransformation();
		aRotateMtx->ApplyUpdate(-3, 0.0f, 1.0f, 0.0f);
		aRotateMtx->SetSteps(-5, 5);
		bossPart_SceneNode_TeethL0->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_TeethL0);

		GenericEntity* boss_TeethL1 = Create::Entity("cube", Vector3(0, 0, 0), Vector3(1, 1, 1), false);
		boss_TeethL1->InitLOD("SpiderSkin_HighLOD", "SpiderSkin_MidLOD", "SpiderSkin_LowLOD");
		boss_TeethL1->typeofent = EntityBase::T_NORMAL;
		boss_TeethL1->doneCondition = EntityBase::CDC_NOCHILD;
		CSceneNode* bossPart_SceneNode_TeethL1 = bossPart_SceneNode_TeethL0->AddChild(boss_TeethL1);
		bossPart_SceneNode_TeethL1->SetTranslate(Vector3(0, 0, 0.6));
		bossPart_SceneNode_TeethL1->SetRotate(-30, 0, 1, 0);
		bossPart_SceneNode_TeethL1->SetPScale(Vector3(0.15, 0.15, 0.6));
		//aRotateMtx = new CUpdateTransformation();
		//aRotateMtx->ApplyUpdate(3, 0.0f, 1.0f, 0.0f);
		//aRotateMtx->SetSteps(-5, 5);
		//bossPart_SceneNode_TeethL1->SetUpdateTransformation(aRotateMtx);
		CSpatialPartition::GetInstance()->Add(boss_TeethL1);
	}

}
