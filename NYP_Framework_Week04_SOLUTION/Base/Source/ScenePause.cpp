#include "ScenePause.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include <iostream>
using namespace std;

ScenePause* ScenePause::sInstance = new ScenePause(SceneManager::GetInstance());

ScenePause::ScenePause()
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
}

ScenePause::ScenePause(SceneManager* _sceneMgr)
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Pause", this);
}

ScenePause::~ScenePause()
{
	if (Box == NULL)
	{
		delete Box;
		Box = NULL;
	}
	if (Buttons[0] != NULL)
	{
		delete Buttons[0];
		Buttons[0] = NULL;
	}
	if (Buttons[1] != NULL)
	{
		delete Buttons[1];
		Buttons[1] = NULL;
	}
	//if (theMouse)
	//{
	//	delete theMouse;
	//	theMouse = NULL;
	//}
	//if (theKeyboard)
	//{
	//	delete theKeyboard;
	//	theKeyboard = NULL;
	//}
	//// Delete the scene graph
	//CSceneGraph::GetInstance()->Destroy();
	//// Delete the Spatial Partition
	//CSpatialPartition::GetInstance()->Destroy();
	//EntityManager::GetInstance()->Destroy();
}

void ScenePause::Init()
{

	playerInfo = CPlayerInfo::GetInstance();
	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	//Create::Sprite2DObject("button_pressed", Vector3(0.0f, 0.0f, 0.0f), Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()), true);

	Box = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_pressed"));
	Box->SetPosition(Vector3(0, 500, 0));
	//Box->SetScale(Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()));
	Box->SetScale(Vector3(1000, 1000));

	Buttons[0] = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_notpressed"));
	Buttons[0]->SetPosition(Vector3(-0.25 * Application::GetInstance().GetWindowWidth(), 510, -0.25 * Application::GetInstance().GetWindowHeight()));
	Buttons[0]->SetScale(Vector3(0.2 * Application::GetInstance().GetWindowWidth(), 0.2 * Application::GetInstance().GetWindowHeight()));
	Buttons[1] = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_notpressed"));
	Buttons[1]->SetPosition(Vector3(0.25 * Application::GetInstance().GetWindowWidth(), 510, -0.25 * Application::GetInstance().GetWindowHeight()));
	Buttons[1]->SetScale(Vector3(0.2 * Application::GetInstance().GetWindowWidth(), 0.2 * Application::GetInstance().GetWindowHeight()));

	//// Setup the 2D entities
	//float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	//float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	//float fontSize = 100.0f;
	//float halfFontSize = fontSize / 2.0f;
	//textObj[0] = Create::Text2DObject("text", Vector3(0, 0, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[1] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.5, -halfWindowHeight * 0.8, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[2] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.5, -halfWindowHeight * 0.8, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[0]->SetText("Exit the game?");
	//textObj[1]->SetText("Yes!");
	//textObj[2]->SetText("No!!!");

}

void ScenePause::Update(double dt)
{
	double MousePositionX = 0, MousePositionY = 0;
	MousePositionX = Application::GetInstance().GetMousePos().first;
	MousePositionY = Application::GetInstance().GetMousePos().second;

	MousePositionX = MousePositionX - 0.5 * (double)SceneManager::GetInstance()->windowWidth;
	MousePositionY = MousePositionY - 0.5 * (double)SceneManager::GetInstance()->windowHeight; //? idk if order correct
	MousePositionY *= -1;

	for (int i = 0; i < 2 && CurrentScreen; ++i)
	{
		float subwidth = maxScreenAABB.x - minScreenAABB.x;
		float subheight = maxScreenAABB.y - minScreenAABB.y;
		float centerX = minScreenAABB.x + 0.5 * subwidth;
		float centerY = minScreenAABB.y + 0.5 * subheight;

		//Vector3 minAABB_button = -Vector3(Buttons[i]->GetScale().x * 0.5 * subwidth, Buttons[i]->GetScale().y * 0.5 * subheight) + Buttons[i]->GetPosition() + Vector3((centerX - 0.5) * 500, (centerY - 0.5) * 500);
		//minAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / 1000;
		//minAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / 1000;
		//Vector3 maxAABB_button = Vector3(Buttons[i]->GetScale().x * 0.5 * subwidth, Buttons[i]->GetScale().y * 0.5 * subheight) + Buttons[i]->GetPosition() + Vector3((centerX - 0.5) * 500, (centerY - 0.5) * 500);
		//maxAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / 1000;
		//maxAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / 1000;

		Vector3 minAABB_button = -Buttons[i]->GetScale() * 0.5 + Vector3(Buttons[i]->GetPosition().x, Buttons[i]->GetPosition().z);
		minAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		minAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();
		Vector3 maxAABB_button = Buttons[i]->GetScale() * 0.5 + Vector3(Buttons[i]->GetPosition().x, Buttons[i]->GetPosition().z);
		maxAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		maxAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();

		minAABB_button.x *= subwidth;
		minAABB_button.y *= subheight;
		maxAABB_button.x *= subwidth;
		maxAABB_button.y *= subheight;

		minAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		minAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;
		maxAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		maxAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;

		//if (i == 0)
		//std::cout << MousePositionX << " " << MousePositionY << " | " << minAABB_button << " " << maxAABB_button << std::endl;

		if (MousePositionX >= minAABB_button.x && MousePositionY >= minAABB_button.y && MousePositionX <= maxAABB_button.x && MousePositionY <= maxAABB_button.y)
		{
			//std::cout << i << std::endl;
			Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");

			if (i == 0)
				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB)) //quit
				{
					SceneManager::GetInstance()->SetActiveScene("Menu");
				}

			if (i == 1)
				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB)) //the opposite of quit
				{
					SceneManager::GetInstance()->RemoveSubScene_Queue("Pause");
				}

		}
		else
		{
			Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_notpressed");
		}
	}

}

void ScenePause::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	glEnable(GL_BLEND);

	Vector3 windowMinAABB = Vector3(minScreenAABB.x * 1000 - 500, minScreenAABB.y * 1000 - 500);
	Vector3 windowMaxAABB = Vector3(maxScreenAABB.x * 1000 - 500, maxScreenAABB.y * 1000 - 500);

	Vector3 windowCenter = windowMinAABB + (windowMaxAABB - windowMinAABB) * 0.5;

	GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, -1000, 1000);

	GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

	RenderHelper::PreRenderMesh();

	modelStack.PushMatrix();
	modelStack.Translate(-windowCenter.x, 0, windowCenter.y);
	modelStack.Scale(maxScreenAABB.x - minScreenAABB.x, 1, maxScreenAABB.y - minScreenAABB.y);

	modelStack.PushMatrix();
	modelStack.Translate(Box->GetPosition().x, Box->GetPosition().y, Box->GetPosition().z);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Scale(Box->GetScale().x, Box->GetScale().y, Box->GetScale().z);
	RenderHelper::RenderMesh(Box->modelMesh);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-Buttons[0]->GetPosition().x, Buttons[0]->GetPosition().y, Buttons[0]->GetPosition().z);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Scale(Buttons[0]->GetScale().x, Buttons[0]->GetScale().y, Buttons[0]->GetScale().z);
	RenderHelper::RenderMesh(Buttons[0]->modelMesh);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-Buttons[1]->GetPosition().x, Buttons[1]->GetPosition().y, Buttons[1]->GetPosition().z);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Scale(Buttons[1]->GetScale().x, Buttons[1]->GetScale().y, Buttons[1]->GetScale().z);
	RenderHelper::RenderMesh(Buttons[1]->modelMesh);
	modelStack.PopMatrix();

	RenderHelper::PostRenderMesh();

	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	modelStack.PopMatrix();

	// PreRenderText
	RenderHelper::PreRenderText();

	modelStack.PushMatrix();
	modelStack.Translate(windowCenter.x, windowCenter.y, 0);
	modelStack.Scale(maxScreenAABB.x - minScreenAABB.x, maxScreenAABB.y - minScreenAABB.y, 1);

	modelStack.PushMatrix();
	modelStack.Translate(-halfWindowWidth * 0.7, 0, 10.0f);
	modelStack.Scale(100, 125, 100);
	RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Go back to menu?", Color(1.0f, 0.0f, 0.0f));
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-halfWindowWidth * 0.8, -halfWindowHeight * 0.5, 10.0f);
	modelStack.Scale(100, 125, 100);
	RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Yep!", Color(1.0f, 0.0f, 0.0f));
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(halfWindowWidth * 0.3, -halfWindowHeight * 0.5, 10.0f);
	modelStack.Scale(100, 125, 100);
	RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "No!!", Color(1.0f, 0.0f, 0.0f));
	modelStack.PopMatrix();

	modelStack.PopMatrix();

	RenderHelper::PostRenderText();

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void ScenePause::Exit()
{
	if (Box != NULL)
	{
		delete Box;
		Box = NULL;
	}
	if (Buttons[0] != NULL)
	{
		delete Buttons[0];
		Buttons[0] = NULL;
	}
	if (Buttons[1] != NULL)
	{
		delete Buttons[1];
		Buttons[1] = NULL;
	}
	//	// Detach camera from other entities
	//	GraphicsManager::GetInstance()->DetachCamera();
	//	playerInfo->DetachCamera();
	//
	//	if (playerInfo->DropInstance() == false)
	//	{
	//#if _DEBUGMODE==1
	//		cout << "Unable to drop PlayerInfo class" << endl;
	//#endif
	//	}
	//
	//	GraphicsManager::GetInstance()->RemoveLight("lights[0]");
	//	GraphicsManager::GetInstance()->RemoveLight("lights[1]");
	//	CSceneGraph::GetInstance()->Destroy();
	//	// Delete the Spatial Partition
	//	CSpatialPartition::GetInstance()->Destroy();
	//	EntityManager::GetInstance()->Destroy();
}
