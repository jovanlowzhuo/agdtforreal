#include "Target.h"
#include "../EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "../SpatialPartition/SpatialPartition.h"
#include "../SceneGraph/SceneGraph.h"

CTarget::CTarget(Mesh * _modelMesh)
	: GenericEntity(NULL)
	, defaultPosition(Vector3(0.0f, 0.0f, 0.0f))
	, defaultTarget(Vector3(0.0f, 0.0f, 0.0f))
	, defaultUp(Vector3(0.0f, 0.0f, 0.0f))
	, target(Vector3(0.0f, 0.0f, 0.0f))
	, up(Vector3(0.0f, 0.0f, 0.0f))
	, maxBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, minBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, m_pTerrain(NULL)
	, m_fElapsedTimeBeforeUpdate(0.0f)
	, m_dSpeed(1.0f)
	, CurrentOffset(0, 0, 0)
	, MinOffset(0, 0, 0)
	, MaxOffset(0, 0, 0)
	, direction(true)
{
	this->modelMesh = _modelMesh;
}

CTarget::~CTarget()
{
}

void CTarget::Init(void)
{
	// Set the default values
	defaultPosition.Set(0, 0, 10);
	defaultTarget.Set(0, 0, 0);
	defaultUp.Set(0, 1, 0);

	// Set the current values
	//resets stuff so i commented it out
	//position.Set(10.0f, 0.0f, 0.0f);
	//target.Set(10.0f, 0.0f, 450.0f);
	//up.Set(0.0f, 1.0f, 0.0f);

	// Set Boundary
	maxBoundary.Set(1, 1, 1);
	minBoundary.Set(-1, -1, -1);

	// Set speed
	m_dSpeed = 1.0;
}

// Reset this player instance to default
void CTarget::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

bool CTarget::SetTrigger(CEventTrigger * setTrigger)
{
	this->Dependent = setTrigger;
	return false;
}

// Set position
void CTarget::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CTarget::SetTarget(const Vector3& target)
{
	this->target = target;
}
// Set Up
void CTarget::SetUp(const Vector3& up)
{
	this->up = up;
}

// Set position
void CTarget::SetOriginal(const Vector3& pos)
{
	OriginalPos = pos;
}

// Set position
void CTarget::SetOffset(const Vector3& min, const Vector3& max)
{
	MinOffset = min;
	MaxOffset = max;
}

void CTarget::SetDirection(bool dir)
{
	direction = dir;
}

// Set the boundary for the player info
void CTarget::SetBoundary(Vector3 max, Vector3 min)
{
	maxBoundary = max;
	minBoundary = min;
}
// Set the terrain for the player info
void CTarget::SetTerrain(GroundEntity* m_pTerrain)
{
	if (m_pTerrain != NULL)
	{
		this->m_pTerrain = m_pTerrain;

		SetBoundary(this->m_pTerrain->GetMaxBoundary(), this->m_pTerrain->GetMinBoundary());
	}
}

// Set the speed of this Enemy's movement
void CTarget::SetSpeed(const double m_dSpeed)
{
	this->m_dSpeed = m_dSpeed;
}
// Set the acceleration of this Enemy's movement
void CTarget::SetAcceleration(const double m_dAcceleration)
{
	this->m_dAcceleration = m_dAcceleration;
}

// Get position
Vector3 CTarget::GetPos(void) const
{
	return position;
}

// Get target
Vector3 CTarget::GetTarget(void) const
{
	return target;
}
// Get Up
Vector3 CTarget::GetUp(void) const
{
	return up;
}
// Get the terrain for the player info
GroundEntity* CTarget::GetTerrain(void)
{
	return m_pTerrain;
}

// Get the speed of this Enemy's movement
double CTarget::GetSpeed(void) const
{
	return m_dSpeed;
}
// Get the acceleration of this Enemy's movement
double CTarget::GetAcceleration(void) const
{
	return m_dAcceleration;
}

// Update
void CTarget::Update(double dt)
{
	if (Dependent->GetEvent())
	{
		if (direction == true)
		{
			if (CurrentOffset < MaxOffset)
			{
				CurrentOffset = Math::UpdateFlipper(CurrentOffset, MinOffset, MaxOffset, dt, direction);
			}
			else
			{
				direction = false;
			}
		}
		else if (direction == false)
		{
			if (CurrentOffset > MinOffset)
			{
				CurrentOffset = Math::UpdateFlipper(CurrentOffset, MinOffset, MaxOffset, dt, direction);
			}
			else
			{
				direction = true;
			}
		}
		position = OriginalPos + CurrentOffset + Vector3(0, 20, 0);
	}
}

// Constrain the position within the borders
void CTarget::Constrain(void)
{
	// Constrain player within the boundary
	if (position.x > maxBoundary.x - 1.0f)
		position.x = maxBoundary.x - 1.0f;
	if (position.z > maxBoundary.z - 1.0f)
		position.z = maxBoundary.z - 1.0f;
	if (position.x < minBoundary.x + 1.0f)
		position.x = minBoundary.x + 1.0f;
	if (position.z < minBoundary.z + 1.0f)
		position.z = minBoundary.z + 1.0f;

	// if the y position is not equal to terrain height at that position, 
	// then update y position to the terrain height

	//if (position.y != m_pTerrain->GetTerrainHeight(position))
	//	position.y = m_pTerrain->GetTerrainHeight(position);
}

// Render
void CTarget::Render(bool LODMap)
{
	if (GetLODStatus())
	{
		if (theDetailLevel != NO_DETAILS)
		{
			if (!LODMap)
			{
				RenderHelper::RenderMesh(GetLODMesh());
			}
			else if (theDetailLevel == HIGH_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("HighLOD"));
			}
			else if (theDetailLevel == MID_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("MidLOD"));
			}
			else if (theDetailLevel == LOW_DETAILS)
			{
				RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("LowLOD"));
			}
		}
	}
	else
	{
		RenderHelper::RenderMesh(modelMesh);
	}
	//modelStack.PopMatrix();
}

CTarget* Create::Target(const std::string& _meshName,
	const Vector3& _position,
	const Vector3& _scale,
	const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CTarget* result = new CTarget(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(false);
	result->typeofent = EntityBase::T_TARGET;

	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

CTarget* Create::Target(const std::string& _meshName,
	const Vector3& _position,
	CEventTrigger* dong,
	GroundEntity* m_pTerrain,
	const Vector3& minOffset,
	const Vector3& maxOffset,
	const bool direction,
	const Vector3& _scale,
	const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CTarget* result = new CTarget(modelMesh);
	result->SetPosition(_position);
	result->SetOriginal(_position);
	result->SetScale(_scale);
	result->SetCollider(true);
	result->SetTrigger(dong);
	result->SetTerrain(m_pTerrain);
	result->SetOffset(minOffset, maxOffset);
	result->SetDirection(direction);
	result->typeofent = EntityBase::T_TARGET;

	// Remove if needed, for testing
	result->SetAABB(result->GetScale(), -result->GetScale());

	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

bool CTarget::CanItBeSetToDone()
{
	if (doneCondition == CDC_NOCHILD)
	{
		if (CSceneGraph::GetInstance()->GetNode(this))
		{
			if (CSceneGraph::GetInstance()->GetNode(this)->childMap.size() > 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	else if (doneCondition == CDC_NONE)
	{
		return true;
	}
	else return false;

}