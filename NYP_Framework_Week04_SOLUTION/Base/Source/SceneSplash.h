#ifndef SCENE_SPLASH_H
#define SCENE_SPLASH_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "SpriteEntity.h"
#include "Enemy/Enemy3D.h"
#include "HardwareAbstraction\Keyboard.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "HardwareAbstraction\Mouse.h"
#include "Collision/EventBox.h"
#include "Collision/Target.h"
#include "WaypointGraph.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneSplash : public Scene
{
public:
	SceneSplash();
	~SceneSplash();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	SceneSplash(SceneManager* _sceneMgr); // This is used to register to SceneManager

	ShaderProgram* currProg;
	FPSCamera camera;
	FPSCamera orthoCam;
	TextEntity* textObj;
	Light* lights[2];

	double TimeToEnterGame;
	bool clickedOnScreen;

	CKeyboard* theKeyboard;
	CMouse* theMouse;
	ostringstream DisplayText;

	std::string loadingtext = "Loading";
	int amountofdots = 0;

	static SceneSplash* sInstance; // The pointer to the object that gets registered
};

#endif