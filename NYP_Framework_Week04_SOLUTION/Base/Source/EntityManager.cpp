#include "EntityManager.h"
#include "EntityBase.h"
#include "Collider/Collider.h"
#include "SpatialPartition/SpatialPartition.h"
#include "SceneGraph/SceneGraph.h"
#include "Projectile/Laser.h"
#include "GenericEntity.h"

#include <iostream>
using namespace std;

// Update all entities
void EntityManager::Update(double _dt)
{
	// Update all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		(*it)->Update(_dt);
	}

	std::list<EntityBase*>::iterator it2 = projectileList.begin();
	while (it2 != projectileList.end())
	{
		(*it2)->Update(_dt);
		++it2;
	}

	// Check for Collision amongst entities with collider properties
	CheckForCollision();

	// Clean up entities that are done
	it = entityList.begin();
	while (it != end)
	{
		if ((*it)->IsDone())
		{
			// Delete if done
			delete *it;
			it = entityList.erase(it);
		}
		else
		{
			// Move on otherwise
			++it;
		}
	}
	it2 = projectileList.begin();
	while (it2 != projectileList.end())
	{
		if ((*it2)->IsDone())
		{ 
			delete *it2;
			projectileList.erase(it2++);
		}
		else
		{
			++it2;
		}

	}
}

// Render all entities
void EntityManager::Render(bool LODMap)
{
	// Render all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		if ((*it)->GetRenderCondition() == false)
		{
			(*it)->Render(LODMap);
		}
	}

	std::list<EntityBase*>::iterator it2 = projectileList.begin();
	while (it2 != projectileList.end())
	{
		(*it2)->Render(LODMap);
		++it2;
	}
}

// Render the UI entities
void EntityManager::RenderUI()
{
	// Render all entities UI
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		(*it)->RenderUI();
	}
}

void EntityManager::Destroy()
{
	std::list<EntityBase*>::iterator it, end;
	it = entityList.begin();
	end = entityList.end();
	while (it != end)
	{
		if (*it != NULL)
		{
			delete *it;
			it = entityList.erase(it);
		}
	}

	std::list<EntityBase*>::iterator it2 = projectileList.begin();
	while (it2 != projectileList.end())
	{
		delete *it2;
		projectileList.erase(it2++);
	}

	Singleton<EntityManager>::Destroy();

}

// Add an entity to this EntityManager
void EntityManager::AddEntity(EntityBase* _newEntity, ENTITY_TYPE _type)
{
	if (_type == FIXED)
		entityList.push_back(_newEntity);
	else if (_type == PROJECTILE)
		projectileList.push_back(_newEntity);
}

// Remove an entity from this EntityManager
bool EntityManager::RemoveEntity(EntityBase* _existingEntity)
{
	// Find the entity's iterator
	std::list<EntityBase*>::iterator findIter = std::find(entityList.begin(), entityList.end(), _existingEntity);

	// Delete the entity if found
	if (findIter != entityList.end())
	{
		delete *findIter;
		findIter = entityList.erase(findIter);
		return true;	
	}
	// Return false if not found
	return false;
}

// Constructor
EntityManager::EntityManager()
{
}

// Destructor
EntityManager::~EntityManager()
{
}

// Check for overlap
bool EntityManager::CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 thatMinAABB, Vector3 thatMaxAABB)
{	
	// Check if this object is overlapping that object
	if (((thatMinAABB >= thisMinAABB) && (thatMinAABB <= thisMaxAABB))
		||
		((thatMaxAABB >= thisMinAABB) && (thatMaxAABB <= thisMaxAABB)))
	{
		return true;
	}

	// Check if that object is overlapping this object
	if (((thisMinAABB >= thatMinAABB) && (thisMinAABB <= thatMaxAABB))
		||
		((thisMaxAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB)))
	{
		return true;
	}

	// Check if this object is within that object
	if (((thisMinAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB))
		&&
		((thisMaxAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB)))
		return true;

	// Check if that object is within this object
	if (((thatMinAABB >= thisMinAABB) && (thatMinAABB <= thisMaxAABB))
		&&
		((thatMaxAABB >= thisMinAABB) && (thatMaxAABB <= thisMaxAABB)))
		return true;

	return false;
}

// Check if this entity's bounding sphere collided with that entity's bounding sphere 
bool EntityManager::CheckSphereCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	// Get the colliders for the 2 entities
	CCollider *thisCollider = dynamic_cast<CCollider*>(ThisEntity);
	CCollider *thatCollider = dynamic_cast<CCollider*>(ThatEntity);

	Vector3 thisMinAABB, thisMaxAABB, thatMinAABB, thatMaxAABB;

	// Get the minAABB and maxAABB for each entity
	if (CSceneGraph::GetInstance()->GetNode(ThisEntity) != NULL)
	{
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ThisEntity)->TheUltimateTransformation();
		thisMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thisCollider->GetMinAABB();
		thisMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thisCollider->GetMaxAABB();
	}
	else
	{
		thisMinAABB = ThisEntity->GetPosition() + thisCollider->GetMinAABB();
		thisMaxAABB = ThisEntity->GetPosition() + thisCollider->GetMaxAABB();
	}
	if (CSceneGraph::GetInstance()->GetNode(ThatEntity) != NULL)
	{
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ThatEntity)->TheUltimateTransformation();
		thatMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMinAABB();
		thatMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMaxAABB();
	}
	else
	{
		thatMinAABB = ThatEntity->GetPosition() + thatCollider->GetMinAABB();
		thatMaxAABB = ThatEntity->GetPosition() + thatCollider->GetMaxAABB();
	}


	// if Radius of bounding sphere of ThisEntity plus Radius of bounding sphere of ThatEntity is 
	// greater than the distance squared between the 2 reference points of the 2 entities,
	// then it could mean that they are colliding with each other.
	if (DistanceSquaredBetween(thisMinAABB, thisMaxAABB) + DistanceSquaredBetween(thatMinAABB, thatMaxAABB) >
		DistanceSquaredBetween(ThisEntity->GetPosition(), ThatEntity->GetPosition()) * 2.0)
	{
		return true;
	}

	return false;
}

// Check if this entity collided with another entity, but both must have collider
bool EntityManager::CheckAABBCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	// Get the colliders for the 2 entities
	CCollider *thisCollider = dynamic_cast<CCollider*>(ThisEntity);
	CCollider *thatCollider = dynamic_cast<CCollider*>(ThatEntity);

	Vector3 thisMinAABB, thisMaxAABB, thatMinAABB, thatMaxAABB;

	// Get the minAABB and maxAABB for each entity
	if (CSceneGraph::GetInstance()->GetNode(ThisEntity) != NULL)
	{
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ThisEntity)->TheUltimateTransformation();
		thisMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thisCollider->GetMinAABB();
		thisMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thisCollider->GetMaxAABB();
	}
	else
	{
		thisMinAABB = ThisEntity->GetPosition() + thisCollider->GetMinAABB();
		thisMaxAABB = ThisEntity->GetPosition() + thisCollider->GetMaxAABB();
	}
	if (CSceneGraph::GetInstance()->GetNode(ThatEntity) != NULL)
	{
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ThatEntity)->TheUltimateTransformation();
		thatMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMinAABB();
		thatMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMaxAABB();
	}
	else
	{
		thatMinAABB = ThatEntity->GetPosition() + thatCollider->GetMinAABB();
		thatMaxAABB = ThatEntity->GetPosition() + thatCollider->GetMaxAABB();
	}
	// Check for overlap
	if (CheckOverlap(thisMinAABB, thisMaxAABB, thatMinAABB, thatMaxAABB))
		return true;

	// if AABB collision check fails, then we need to check the other corners of the bounding boxes to 
	// Do more collision checks with other points on each bounding box
	Vector3 altThisMinAABB = Vector3(thisMinAABB.x, thisMinAABB.y, thisMaxAABB.z);
	Vector3 altThisMaxAABB = Vector3(thisMaxAABB.x, thisMaxAABB.y, thisMinAABB.z);
	//	Vector3 altThatMinAABB = Vector3(thatMinAABB.x, thatMinAABB.y, thatMaxAABB.z);
	//	Vector3 altThatMaxAABB = Vector3(thatMaxAABB.x, thatMaxAABB.y, thatMinAABB.z);

	// Check for overlap
	if (CheckOverlap(altThisMinAABB, altThisMaxAABB, thatMinAABB, thatMaxAABB))
		return true;

	return false;
}

// Check if this entity collided with another entity, but both must have collider
bool EntityManager::CheckAABBPointCollision(Vector3 apoint, EntityBase *ThatEntity)
{
	// Get the colliders for the 2 entities
	//CCollider *thisCollider = dynamic_cast<CCollider*>(ThisEntity);
	CCollider *thatCollider = dynamic_cast<CCollider*>(ThatEntity);

	// Get the minAABB and maxAABB for each entity
	//Vector3 thisMinAABB = ThisEntity->GetPosition() + thisCollider->GetMinAABB();
	//Vector3 thisMaxAABB = ThisEntity->GetPosition() + thisCollider->GetMaxAABB();
	Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ThatEntity)->TheUltimateTransformation();

	Vector3 themin = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMinAABB();
	Vector3 themax = Vector3(trn.a[12], trn.a[13], trn.a[14]) + thatCollider->GetMaxAABB();

	if (themin.x <= apoint.x
		&& apoint.x <= themax.x
		&& themin.y <= apoint.y
		&& apoint.y <= themax.y
		&& themin.z <= apoint.z
		&& apoint.z <= themax.z)
		return true;

	//// Check for overlap
	//if (CheckOverlap(thisMinAABB, thisMaxAABB, thatMinAABB, thatMaxAABB))
	//	return true;

	//// if AABB collision check fails, then we need to check the other corners of the bounding boxes to 
	//// Do more collision checks with other points on each bounding box
	//Vector3 altThisMinAABB = Vector3(thisMinAABB.x, thisMinAABB.y, thisMaxAABB.z);
	//Vector3 altThisMaxAABB = Vector3(thisMaxAABB.x, thisMaxAABB.y, thisMinAABB.z);
	////	Vector3 altThatMinAABB = Vector3(thatMinAABB.x, thatMinAABB.y, thatMaxAABB.z);
	////	Vector3 altThatMaxAABB = Vector3(thatMaxAABB.x, thatMaxAABB.y, thatMinAABB.z);

	//// Check for overlap
	//if (CheckOverlap(altThisMinAABB, altThisMaxAABB, thatMinAABB, thatMaxAABB))
	//	return true;

	return false;
}

//// Check if any Collider is colliding with another Collider
//bool EntityManager::CheckForCollision(void)
//{
//	// Check for Collision
//	std::list<EntityBase*>::iterator colliderThis, colliderThisEnd;
//	std::list<EntityBase*>::iterator colliderThat, colliderThatEnd;
//
//	colliderThisEnd = entityList.end();
//	for (colliderThis = entityList.begin(); colliderThis != colliderThisEnd; ++colliderThis)
//	{
//		// This object was derived from a CCollider class, then it will have Collision Detection methods
//		//CCollider *thisCollider = dynamic_cast<CCollider*>(*colliderThis);
//		EntityBase *thisEntity = dynamic_cast<EntityBase*>(*colliderThis);
//
//		if (thisEntity->HasCollider())
//		{
//			// Check for collision with another collider class
//			colliderThatEnd = entityList.end();
//			for (colliderThat = entityList.begin(); colliderThat != colliderThatEnd; ++colliderThat)
//			{
//				if (colliderThat == colliderThis)
//					continue;
//
//				if ((*colliderThat)->HasCollider())
//				{
//					EntityBase *thatEntity = dynamic_cast<EntityBase*>(*colliderThat);
//
//					if (CheckSphereCollision(thisEntity, thatEntity))
//					{
//						if (CheckAABBCollision(thisEntity, thatEntity))
//						{
//							thisEntity->SetIsDone(true);
//							thatEntity->SetIsDone(true);
//						}
//					}
//				}
//			}
//		}
//	}
//	return false;
//}

bool EntityManager::CheckForCollision()
{
	std::list<EntityBase*>::iterator projectileCollider;

	projectileCollider = projectileList.begin();
	while (projectileCollider != projectileList.end())
	{
		EntityBase *proj = dynamic_cast<EntityBase*>(*projectileCollider);

		if (!proj->HasCollider())
		{
			++projectileCollider;
			//continue;
		}
		else
		{
			if (proj->GetIsLaser())
			{
				CLaser* laser = dynamic_cast<CLaser*>(proj);

				vector<EntityBase*> exportList = CSpatialPartition::GetInstance()->GetObjects(proj->GetPosition(), 20.0f, true);

				//std::cout << exportList.size() << std::endl;
				for (int i = 0; i < (int)exportList.size(); ++i)
				{
					if (!exportList[i]->IsDone() && exportList[i]->HasCollider() && exportList[i]->typeofent != EntityBase::T_NORMAL)
					{
						CSceneNode* temp;
						temp = CSceneGraph::GetInstance()->GetNode(exportList[i]);	//use this for the checking of what limb is hit
						if ((temp->GetNumOfChild() > 1))
						{
							Vector3 hitPos = Vector3(0, 0, 0);

							Mtx44 trn = CSceneGraph::GetInstance()->GetNode(exportList[i])->TheUltimateTransformation();
							CCollider *otherCol = dynamic_cast<CCollider*>(exportList[i]);
							Vector3 otherMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMinAABB();
							Vector3 otherMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMaxAABB();

							//std::cout << "working" << std::endl;

							if (CheckLineSegmentPlane(
								laser->GetPosition() + laser->GetDirection() * laser->GetLength(),
								laser->GetPosition(),
								otherMinAABB,
								otherMaxAABB,
								hitPos) == true)
							{
								GetChildCheck(temp, laser);
								//exportList[i]->SetIsDone(true);
								//CSpatialPartition::GetInstance()->Remove(exportList[i]);
								//CSceneGraph::GetInstance()->DeleteNode(exportList[i]);
							}
						}
						else
						{
							Vector3 hitPos = Vector3(0, 0, 0);

							Mtx44 trn = CSceneGraph::GetInstance()->GetNode(exportList[i])->TheUltimateTransformation();
							CCollider *otherCol = dynamic_cast<CCollider*>(exportList[i]);
							Vector3 otherMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMinAABB();
							Vector3 otherMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMaxAABB();

							//std::cout << "working" << std::endl;

							if (CheckLineSegmentPlane(
								laser->GetPosition() + laser->GetDirection() * laser->GetLength(),
								laser->GetPosition(),
								otherMinAABB,
								otherMaxAABB,
								hitPos) == true)
							{
								if (exportList[i]->CanItBeSetToDone())
								{
									exportList[i]->SetIsDone(true);
									CSpatialPartition::GetInstance()->Remove(exportList[i]);
									CSceneGraph::GetInstance()->DeleteNode(exportList[i]);
								}
							}
						}
					}
				}

				//std::list<EntityBase*>::iterator it, end;
				//end = entityList.end();
				//for (it = entityList.begin(); it != end; ++it)
				//{
				//	if ((*it)->HasCollider())
				//	{
				//		Vector3 hitPos = Vector3(0, 0, 0);

				//		CCollider *otherCol = dynamic_cast<CCollider*>(*it);
				//		Vector3 otherMinAABB = (*it)->GetPosition() + otherCol->GetMinAABB();
				//		Vector3 otherMaxAABB = (*it)->GetPosition() + otherCol->GetMaxAABB();

				//		if (CheckLineSegmentPlane(
				//			laser->GetPosition() + laser->GetDirection() * laser->GetLength(),
				//			laser->GetPosition(),
				//			otherMinAABB,
				//			otherMaxAABB,
				//			hitPos) == true)
				//		{
				//			//proj->SetIsDone(true);
				//			proj->SetIsDone(true);
				//			(*it)->SetPosition((*it)->GetPosition() + Vector3(10, 10, 10));

				//		}
				//	}
				//}

			}
			else
			{
				vector<EntityBase*> exportList = CSpatialPartition::GetInstance()->GetObjects(proj->GetPosition(), 20.0f);

				for (int i = 0; i < (int)exportList.size(); ++i)
				{
					if (!exportList[i]->IsDone() && exportList[i]->HasCollider() && exportList[i]->typeofent != EntityBase::T_NORMAL)
					{
						if (CheckSphereCollision(proj, exportList[i]))
						{
							if (CheckAABBCollision(proj, exportList[i]))
							{
								if (exportList[i]->CanItBeSetToDone())
								{
									proj->SetIsDone(true);
									exportList[i]->SetIsDone(true);
								}
								Mtx44 trn = CSceneGraph::GetInstance()->GetNode(exportList[i])->TheUltimateTransformation();
								CSpatialPartition::GetInstance()->RadialExplosion(Vector3(trn.a[12], trn.a[13], trn.a[14]), 50);
							
								CSpatialPartition::GetInstance()->Remove(exportList[i]);
								CSceneGraph::GetInstance()->DeleteNode(exportList[i]);

							}
						}
					}
				}

				//std::list<EntityBase*>::iterator it, end;
				//end = entityList.end();
				//for (it = entityList.begin(); it != end; ++it)
				//{
				//	if ((*it)->HasCollider())
				//	{
				//		if (CheckSphereCollision(proj, *it))
				//		{
				//			if (CheckAABBCollision(proj, *it))
				//			{
				//				proj->SetIsDone(true);
				//				(*it)->SetPosition((*it)->GetPosition() + Vector3(10, 10, 10));
				//				
				//				//(*it)->SetIsDone(true);
				//				//CSpatialPartition::GetInstance()->Remove(*it);
				//				//CSceneGraph::GetInstance()->DeleteNode(*it);
				//			}
				//		}
				//	}
				//}

			}
			++projectileCollider;
		}
	}
	return false;
}

bool EntityManager::AABBCollide(const Vector3& point)
{
	vector<EntityBase*> exportList = CSpatialPartition::GetInstance()->GetObjects(point, 20.0f);

	for (int i = 0; i < (int)exportList.size(); ++i)
	{
		if (exportList[i]->HasCollider() && exportList[i]->typeofent != EntityBase::T_ENEMY)
		{
			if (CheckAABBPointCollision(point, exportList[i]))
			{
				switch (exportList[i]->typeofent)
				{
					case EntityBase::T_NORMAL:
					{
						return true;
					}
					case EntityBase::T_DESTRUCTIBLE:
					{
						return true;
					}
					case EntityBase::T_TRIGGER:
					{
						exportList[i]->AssociatedTrigger->SetEvent(true);
					}
				}
			}
		}
	}
	return false;
}

bool EntityManager::GetIntersection(const float fDst1, const float fDst2, Vector3 P1, Vector3 P2, Vector3 &Hit)
{
	if ((fDst1 * fDst2) >= 0.0f) return false;
	if (Math::FAbs(fDst1 - fDst2) <= Math::EPSILON) return false;

	Hit = P1 + (P2 - P1) * (-fDst1 / (fDst2 - fDst1));
	return true;
}

bool EntityManager::InBox(Vector3 Hit, Vector3 B1, Vector3 B2, const int Axis)
{
	if (Axis == 1 && Hit.z > B1.z && Hit.z < B2.z && Hit.y > B1.y && Hit.y < B2.y) return true;
	if (Axis == 2 && Hit.z > B1.z && Hit.z < B2.z && Hit.x > B1.x && Hit.x < B2.x) return true;
	if (Axis == 3 && Hit.x > B1.x && Hit.x < B2.x && Hit.y > B1.y && Hit.y < B2.y) return true;
	return false;
}

bool EntityManager::CheckLineSegmentPlane(Vector3 line_start, Vector3 line_end, Vector3 minAABB, Vector3 maxAABB, Vector3 &Hit)
{
	if ((GetIntersection(line_start.x - minAABB.x, line_end.x - minAABB.x, line_start, line_end, Hit) &&
		InBox(Hit, minAABB, maxAABB, 1))
		|| (GetIntersection(line_start.y - minAABB.y, line_end.y - minAABB.y, line_start, line_end, Hit) &&
			InBox(Hit, minAABB, maxAABB, 2))
		|| (GetIntersection(line_start.z - minAABB.z, line_end.z - minAABB.z, line_start, line_end, Hit) &&
			InBox(Hit, minAABB, maxAABB, 3))
		|| (GetIntersection(line_start.x - maxAABB.x, line_end.x - maxAABB.x, line_start, line_end, Hit) &&
			InBox(Hit, minAABB, maxAABB, 1))
		|| (GetIntersection(line_start.y - maxAABB.y, line_end.y - maxAABB.y, line_start, line_end, Hit) &&
			InBox(Hit, minAABB, maxAABB, 2))
		|| (GetIntersection(line_start.z - maxAABB.z, line_end.z - maxAABB.z, line_start, line_end, Hit) &&
			InBox(Hit, minAABB, maxAABB, 3))
		) return true;

	return false;
}

CSceneNode* EntityManager::GetChildCheck(CSceneNode* node, CLaser* laser)
{
	for (std::map<EntityBase*, CSceneNode*>::iterator it = node->childMap.begin(); it != node->childMap.end(); ++it)
	{
		CSceneNode* Test;
		Test = GetChildCheck(it->second, laser);
		if (Test != nullptr)
		{
			node->DeleteChild(Test->GetEntity());
			//std::cout << "god" << std::endl;
			break;
		}
	}

	if ((node->GetParent()->GetID() != 0) && (node->GetNumOfChild() == 0))
	{
		Vector3 hitPos = Vector3(0, 0, 0);

		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(node->GetID())->TheUltimateTransformation();

		CCollider *otherCol = dynamic_cast<CCollider*>(node->GetEntity());
		Vector3 otherMinAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMinAABB();
		Vector3 otherMaxAABB = Vector3(trn.a[12], trn.a[13], trn.a[14]) + otherCol->GetMaxAABB();

		//std::cout << "working" << std::endl;

		if (CheckLineSegmentPlane(
			laser->GetPosition() + laser->GetDirection() * laser->GetLength(),
			laser->GetPosition(),
			otherMinAABB,
			otherMaxAABB,
			hitPos) == true)
		{
			std::cout << "die" << std::endl;
			return node;
		}
	}
	else
	{
		return NULL;
	}
}