#include "ShopThings.h"
#include "Lua/LuaInterface.h"

void ShopThings::Shop_ReadLua()
{
	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_SHOPDATA_VARIABLES.lua", CLuaInterface::GetInstance()->theLuaState);

	Currency = CLuaInterface::GetInstance()->getIntValue("money");
	ScannerBought = CLuaInterface::GetInstance()->getIntValue("scannerBought");
	OmegacannonBought = CLuaInterface::GetInstance()->getIntValue("omegacannonBought");
	RobotBought = CLuaInterface::GetInstance()->getIntValue("robotBought");
	ScannerCost = CLuaInterface::GetInstance()->getIntValue("scannerCost");
	OmegacannonCost = CLuaInterface::GetInstance()->getIntValue("omegacannonCost");
	RobotCost = CLuaInterface::GetInstance()->getIntValue("robotCost");

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);
}

void ShopThings::Shop_OverwriteLua()
{
	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_MYSAVEFUNCTIONS.lua", CLuaInterface::GetInstance()->theLuaState);

	CLuaInterface::GetInstance()->saveIntValue("money", Currency, "SaveShopInfo", true);
	CLuaInterface::GetInstance()->saveIntValue("scannerBought", (int)ScannerBought, "SaveShopInfo", false);
	CLuaInterface::GetInstance()->saveIntValue("omegacannonBought", (int)OmegacannonBought, "SaveShopInfo", false);
	CLuaInterface::GetInstance()->saveIntValue("robotBought", (int)RobotBought, "SaveShopInfo", false);
	CLuaInterface::GetInstance()->saveIntValue("scannerCost", (int)ScannerCost, "SaveShopInfo", false);
	CLuaInterface::GetInstance()->saveIntValue("omegacannonCost", (int)OmegacannonCost, "SaveShopInfo", false);
	CLuaInterface::GetInstance()->saveIntValue("robotCost", (int)RobotCost, "SaveShopInfo", false);

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);
}