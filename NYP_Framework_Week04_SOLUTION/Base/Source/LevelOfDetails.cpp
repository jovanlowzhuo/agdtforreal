#include "LevelOfDetails.h"
#include "MeshBuilder.h"

CLevelOfDetails::CLevelOfDetails()
	: modelMesh_HighDetails(NULL)
	, modelMesh_MidDetails(NULL)
	, modelMesh_LowDetails(NULL)
	, m_Active(false)
	, theDetailLevel(HIGH_DETAILS)
{
}

CLevelOfDetails::~CLevelOfDetails()
{
	modelMesh_HighDetails = NULL;
	modelMesh_MidDetails = NULL;
	modelMesh_LowDetails = NULL;
}

bool CLevelOfDetails::InitLOD(const std::string& high, const std::string& mid, const std::string& low)
{
	modelMesh_HighDetails = MeshBuilder::GetInstance()->GetMesh(high);
	modelMesh_MidDetails = MeshBuilder::GetInstance()->GetMesh(mid);
	modelMesh_LowDetails = MeshBuilder::GetInstance()->GetMesh(low);

	if ((modelMesh_HighDetails == nullptr) ||
		(modelMesh_MidDetails == nullptr) ||
		(modelMesh_LowDetails == nullptr))
		return false;

	SetLODStatus(true);
	return true;
}

bool CLevelOfDetails::DestroyLOD()
{
	if (modelMesh_HighDetails)
	{
		delete modelMesh_HighDetails;
		modelMesh_HighDetails = NULL;
	}
	if (modelMesh_MidDetails)
	{
		delete modelMesh_MidDetails;
		modelMesh_MidDetails = NULL;
	}
	if (modelMesh_LowDetails)
	{
		delete modelMesh_LowDetails;
		modelMesh_LowDetails = NULL;
	}
	return false;
}

void CLevelOfDetails::SetLODStatus(const bool bActive)
{
	m_Active = bActive;
}

bool CLevelOfDetails::GetLODStatus() const
{
	return m_Active;
}

bool CLevelOfDetails::SetLODMesh(Mesh* theMesh, const DETAIL_LEVEL detailLevel)
{
	if (theDetailLevel == HIGH_DETAILS)
		modelMesh_HighDetails = theMesh;
	else if (theDetailLevel == MID_DETAILS)
		modelMesh_MidDetails = theMesh;
	else if (theDetailLevel == LOW_DETAILS)
		modelMesh_LowDetails = theMesh;
	else return false;

	return true;
}

Mesh* CLevelOfDetails::GetLODMesh() const
{
	if (theDetailLevel == HIGH_DETAILS)
		return modelMesh_HighDetails;
	else if (theDetailLevel == MID_DETAILS)
		return modelMesh_MidDetails;
	else if (theDetailLevel == LOW_DETAILS)
		return modelMesh_LowDetails;
	else return false;

	return NULL;
}

Mesh* CLevelOfDetails::GetLODMesh(const DETAIL_LEVEL detailLevel) const
{
	if (detailLevel == HIGH_DETAILS)
		return modelMesh_HighDetails;
	else if (detailLevel == MID_DETAILS)
		return modelMesh_MidDetails;
	else if (detailLevel == LOW_DETAILS)
		return modelMesh_LowDetails;
	else return false;

	return NULL;
}

int CLevelOfDetails::GetDetailLevel() const
{
	return theDetailLevel;
}

bool CLevelOfDetails::SetDetailLevel(const DETAIL_LEVEL detailLevel)
{
	if ((detailLevel >= NO_DETAILS) && (detailLevel < NUM_DETAIL_LEVEL))
	{
		theDetailLevel = detailLevel;
		return true;
	}

	return false;
}
