#pragma once
#include "Mesh.h"

class CLevelOfDetails
{
public:
	Mesh* modelMesh_HighDetails;
	Mesh* modelMesh_MidDetails;
	Mesh* modelMesh_LowDetails;

	enum DETAIL_LEVEL
	{
		NO_DETAILS = 0,
		HIGH_DETAILS,
		MID_DETAILS,
		LOW_DETAILS,
		NUM_DETAIL_LEVEL
	};

	CLevelOfDetails();
	virtual ~CLevelOfDetails();

	bool InitLOD(const std::string& high, const std::string& mid, const std::string& low);
	bool DestroyLOD();

	void SetLODStatus(const bool bActive);
	bool GetLODStatus() const;

	bool SetLODMesh(Mesh* theMesh, const DETAIL_LEVEL detailLevel);
	Mesh* GetLODMesh() const;
	Mesh* GetLODMesh(const DETAIL_LEVEL detailLevel) const;
	int GetDetailLevel() const;
	bool SetDetailLevel(const DETAIL_LEVEL detailLevel);

protected:
	bool m_Active;
	DETAIL_LEVEL theDetailLevel;

};
