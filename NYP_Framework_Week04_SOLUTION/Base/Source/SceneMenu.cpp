#include "SceneMenu.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include <iostream>
using namespace std;

SceneMenu* SceneMenu::sInstance = new SceneMenu(SceneManager::GetInstance());

SceneMenu::SceneMenu()
	: theMouse(NULL)
	, theKeyboard(NULL)
{
}

SceneMenu::SceneMenu(SceneManager* _sceneMgr)
	: theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Menu", this);
}

SceneMenu::~SceneMenu()
{
	//if (theCameraEffects)
	//{
	//	delete theCameraEffects;
	//	theCameraEffects = NULL;
	//}
	///*if (theMinimap)
	//{
	//	delete theMinimap;
	//	theMinimap = NULL;
	//}*/
	if (theMouse)
	{
		delete theMouse;
		theMouse = NULL;
	}
	if (theKeyboard)
	{
		delete theKeyboard;
		theKeyboard = NULL;
	}

	EntityManager::GetInstance()->Destroy();
}

void SceneMenu::Init()
{
	//srand(NULL);

	currProg = GraphicsManager::GetInstance()->GetActiveShader();

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("textEnabled", 0);

	// Create and attach the camera to the scene
	//camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	//camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	//orthoCam.Init(Vector3(0, 10, 0), Vector3(0, 15, 10), Vector3(0, 1, 0));
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup the 2D entities
	float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	float fontSize = 25.0f;
	float halfFontSize = fontSize / 2.0f;
	for (int i = 0; i < 3; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight + fontSize * i + halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	}
	textObj[0]->SetText("HELLO WORLD");

	// Activate the Blend Function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//// Hardware Abstraction
	//theKeyboard = new CKeyboard();
	//theKeyboard->Create(playerInfo);

	//theMouse = new CMouse();
	//theMouse->Create(playerInfo);

	Create::Sprite2DObject("menuscreen", Vector3(0.0f, 0.0f, 0.0f), Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()), true);
	
	//0 - Start game 
	//1 - Help menu
	//2 - Exit
	
	Buttons[0] = Create::Sprite2DObject("button_notpressed", Vector3(0, 0.15f * Application::GetInstance().GetWindowHeight(), 1.0f), Vector3(0.45 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()), true);
	Buttons[1] = Create::Sprite2DObject("button_notpressed", Vector3(0, -0.15f * Application::GetInstance().GetWindowHeight(), 1.0f), Vector3(0.4 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()), true);
	Buttons[2] = Create::Sprite2DObject("button_notpressed", Vector3(0, -0.45f * Application::GetInstance().GetWindowHeight(), 1.0f), Vector3(0.4 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()), true);

	//Graph::GetInstance()->LoadCSV("Image//graph.csv");
	////Graph::GetInstance()->Generate(0, 50, 8);
	//std::vector<Node*> test = Graph::GetInstance()->PathFinding(Vector3(0, 0, 0), Vector3(0, 90, 0));
	////if (test.size() > 0)
	////{
	////	for (int i = 0; i < test.size(); ++i)
	////	{
	////		std::cout << test[i]->pos << std::endl;
	////	}
	////}
}

void SceneMenu::Update(double dt)
{
	// Update our entities
	EntityManager::GetInstance()->Update(dt);

	// THIS WHOLE CHUNK TILL <THERE> CAN REMOVE INTO ENTITIES LOGIC! Or maybe into a scene function to keep the update clean
	if (KeyboardController::GetInstance()->IsKeyDown('1'))
		glEnable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('2'))
		glDisable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (KeyboardController::GetInstance()->IsKeyDown('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//if (KeyboardController::GetInstance()->IsKeyPressed('H'))
	//{
	//	//SceneManager::GetInstance()->SetActiveScene("Start2");
	//	SceneManager::GetInstance()->EditSceneAABB("Start2", 0.6, 0, 1.0, 0.5); // 4:3 ratio?
	//	SceneManager::GetInstance()->AddSubScene("Start2");
	//}

	if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB))
	{
		cout << "Left Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::RMB))
	{
		cout << "Right Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::MMB))
	{
		cout << "Middle Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in X-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in Y-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) << endl;
	}
	// <THERE>

	//// Hardware Abstraction
	//theKeyboard->Read((float)dt);
	//theMouse->Read((float)dt);

	double MousePositionX = 0, MousePositionY = 0;
	MousePositionX = Application::GetInstance().GetMousePos().first;
	MousePositionY = Application::GetInstance().GetMousePos().second;

	//MousePositionX = MousePositionX * (1000 / (double)Application::GetInstance().GetWindowWidth()) - 500;
	//MousePositionY = MousePositionY * (1000 / (double)Application::GetInstance().GetWindowHeight()) - 500;
	
	//MousePositionX = MousePositionX - 0.5 * (double)Application::GetInstance().GetWindowWidth();
	//MousePositionY = MousePositionY - 0.5 * (double)Application::GetInstance().GetWindowHeight();
	MousePositionX = MousePositionX - 0.5 * (double)SceneManager::GetInstance()->windowWidth;
	MousePositionY = MousePositionY - 0.5 * (double)SceneManager::GetInstance()->windowHeight; //? idk if order correct
	MousePositionY *= -1;

	for (int i = 0; i < 3 && CurrentScreen; ++i)
	{
		Vector3 minAABB_button = -Buttons[i]->GetScale() * 0.5 + Buttons[i]->GetPosition();
		minAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		minAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();
		Vector3 maxAABB_button = Buttons[i]->GetScale() * 0.5 + Buttons[i]->GetPosition();
		maxAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		maxAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();
		
		//std::cout << minAABB_button << " " << maxAABB_button << std::endl;
		if (MousePositionX >= minAABB_button.x && MousePositionY >= minAABB_button.y && MousePositionX <= maxAABB_button.x && MousePositionY <= maxAABB_button.y)
		{
			//std::cout << i << std::endl;
			Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");

			if (i == 0)
				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
			{
				SceneManager::GetInstance()->SetActiveScene("Start");

			}

			if (i == 1)
				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
				{
					
					SceneManager::GetInstance()->AddSubScene("Shop");
				}

			if (i == 2)
				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
			{
				SceneManager::GetInstance()->AddSubScene("ExitNotif");
			}

		}
		else
		{
			Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_notpressed");
		}
	}

	GraphicsManager::GetInstance()->UpdateLights(dt);

	// Update the 2 text object values
	// Eg. FPSRenderEntity or inside RenderUI for LightEntity
	DisplayText.str("");
	DisplayText.clear();
	DisplayText << "FPS: " << CFPSCounter::GetInstance()->GetFrameRate() << endl;
	textObj[1]->SetText(DisplayText.str());

	//DisplayText.str("");
	//DisplayText.clear();
	//DisplayText << "Sway:" << playerInfo->m_fCameraSwayAngle;
	//textObj[2]->SetText(DisplayText.str());

}

void SceneMenu::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	Mesh* crosshair = MeshBuilder::GetInstance()->GetMesh("crosshair");

	modelStack.PushMatrix();
	//Vector3 tempdirplayer = (camera.GetCameraTarget() - camera.GetCameraPos()).Normalized();
	//Vector3 temprightplayer = (tempdirplayer.Cross(camera.GetCameraUp())).Normalized();
	//Vector3 tempupplayer = (camera.GetCameraUp()).Normalized();
	//modelStack.Translate(temprightplayer.x * 100, tempupplayer.y * 100, temprightplayer.z * 100); //weird thing

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -6.0f);
	modelStack.Scale(0.25, 0.25f, 0.25f);
	RenderHelper::RenderMeshGun(crosshair);
	modelStack.PopMatrix();

	// PreRenderMesh
	RenderHelper::PreRenderMesh();
	EntityManager::GetInstance()->Render(); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Enable blend mode
	glEnable(GL_BLEND);

	GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, 0.1, 1000);
	GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// PreRenderText
	RenderHelper::PreRenderText();

	EntityManager::GetInstance()->RenderUI();

	// PostRenderText
	RenderHelper::PostRenderText();

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	modelStack.PopMatrix();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneMenu::Exit()
{
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	//delete lights[0];
	//delete lights[1];
	EntityManager::GetInstance()->Destroy();
}
