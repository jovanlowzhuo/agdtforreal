#include "SceneSplash.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include <iostream>
using namespace std;

SceneSplash* SceneSplash::sInstance = new SceneSplash(SceneManager::GetInstance());

SceneSplash::SceneSplash()
	: theMouse(NULL)
	, theKeyboard(NULL)
{
}

SceneSplash::SceneSplash(SceneManager* _sceneMgr)
	: theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Splash", this);
}

SceneSplash::~SceneSplash()
{
	//if (theCameraEffects)
	//{
	//	delete theCameraEffects;
	//	theCameraEffects = NULL;
	//}
	///*if (theMinimap)
	//{
	//	delete theMinimap;
	//	theMinimap = NULL;
	//}*/
	if (theMouse)
	{
		delete theMouse;
		theMouse = NULL;
	}
	if (theKeyboard)
	{
		delete theKeyboard;
		theKeyboard = NULL;
	}

	EntityManager::GetInstance()->Destroy();
}

void SceneSplash::Init()
{
	//srand(NULL);

	currProg = GraphicsManager::GetInstance()->GetActiveShader();

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("textEnabled", 0);

	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup the 2D entities
	float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	float fontSize = 50.0f;
	float halfFontSize = fontSize / 2.0f;

	textObj = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight * 0.5, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	textObj->SetText("Click to Begin!!!");

	// Activate the Blend Function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	Create::Sprite2DObject("splashscreen", Vector3(0.0f, 0.0f, 0.0f), Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()), true);

	clickedOnScreen = false;

	TimeToEnterGame = 3.0;

}

void SceneSplash::Update(double dt)
{
	// Update our entities
	EntityManager::GetInstance()->Update(dt);

	// THIS WHOLE CHUNK TILL <THERE> CAN REMOVE INTO ENTITIES LOGIC! Or maybe into a scene function to keep the update clean
	if (KeyboardController::GetInstance()->IsKeyDown('1'))
		glEnable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('2'))
		glDisable(GL_CULL_FACE);
	if (KeyboardController::GetInstance()->IsKeyDown('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (KeyboardController::GetInstance()->IsKeyDown('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if (KeyboardController::GetInstance()->IsKeyDown('5'))
	{
		lights[0]->type = Light::LIGHT_POINT;
	}
	else if (KeyboardController::GetInstance()->IsKeyDown('6'))
	{
		lights[0]->type = Light::LIGHT_DIRECTIONAL;
	}
	else if (KeyboardController::GetInstance()->IsKeyDown('7'))
	{
		lights[0]->type = Light::LIGHT_SPOT;
	}

	//if (KeyboardController::GetInstance()->IsKeyPressed('H'))
	//{
	//	//SceneManager::GetInstance()->SetActiveScene("Start2");
	//	SceneManager::GetInstance()->EditSceneAABB("Start2", 0.6, 0, 1.0, 0.5); // 4:3 ratio?
	//	SceneManager::GetInstance()->AddSubScene("Start2");
	//}

	if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB))
	{
		cout << "Left Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::RMB))
	{
		cout << "Right Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::MMB))
	{
		cout << "Middle Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in X-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != 0.0)
	{
		//cout << "Mouse Wheel has offset in Y-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) << endl;
	}
	// <THERE>

	if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
	{
		if (!clickedOnScreen)
		{
			clickedOnScreen = true;
		}
	}

	if (clickedOnScreen)
	{
		TimeToEnterGame -= dt;

		if (amountofdots >= 8)
		{
			loadingtext = "Loading";
			amountofdots = 0;
		}
		else
		{
			loadingtext += ".";
			++amountofdots;
		}

		textObj->SetText(loadingtext);
	}

	if (TimeToEnterGame <= 0.0)
	{
		SceneManager::GetInstance()->SetActiveScene("Menu");
	}

	//// Hardware Abstraction
	//theKeyboard->Read((float)dt);
	//theMouse->Read((float)dt);

	double MousePositionX = 0, MousePositionY = 0;
	MousePositionX = Application::GetInstance().GetMousePos().first;
	MousePositionY = Application::GetInstance().GetMousePos().second;

	//MousePositionX = MousePositionX * (1000 / (double)Application::GetInstance().GetWindowWidth()) - 500;
	//MousePositionY = MousePositionY * (1000 / (double)Application::GetInstance().GetWindowHeight()) - 500;

	//MousePositionX = MousePositionX - 0.5 * (double)Application::GetInstance().GetWindowWidth();
	//MousePositionY = MousePositionY - 0.5 * (double)Application::GetInstance().GetWindowHeight();
	MousePositionX = MousePositionX - 0.5 * (double)SceneManager::GetInstance()->windowWidth;
	MousePositionY = MousePositionY - 0.5 * (double)SceneManager::GetInstance()->windowHeight; //? idk if order correct
	MousePositionY *= -1;

	GraphicsManager::GetInstance()->UpdateLights(dt);


}

void SceneSplash::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	Mesh* crosshair = MeshBuilder::GetInstance()->GetMesh("crosshair");

	modelStack.PushMatrix();
	//Vector3 tempdirplayer = (camera.GetCameraTarget() - camera.GetCameraPos()).Normalized();
	//Vector3 temprightplayer = (tempdirplayer.Cross(camera.GetCameraUp())).Normalized();
	//Vector3 tempupplayer = (camera.GetCameraUp()).Normalized();
	//modelStack.Translate(temprightplayer.x * 100, tempupplayer.y * 100, temprightplayer.z * 100); //weird thing

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -6.0f);
	modelStack.Scale(0.25, 0.25f, 0.25f);
	RenderHelper::RenderMeshGun(crosshair);
	modelStack.PopMatrix();

	// PreRenderMesh
	RenderHelper::PreRenderMesh();
	EntityManager::GetInstance()->Render(); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Enable blend mode
	glEnable(GL_BLEND);

	GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, 0.1, 1000);
	GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// PreRenderText
	RenderHelper::PreRenderText();

	EntityManager::GetInstance()->RenderUI();

	// PostRenderText
	RenderHelper::PostRenderText();

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	modelStack.PopMatrix();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneSplash::Exit()
{
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	//delete lights[0];
	//delete lights[1];
	EntityManager::GetInstance()->Destroy();
}
