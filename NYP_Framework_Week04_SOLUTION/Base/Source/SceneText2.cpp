#include "SceneText2.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"
#include "WaypointGraph.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include <iostream>
using namespace std;

SceneText2* SceneText2::sInstance = new SceneText2(SceneManager::GetInstance());

SceneText2::SceneText2()
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
}

SceneText2::SceneText2(SceneManager* _sceneMgr)
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Start2", this);
}

SceneText2::~SceneText2()
{

	//if (theMouse)
	//{
	//	delete theMouse;
	//	theMouse = NULL;
	//}
	//if (theKeyboard)
	//{
	//	delete theKeyboard;
	//	theKeyboard = NULL;
	//}
	//// Delete the scene graph
	//CSceneGraph::GetInstance()->Destroy();
	//// Delete the Spatial Partition
	//CSpatialPartition::GetInstance()->Destroy();
	//EntityManager::GetInstance()->Destroy();
}

void SceneText2::Init()
{
	//srand(NULL);

	//currProg = GraphicsManager::GetInstance()->LoadShader("default2", "Shader//comg.vertexshader", "Shader//comg.fragmentshader");

	////GraphicsManager::GetInstance()->SetActiveShader("default");
	//currProg = GraphicsManager::GetInstance()->GetActiveShader();

	//// Tell the shader program to store these uniform locations
	//currProg->AddUniform("MVP");
	//currProg->AddUniform("MV");
	//currProg->AddUniform("MV_inverse_transpose");
	//currProg->AddUniform("material.kAmbient");
	//currProg->AddUniform("material.kDiffuse");
	//currProg->AddUniform("material.kSpecular");
	//currProg->AddUniform("material.kShininess");
	//currProg->AddUniform("lightEnabled");
	//currProg->AddUniform("numLights");
	//currProg->AddUniform("lights[0].type");
	//currProg->AddUniform("lights[0].position_cameraspace");
	//currProg->AddUniform("lights[0].color");
	//currProg->AddUniform("lights[0].power");
	//currProg->AddUniform("lights[0].kC");
	//currProg->AddUniform("lights[0].kL");
	//currProg->AddUniform("lights[0].kQ");
	//currProg->AddUniform("lights[0].spotDirection");
	//currProg->AddUniform("lights[0].cosCutoff");
	//currProg->AddUniform("lights[0].cosInner");
	//currProg->AddUniform("lights[0].exponent");
	//currProg->AddUniform("lights[1].type");
	//currProg->AddUniform("lights[1].position_cameraspace");
	//currProg->AddUniform("lights[1].color");
	//currProg->AddUniform("lights[1].power");
	//currProg->AddUniform("lights[1].kC");
	//currProg->AddUniform("lights[1].kL");
	//currProg->AddUniform("lights[1].kQ");
	//currProg->AddUniform("lights[1].spotDirection");
	//currProg->AddUniform("lights[1].cosCutoff");
	//currProg->AddUniform("lights[1].cosInner");
	//currProg->AddUniform("lights[1].exponent");
	//currProg->AddUniform("colorTextureEnabled");
	//currProg->AddUniform("colorTexture");
	//currProg->AddUniform("textEnabled");
	//currProg->AddUniform("textColor");

	//currProg->AddUniform("X_MIN");
	//currProg->AddUniform("X_MAX");
	//currProg->AddUniform("Y_MIN");
	//currProg->AddUniform("Y_MAX");
	//currProg->AddUniform("stretch");

	//// Tell the graphics manager to use the shader we just loaded
	//GraphicsManager::GetInstance()->SetActiveShader("default");

	//lights[0] = new Light();
	//GraphicsManager::GetInstance()->AddLight("lights[0]", lights[0]);
	//lights[0]->type = Light::LIGHT_DIRECTIONAL;
	//lights[0]->position.Set(0, 20, 0);
	//lights[0]->color.Set(1, 1, 1);
	//lights[0]->power = 1;
	//lights[0]->kC = 1.f;
	//lights[0]->kL = 0.01f;
	//lights[0]->kQ = 0.001f;
	//lights[0]->cosCutoff = cos(Math::DegreeToRadian(45));
	//lights[0]->cosInner = cos(Math::DegreeToRadian(30));
	//lights[0]->exponent = 3.f;
	//lights[0]->spotDirection.Set(0.f, 1.f, 0.f);
	//lights[0]->name = "lights[0]";

	//lights[1] = new Light();
	//GraphicsManager::GetInstance()->AddLight("lights[1]", lights[1]);
	//lights[1]->type = Light::LIGHT_DIRECTIONAL;
	//lights[1]->position.Set(1, 1, 0);
	//lights[1]->color.Set(1, 1, 0.5f);
	//lights[1]->power = 0.4f;
	//lights[1]->name = "lights[1]";

	//currProg->UpdateInt("numLights", 1);
	//currProg->UpdateInt("textEnabled", 0);

	//// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	//playerInfo->Init();

	// Create and attach the camera to the scene
	//camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	//camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	//playerInfo->AttachCamera(&camera);
	//GraphicsManager::GetInstance()->AttachCamera(&camera);

	//if (loadShader == false)
	//{
	//	// Load all the meshes
	//	MeshBuilder::GetInstance()->GenerateAxes("reference");
	//	MeshBuilder::GetInstance()->GenerateCrossHair("crosshair");
	//	MeshBuilder::GetInstance()->GenerateQuad("quad", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("quad")->textureID = LoadTGA("Image//calibri.tga");
	//	MeshBuilder::GetInstance()->GenerateText("text", 16, 16);
	//	MeshBuilder::GetInstance()->GetMesh("text")->textureID = LoadTGA("Image//calibri.tga");
	//	MeshBuilder::GetInstance()->GetMesh("text")->material.kAmbient.Set(1, 0, 0);
	//	MeshBuilder::GetInstance()->GenerateOBJ("Chair", "OBJ//chair.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Chair")->textureID = LoadTGA("Image//chair.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("Enemy_HighLOD", "OBJ//Scooter.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Enemy_HighLOD")->textureID = LoadTGA("Image//floorHighLOD.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("Enemy_MidLOD", "OBJ//scootermid.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Enemy_MidLOD")->textureID = LoadTGA("Image//floorMidLOD.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("Enemy_LowLOD", "OBJ//scooterlow.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Enemy_LowLOD")->textureID = LoadTGA("Image//floorLowLOD.tga");
	//	MeshBuilder::GetInstance()->GenerateRing("ring", Color(1, 0, 1), 36, 1, 0.5f);
	//	MeshBuilder::GetInstance()->GenerateSphere("lightball", Color(1, 1, 1), 18, 36, 1.f);
	//	MeshBuilder::GetInstance()->GenerateSphere("sphere", Color(1, 0, 0), 18, 36, 1.f);

	//	MeshBuilder::GetInstance()->GenerateSphere("HighLOD", Color(1, 0, 0), 6, 12, 5.f);
	//	MeshBuilder::GetInstance()->GenerateSphere("MidLOD", Color(1, 1, 0), 6, 12, 5.f);
	//	MeshBuilder::GetInstance()->GenerateSphere("LowLOD", Color(0, 1, 0), 6, 12, 5.f);
	//	MeshBuilder::GetInstance()->GenerateSphere("PlayerPoint", Color(0, 0, 1), 6, 12, 5.f);

	//	MeshBuilder::GetInstance()->GenerateCone("cone", Color(0.5f, 1, 0.3f), 36, 10.f, 10.f);
	//	MeshBuilder::GetInstance()->GenerateCube("cube", Color(1.0f, 1.0f, 0.0f), 1.0f);
	//	MeshBuilder::GetInstance()->GetMesh("cone")->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);
	//	MeshBuilder::GetInstance()->GetMesh("cone")->material.kSpecular.Set(0.f, 0.f, 0.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("GRASS_DARKGREEN", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("GRASS_DARKGREEN")->textureID = LoadTGA("Image//grass_darkgreen.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("GEO_GRASS_LIGHTGREEN", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("GEO_GRASS_LIGHTGREEN")->textureID = LoadTGA("Image//grass_lightgreen.tga");

	//	MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_HighLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("MantisSkin_HighLOD")->textureID = LoadTGA("Image//HighLODMantis.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_MidLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("MantisSkin_MidLOD")->textureID = LoadTGA("Image//MidLODMantis.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_LowLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("MantisSkin_LowLOD")->textureID = LoadTGA("Image//LowLODMantis.tga");

	//	MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_HighLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("SpiderSkin_HighLOD")->textureID = LoadTGA("Image//HighLODSpider.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_MidLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("SpiderSkin_MidLOD")->textureID = LoadTGA("Image//MidLODSpider.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_LowLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("SpiderSkin_LowLOD")->textureID = LoadTGA("Image//LowLODSpider.tga");

	//	MeshBuilder::GetInstance()->GenerateOBJ("Metal_HighLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Metal_HighLOD")->textureID = LoadTGA("Image//HighLODMetal.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("Metal_MidLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Metal_MidLOD")->textureID = LoadTGA("Image//MidLODMetal.tga");
	//	MeshBuilder::GetInstance()->GenerateOBJ("Metal_LowLOD", "OBJ//specialCube.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Metal_LowLOD")->textureID = LoadTGA("Image//LowLODMetal.tga");

	//	MeshBuilder::GetInstance()->GenerateSphere("BallMetal_HighLOD", Color(1, 0, 0), 18, 36, 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("BallMetal_HighLOD")->textureID = LoadTGA("Image//HighLODMantis.tga");
	//	MeshBuilder::GetInstance()->GenerateSphere("BallMetal_MidLOD", Color(1, 0, 0), 18, 36, 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("BallMetal_MidLOD")->textureID = LoadTGA("Image//MidLODMantis.tga");
	//	MeshBuilder::GetInstance()->GenerateSphere("BallMetal_LowLOD", Color(1, 0, 0), 18, 36, 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("BallMetal_LowLOD")->textureID = LoadTGA("Image//LowLODMantis.tga");

	//	MeshBuilder::GetInstance()->GenerateQuad("Floor_HighLOD", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("Floor_HighLOD")->textureID = LoadTGA("Image//floorHighLOD.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("Floor_MidLOD", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("Floor_MidLOD")->textureID = LoadTGA("Image//floorMidLOD.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("Floor_LowLOD", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("Floor_LowLOD")->textureID = LoadTGA("Image//floorLowLOD.tga");

	//	MeshBuilder::GetInstance()->GenerateRay("laser", Color(0, 1, 0), 10.0f);

	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_FRONT", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_BACK", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_LEFT", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_RIGHT", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_TOP", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_BOTTOM", Color(1, 1, 1), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_FRONT")->textureID = LoadTGA("Image//SkyBox//darkcity_ft.tga");
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_BACK")->textureID = LoadTGA("Image//SkyBox//darkcity_bk.tga");
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_LEFT")->textureID = LoadTGA("Image//SkyBox//darkcity_lf.tga");
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_RIGHT")->textureID = LoadTGA("Image//SkyBox//darkcity_rt.tga");
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_TOP")->textureID = LoadTGA("Image//SkyBox//darkcity_up.tga");
	//	MeshBuilder::GetInstance()->GetMesh("SKYBOX_BOTTOM")->textureID = LoadTGA("Image//SkyBox//darkcity_dn.tga");

	//	MeshBuilder::GetInstance()->GenerateQuad("EMPTYMESH", Color(1, 1, 0), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("EMPTYMESH")->textureID = LoadTGA("Image//basetile.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("GRIDMESH", Color(1, 1, 0), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("GRIDMESH")->textureID = LoadTGA("Image//tile.tga");

	//	MeshBuilder::GetInstance()->GenerateQuad("ExploIndi", Color(1, 1, 0), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("ExploIndi")->textureID = LoadTGA("Image//exploIndicator.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("ExploRad", Color(1, 1, 0), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("ExploRad")->textureID = LoadTGA("Image//exploRing.tga");
	//	MeshBuilder::GetInstance()->GenerateQuad("ProjTile", Color(1, 1, 0), 1.f);
	//	MeshBuilder::GetInstance()->GetMesh("ProjTile")->textureID = LoadTGA("Image//projIndicator.tga");

	//	MeshBuilder::GetInstance()->GenerateQuad("EXPLOMESH", Color(1, 1, 0), 1.f); //unused
	//	MeshBuilder::GetInstance()->GetMesh("EXPLOMESH")->textureID = LoadTGA("Image//tileExploded.tga");

	//	MeshBuilder::GetInstance()->GenerateOBJ("Target", "OBJ//target.obj");
	//	MeshBuilder::GetInstance()->GetMesh("Target")->textureID = LoadTGA("Image//target.tga");

	//	loadShader = true;
	//}
	// Set up the Spatial Partition and pass it to the EntityManager to manage
	//CSpatialPartition::GetInstance()->Init(100, 100, 10, 10);
	//CSpatialPartition::GetInstance()->Init(1000, 1000, 1, 1);
	//CSpatialPartition::GetInstance()->SetMeshRenderMode(CGrid::FILL);
	//CSpatialPartition::GetInstance()->SetMesh("GRIDMESH");
	//CSpatialPartition::GetInstance()->SetCamera(&camera);
	//CSpatialPartition::GetInstance()->SetLevelOfDetails(powf(150, 2), powf(400, 2)); //these are squared values of actual distance threshold

	//// Create entities into the scene
	//Create::Entity("reference", Vector3(0.0f, 0.0f, 0.0f)); // Reference
	//CSceneNode* testNode = CSceneGraph::GetInstance()->AddNode(Create::Entity("lightball", Vector3(lights[0]->position.x, lights[0]->position.y + 25, lights[0]->position.z), Vector3(), false));

	////GenericEntity* aCube = Create::Entity("cube", Vector3(-20.0f, 0.0f, -20.0f));
	//Create::Entity("ring", Vector3(0.0f, 0.0f, 0.0f)); // Reference

	////GenericEntity* fixedObj = Create::Entity("Chair", Vector3(-20.0f, 0.0f, 80.0f), Vector3(1, 1, 1), false); // Reference
	////fixedObj->SetCollider(true);
	////fixedObj->SetAABB(Vector3(15, 20, 15), Vector3(-15, -20, -15));
	////fixedObj->typeofent = EntityBase::T_DESTRUCTIBLE;
	//////NOTE : ADD INTO SCENE GRAPH
	////CSpatialPartition::GetInstance()->Add(fixedObj);
	////CSceneNode* chairNode = CSceneGraph::GetInstance()->AddNode(fixedObj);

	//groundEntity = Create::Ground("GRASS_DARKGREEN", "GEO_GRASS_LIGHTGREEN");
	////	Create::Text3DObject("text", Vector3(0.0f, 0.0f, 0.0f), "DM2210", Vector3(10.0f, 10.0f, 10.0f), Color(0, 1, 1));
	//Create::Sprite2DObject("crosshair", Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f));

	//SkyBoxEntity* theSkyBox = Create::SkyBox("SKYBOX_FRONT", "SKYBOX_BACK",
	//	"SKYBOX_LEFT", "SKYBOX_RIGHT",
	//	"SKYBOX_TOP", "SKYBOX_BOTTOM");

	//// Customise the ground entity
	//groundEntity->SetPosition(Vector3(0, -10, 0));
	//groundEntity->SetScale(Vector3(100.0f, 100.0f, 100.0f));
	//groundEntity->SetGrids(Vector3(10.0f, 1.0f, 10.0f));
	//playerInfo->SetTerrain(groundEntity);

	//// Create entities such as NPC etc
	////this->CreateEntities();
	//this->CreateMantis();
	////CreateBoss();
	////CreateStaticSpider();

	//// Setup the 2D entities
	//float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	//float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	//float fontSize = 25.0f;
	//float halfFontSize = fontSize / 2.0f;
	//for (int i = 0; i < 3; ++i)
	//{
	//	textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight + fontSize * i + halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//}
	//textObj[0]->SetText("HELLO WORLD");

	//// Activate the Blend Function
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//// Minimap
	//theMinimap = Create::Minimap(false);
	//theMinimap->SetBackground(MeshBuilder::GetInstance()->GetMesh("MINIMAP"));
	//theMinimap->SetBorder(MeshBuilder::GetInstance()->GetMesh("MINIMAPBORDER"));
	//theMinimap->SetAvatar(MeshBuilder::GetInstance()->GetMesh("MINIMAPAVATAR"));
	//theMinimap->SetStencil(MeshBuilder::GetInstance()->GetMesh("MINIMAP_STENCIL"));

	//// CameraEffects
	//theCameraEffects = Create::CameraEffects(false);
	//theCameraEffects->SetBloodScreen(MeshBuilder::GetInstance()->GetMesh("CAMERAEFFECTS_BLOODSCREEN"));
	//theCameraEffects->SetStatus_BloodScreen(false);

	//theCameraEffects->SetScopeScreen(MeshBuilder::GetInstance()->GetMesh("CAMERAEFFECTS_SCOPESCREEN"));
	////theCameraEffects->GetScopeScreen()->textureID = LoadTGA("Image//scope.tga");
	//theCameraEffects->SetStatus_ScopeScreen(false);

	////Vector3 HorizontalTrigger(45.0f, 20.0f, 10.0f);
	////theEventTrigger = Create::EventBox("cube", Vector3(10.0f, -5.0f, -10.0f), HorizontalTrigger, false);
	////theEventTrigger->SetRenderCondition(true);
	////anEnemy3D_1 = Create::Enemy3D("cube", Vector3(15.0f, -20.0f, -10.0f), theEventTrigger->AssociatedTrigger, groundEntity, Vector3(5.0f, 5.0f, 5.0f), false);
	////CSceneNode* enemyNode = CSceneGraph::GetInstance()->AddNode(anEnemy3D_1);
	////CSceneNode* eventNode = enemyNode->AddChild(theEventTrigger);
	////CSpatialPartition::GetInstance()->Add(anEnemy3D_1);

	//Vector3 Offset(50.0f, 0, 50.0f);
	//Vector3 VerticalTarget(1.0f, 10.0f, 10.0f);
	//Vector3 HorizontalTarget(10.0f, 10.0f, 1.0f);
	//Vector3 VerticalTrigger(10.0f, 20.0f, 45.0f);
	//Vector3 HorizontalTrigger(45.0f, 20.0f, 10.0f);
	//Vector3 MoveVerticalMin(0, 0, -20);
	//Vector3 MoveVerticalMax(0, 0, 20);
	//Vector3 MoveHorizontalMin(-20, 0, 0);
	//Vector3 MoveHorizontalMax(20, 0, 0);

	//theEventTrigger = Create::EventBox("Target", Vector3(25.0f, -5.0f, -200.0f), HorizontalTrigger);
	//theEventTrigger->SetAABB(HorizontalTrigger, -HorizontalTrigger);
	//theEventTrigger->SetCollider(true);
	//CTarget* tempTest;
	//tempTest = Create::Target("Target", Vector3(75.0f, -20.0f, -175.0f), theEventTrigger->AssociatedTrigger, groundEntity, MoveVerticalMin, MoveVerticalMax, false, VerticalTarget);
	//tempTest->SetAABB(VerticalTarget, -VerticalTarget);
	//tempTest->SetCollider(true);
	//CSceneNode* eventNode = CSceneGraph::GetInstance()->AddNode(theEventTrigger);
	//eventNode->recalculate = false;
	//CSceneNode* enemyNode = eventNode->AddChild(tempTest);
	//enemyNode->recalculate = false;
	//CSpatialPartition::GetInstance()->Add(theEventTrigger);
	//CSpatialPartition::GetInstance()->Add(tempTest);

	//// Hardware Abstraction
	//theKeyboard = new CKeyboard();
	//theKeyboard->Create(playerInfo);

	//theMouse = new CMouse();
	//theMouse->Create(playerInfo);
}

void SceneText2::Update(double dt)
{
	if (KeyboardController::GetInstance()->IsKeyPressed(Application::GetInstance().keyScene2Off))
	{
		SceneManager::GetInstance()->RemoveSubScene_Queue("Start2");
	}
}

void SceneText2::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
//	Mesh* crosshair = MeshBuilder::GetInstance()->GetMesh("crosshair");
//
//	modelStack.PushMatrix();
//	modelStack.Translate(0, 0, -6.0f);
//	modelStack.Scale(0.25, 0.25f, 0.25f);
//	RenderHelper::RenderMeshGun(crosshair);
//	modelStack.PopMatrix();
//
//	// PreRenderMesh
//	RenderHelper::PreRenderMesh();
//	EntityManager::GetInstance()->Render(); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
//	CSceneGraph::GetInstance()->Render(); //All active entities/moving things
//	CSpatialPartition::GetInstance()->Render(); //Spatial partitioning grid
//// PostRenderMesh
//	RenderHelper::PostRenderMesh();

	// Enable blend mode
	glEnable(GL_BLEND);

	Vector3 windowMinAABB = Vector3(minScreenAABB.x * 1000 - 500, minScreenAABB.y * 1000 - 500);
	Vector3 windowMaxAABB = Vector3(maxScreenAABB.x * 1000 - 500, maxScreenAABB.y * 1000 - 500);
	
	Vector3 windowCenter = windowMinAABB + (windowMaxAABB - windowMinAABB) * 0.5;

	GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, 0.1, 1000);
	//GraphicsManager::GetInstance()->SetOrthographicProjection(windowMinAABB.x - windowCenter.x, windowMaxAABB.x - windowCenter.x, windowMinAABB.y - windowCenter.y, windowMaxAABB.y - windowCenter.y, 0.1, 1000);
	//GraphicsManager::GetInstance()->SetOrthographicProjection(windowMinAABB.x, windowMaxAABB.x, windowMinAABB.y, windowMaxAABB.y, 0.1, 1000);
	
	//GraphicsManager::GetInstance()->SetOrthographicProjection(100, 500, -500, 0, 0.1, 1000);
	
	GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

	modelStack.PushMatrix();
	modelStack.Translate(-windowCenter.x, 0, windowCenter.y);
	modelStack.Scale(maxScreenAABB.x - minScreenAABB.x, 1, maxScreenAABB.y - minScreenAABB.y);
	//if (showQuadtreemap)
	{
		// PreRenderMesh
		RenderHelper::PreRenderMesh();
		MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
		modelStack.PushMatrix();
		modelStack.Translate(0, 0, 0);
		modelStack.Scale(1, 1, 1);
		EntityManager::GetInstance()->Render(true); //DECORATIVE/AESTHETIC THINGS e.g terrain, skybox
		CSceneGraph::GetInstance()->Render(true); //All active entities/moving things
		CSpatialPartition::GetInstance()->Render(); //Spatial partitioning grid

		for (int i = 0; i < Graph::GetInstance()->m_nodes.size(); ++i)
		{
			modelStack.PushMatrix();
			modelStack.Translate(Graph::GetInstance()->m_nodes[i]->pos.x, CSpatialPartition::GetInstance()->yOffset + 2, Graph::GetInstance()->m_nodes[i]->pos.z);
			modelStack.Rotate(-90, 1, 0, 0);
			modelStack.Scale(100, 100, 100);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("waypoint"));
			modelStack.PopMatrix();
		}

		modelStack.PushMatrix();
		modelStack.Translate(CPlayerInfo::GetInstance()->GetPos().x, CPlayerInfo::GetInstance()->GetPos().y, CPlayerInfo::GetInstance()->GetPos().z);
		modelStack.Scale(4, 1, 4);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("PlayerPoint"));

		modelStack.PopMatrix();

		modelStack.PopMatrix();
	}

	modelStack.PopMatrix();

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// PreRenderText
	RenderHelper::PreRenderText();

	//EntityManager::GetInstance()->RenderUI();

	//if (KeyboardController::GetInstance()->IsKeyDown('9'))
	//	theCameraEffects->SetStatus_BloodScreen(true);
	//// Render Camera Effects
	//theCameraEffects->RenderUI();

	//// Render Minimap
	//theMinimap->RenderUI();

	// PostRenderText
	RenderHelper::PostRenderText();

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneText2::Exit()
{
//	// Detach camera from other entities
//	GraphicsManager::GetInstance()->DetachCamera();
//	playerInfo->DetachCamera();
//
//	if (playerInfo->DropInstance() == false)
//	{
//#if _DEBUGMODE==1
//		cout << "Unable to drop PlayerInfo class" << endl;
//#endif
//	}
//
//	GraphicsManager::GetInstance()->RemoveLight("lights[0]");
//	GraphicsManager::GetInstance()->RemoveLight("lights[1]");
//	CSceneGraph::GetInstance()->Destroy();
//	// Delete the Spatial Partition
//	CSpatialPartition::GetInstance()->Destroy();
//	EntityManager::GetInstance()->Destroy();
}