#include "Enemy3D.h"
#include "../EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "../SpatialPartition/SpatialPartition.h"
#include "../SceneGraph/SceneGraph.h"
#include "../WaypointGraph.h"
#include "../PlayerInfo/PlayerInfo.h"
#include "LuaInterface.h"

CEnemy3D::CEnemy3D(Mesh* _modelMesh)
	: GenericEntity(NULL)
	, defaultPosition(Vector3(0.0f,0.0f,0.0f))
	, defaultTarget(Vector3(0.0f, 0.0f, 0.0f))
	, defaultUp(Vector3(0.0f, 0.0f, 0.0f))
	, target(Vector3(0.0f, 0.0f, 0.0f))
	, up(Vector3(0.0f, 0.0f, 0.0f))
	, maxBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, minBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, m_pTerrain(NULL)
	, m_fElapsedTimeBeforeUpdate(5.0f)
	, m_dSpeed(1.0f)
{
	this->modelMesh = _modelMesh;
}


CEnemy3D::~CEnemy3D()
{
}

void CEnemy3D::Init(void)
{
	// Set the default values
	defaultPosition.Set(0, 0, 10);
	defaultTarget.Set(0, 0, 0);
	defaultUp.Set(0, 1, 0);

	// Set the current values
	//resets stuff so i commented it out
	//position.Set(10.0f, 0.0f, 0.0f);
	//target.Set(10.0f, 0.0f, 450.0f);
	//up.Set(0.0f, 1.0f, 0.0f);

	// Set Boundary
	maxBoundary.Set(1, 1, 1);
	minBoundary.Set(-1, -1, -1);

	// Set speed
	m_dSpeed = 1.0;

	ModifyFSM();

	////Default FSM
	//FSM_State = "patrol";
	//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
	//	"patrol",
	//	std::pair<std::string, FSM_Condition>(
	//		"chase",
	//		FSMC_NEARPLAYER
	//		) ));
	//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
	//	"patrol",
	//	std::pair<std::string, FSM_Condition>(
	//		"idle",
	//		FSMC_TIMEPASSED_TWENTYSECONDS
	//		)));
	//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
	//	"idle",
	//	std::pair<std::string, FSM_Condition>(
	//		"chase",
	//		FSMC_NEARPLAYER
	//		)));
	//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
	//	"chase",
	//	std::pair<std::string, FSM_Condition>(
	//		"patrol",
	//		FSMC_AWAYPLAYER
	//		)));

	#pragma region add later pls

	#pragma region 1st part

	//// Should replace with this later
	//// Set a default destination position
	//destinationPosition.Set(5.0f, 0.0f, 500.0f);

	//// Set speed
	//m_dSpeed = 1.0;

	//// Init cWaypointManager
	//cWaypointManager.Init();
	//Vector3 aWayPoint_A = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.A");
	//Vector3 aWayPoint_B = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.B");
	//aWayPoint_B.x = aWayPoint_B.x + aWayPoint_B.x;
	//aWayPoint_B.z = aWayPoint_B.z + aWayPoint_B.z;
	//Vector3 aWayPoint_C = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.C");
	//aWayPoint_C.x = aWayPoint_C.x + aWayPoint_C.x;
	//aWayPoint_C.z = aWayPoint_C.z + aWayPoint_C.z;

	//int m_iWayPointID = cWaypointManager.AddWaypoint(aWayPoint_A);
	//m_iWayPointID = cWaypointManager.AddWaypoint(m_iWayPointID, aWayPoint_B);
	//m_iWayPointID = cWaypointManager.AddWaypoint(m_iWayPointID, aWayPoint_C);

	//cWaypointManager.PrintSelf();

	#pragma endregion

	#pragma region 2nd part

	//try
	//{
	//	Vector3 aWayPoint_A = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.D");
	//	if ((aWayPoint_A.x = -777.7f) && (aWayPoint_A.x = -888.8f) && (aWayPoint_A.x = -999.9f))
	//		throw "error102";
	//	Vector3 aWayPoint_B = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.B");
	//	Vector3 aWayPoint_C = CLuaManager::GetInstance()->get<Vector3>("WayPoints.Enemy_1.C");
	//}
	//catch (exception& e)
	//{
	//	cout << e.what() << "\n";
	//}
	//catch (const char* sErrorMessage)
	//{
	//	CLuaManager::GetInstance()->error(sErrorMessage);
	//}

	//// Apply offsets to the Enemy3D's position
	//aWayPoint_B.x = aWayPoint_B.x + aWayPoint_B.x;
	//aWayPoint_B.z = aWayPoint_B.z + aWayPoint_B.z;
	//aWayPoint_C.x = aWayPoint_C.x + aWayPoint_C.x;
	//aWayPoint_C.z = aWayPoint_C.z + aWayPoint_C.z;

	#pragma endregion

	#pragma endregion
}

// Reset this player instance to default
void CEnemy3D::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

bool CEnemy3D::SetTrigger(CEventTrigger * setTrigger)
{
	this->Dependent = setTrigger;
	return false;
}

// Set position
void CEnemy3D::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CEnemy3D::SetTarget(const Vector3& target)
{
	this->target = target;
}
// Set Up
void CEnemy3D::SetUp(const Vector3& up)
{
	this->up = up;
}
// Set the boundary for the player info
void CEnemy3D::SetBoundary(Vector3 max, Vector3 min)
{
	maxBoundary = max;
	minBoundary = min;
}
// Set the terrain for the player info
void CEnemy3D::SetTerrain(GroundEntity* m_pTerrain)
{
	if (m_pTerrain != NULL)
	{
		this->m_pTerrain = m_pTerrain;

		SetBoundary(this->m_pTerrain->GetMaxBoundary(), this->m_pTerrain->GetMinBoundary());
	}
}

// Set the speed of this Enemy's movement
void CEnemy3D::SetSpeed(const double m_dSpeed)
{
	this->m_dSpeed = m_dSpeed;
}
// Set the acceleration of this Enemy's movement
void CEnemy3D::SetAcceleration(const double m_dAcceleration)
{
	this->m_dAcceleration = m_dAcceleration;
}

// Get position
Vector3 CEnemy3D::GetPos(void) const
{
	return position;
}

// Get target
Vector3 CEnemy3D::GetTarget(void) const
{
	return target;
}
// Get Up
Vector3 CEnemy3D::GetUp(void) const
{
	return up;
}
// Get the terrain for the player info
GroundEntity* CEnemy3D::GetTerrain(void)
{
	return m_pTerrain;
}

// Get the speed of this Enemy's movement
double CEnemy3D::GetSpeed(void) const
{
	return m_dSpeed;
}
// Get the acceleration of this Enemy's movement
double CEnemy3D::GetAcceleration(void) const
{
	return m_dAcceleration;
}

// Update
void CEnemy3D::Update(double dt)
{


	Vector3 viewVector = (target - position).Normalized();
	position += viewVector * (float)m_dSpeed * (float)dt * CSpatialPartition::GetInstance()->EnemySpeedMultiplier;

	Constrain();

	m_fElapsedTimeBeforeUpdate += (float)dt;
	miscTimerforfsm += dt;
	if (m_fElapsedTimeBeforeUpdate > 1.0f)
	{
		m_fElapsedTimeBeforeUpdate = 0.0f;
		//if (position.z > 400.0f)
		//	target.z = position.z * -1;
		//else if (position.z < -400.0f)
		//	target.z = position.z * -1;

		Vector3 _nearestpost = CLuaInterface::GetInstance()->GetNearestWaypoint_Lua(position);


		for (std::vector<std::pair<std::string, std::pair<std::string, FSM_Condition>>>::iterator it = FSMTransitionManager.begin(); it != FSMTransitionManager.end(); ++it)
		{
			if ((*it).first == FSM_State)
			{
				bool switchscene = false;
				switch ((*it).second.second)
				{
				case FSMC_AWAYPLAYER:
				{
					if ((CPlayerInfo::GetInstance()->GetPos() - GetPos()).Length() >= 250)
					{
						switchscene = true;
					}
				}
					break;
				case FSMC_NEARPLAYER:
				{
					if ((CPlayerInfo::GetInstance()->GetPos() - GetPos()).Length() <= 70)
					{
						switchscene = true;
					}
				}
					break;
				case FSMC_TIMEPASSED_TWENTYSECONDS:
				{
					if (miscTimerforfsm >= 20.0)
					{
						switchscene = true;
					}
				}
					break;
				case FSMC_TIMEPASSED_ONESECOND:
				{
					if (miscTimerforfsm >= 1.0)
					{
						switchscene = true;
					}
				}
				break;
				case FSMC_TIMEPASSED_TENSECONDS:
				{
					if (miscTimerforfsm >= 10.0)
					{
						switchscene = true;
					}
				}
					break;
				default:
					break;
				}

				if (switchscene)
				{
					FSM_State = (*it).second.first;
					miscTimerforfsm = 0.0;
					while (AStarnodes.size() > 0)
					{
						AStarnodes.pop();
					}
					target = position;
					break;
				}

			}

			if (FSM_State == "chase")//chase, patrol, idle, escape, teleport, pathing
			{
				target = CPlayerInfo::GetInstance()->GetPos();
				m_dSpeed = 20.0;
			}
			else if (FSM_State == "patrol")
			{
				m_dSpeed = 80.0;

				//Node* nearestnode = Graph::GetInstance()->GetNearestNode(position);
				
				if ((position - target).Length() <= 2 && (position - _nearestpost).Length() > 2) //no target, away from a waypoint
				{
					target = _nearestpost;
				}
				else if ((position - target).Length() <= 2 && (position - _nearestpost).Length() <= 2)
				{
					Node* nearestnode = Graph::GetInstance()->GetNearestNode(_nearestpost);
					target = Graph::GetInstance()->m_nodes.at(nearestnode->nextNode)->pos;
				}


			}
			else if (FSM_State == "idle")
			{


				//Node* nearestnode = Graph::GetInstance()->GetNearestNode(position);

				target = _nearestpost;

				if ((position - _nearestpost).Length() > 3)
				{				
					m_dSpeed = 60.0;
				}
				else
				{
					m_dSpeed = 0.0;
				}
			}
			else if (FSM_State == "escape")
			{
				target = (position - CPlayerInfo::GetInstance()->GetPos());
				m_dSpeed = 60.0;
			}
			//else if (FSM_State == "teleport")
			//{

			//}
			else if (FSM_State == "pathing")
			{
				if (AStarnodes.size() == 0)
				{
					std::vector<Vector3>tempastarvector = Graph::GetInstance()->PathFinding(position, CPlayerInfo::GetInstance()->GetPos());
					for (int i = tempastarvector.size() - 1; i >= 0; --i)
					{
						AStarnodes.push(tempastarvector[i]);
					}
					tempastarvector.clear();
				}

				if ((position - AStarnodes.front()).Length() < 2)
				{
					AStarnodes.pop();
					if (AStarnodes.size() > 0)
					{
						target = AStarnodes.front();
					}
				}
				else if (AStarnodes.size() > 0)
				{
					target = AStarnodes.front();
				}

				m_dSpeed = 80.0;
			}

		}
	}
}

// Constrain the position within the borders
void CEnemy3D::Constrain(void)
{
	// Constrain player within the boundary
	if (position.x > maxBoundary.x - 1.0f)
		position.x = maxBoundary.x - 1.0f;
	if (position.z > maxBoundary.z - 1.0f)
		position.z = maxBoundary.z - 1.0f;
	if (position.x < minBoundary.x + 1.0f)
		position.x = minBoundary.x + 1.0f;
	if (position.z < minBoundary.z + 1.0f)
		position.z = minBoundary.z + 1.0f;

	// if the y position is not equal to terrain height at that position, 
	// then update y position to the terrain height

	//if (position.y != m_pTerrain->GetTerrainHeight(position))
	//	position.y = m_pTerrain->GetTerrainHeight(position);
}

// Render
void CEnemy3D::Render(bool LODMap)
{
	//
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	//modelStack.Translate(position.x, position.y, position.z);
	//modelStack.Scale(scale.x, scale.y, scale.z);
	
	if (GetLODStatus())
	{
		if (FSM_State == "chase")//chase, patrol, idle, escape, pathing
		{
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("reds"));
		}
		else if (FSM_State == "patrol")
		{
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("yellows"));
		}
		else if (FSM_State == "idle")
		{
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("greens"));
		}
		else if (FSM_State == "escape")
		{
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("blues"));
		}
		else if (FSM_State == "pathing")
		{
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("purples"));
		}
		

		//if (theDetailLevel != NO_DETAILS)
		//{
		//	if (!LODMap)
		//	{
		//		RenderHelper::RenderMesh(GetLODMesh());
		//	}
		//	else if (theDetailLevel == HIGH_DETAILS)
		//	{
		//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("HighLOD"));
		//	}
		//	else if (theDetailLevel == MID_DETAILS)
		//	{
		//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("MidLOD"));
		//	}
		//	else if (theDetailLevel == LOW_DETAILS)
		//	{
		//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("LowLOD"));
		//	}
		//}
	}
	else
	{

		{
			RenderHelper::RenderMesh(modelMesh);
		}
	}
	modelStack.PopMatrix();
}

CEnemy3D* Create::Enemy3D(const std::string& _meshName,
						const Vector3& _position,
						const Vector3& _scale,
						const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CEnemy3D* result = new CEnemy3D(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(false);
	result->typeofent = EntityBase::T_ENEMY;

	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

CEnemy3D* Create::Enemy3D(const std::string& _meshName,
						  const Vector3& _position,
						  CEventTrigger* dong,
						  GroundEntity* m_pTerrain,
						  const Vector3& _scale,
						  const bool bAddToEntityManager)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CEnemy3D* result = new CEnemy3D(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(true);
	result->SetTrigger(dong);
	result->SetTerrain(m_pTerrain);
	result->typeofent = EntityBase::T_ENEMY;

	// Remove if needed, for testing
	result->SetAABB(result->GetScale(), -result->GetScale());

	if (bAddToEntityManager)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}

bool CEnemy3D::CanItBeSetToDone()
{
	if (doneCondition == CDC_NOCHILD)
	{
		if (CSceneGraph::GetInstance()->GetNode(this))
		{
			if (CSceneGraph::GetInstance()->GetNode(this)->childMap.size() > 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	else if (doneCondition == CDC_NONE)
	{
		return true;
	}
	else return false;

}

void CEnemy3D::ModifyFSM()
{
	miscTimerforfsm = 0.0;
	m_fElapsedTimeBeforeUpdate = 1.0;
	while (AStarnodes.size() > 0)
	{
		AStarnodes.pop();
	}
	target = position;
	FSMTransitionManager.clear();

	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_FSMDATA.lua", CLuaInterface::GetInstance()->theLuaState);

	FSM_State = CLuaInterface::GetInstance()->getStringValue("defaultstate");
	int totalTransitions = CLuaInterface::GetInstance()->getIntValue("transitioncount");

	FSM_Condition throwaway;
	std::string throwawaycondition = "";

	for (int i = 0; i < totalTransitions; ++i)
	{
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, ("t" + std::to_string(i)).c_str());

		throwawaycondition = CLuaInterface::GetInstance()->getFieldString("condition");
		if (throwawaycondition == "awayplayer")
		{
			throwaway = FSMC_AWAYPLAYER;
		}
		else if (throwawaycondition == "nearplayer")
		{
			throwaway = FSMC_NEARPLAYER;
		}
		else if (throwawaycondition == "onesecond")
		{
			throwaway = FSMC_TIMEPASSED_ONESECOND;
		}
		else if (throwawaycondition == "tenseconds")
		{
			throwaway = FSMC_TIMEPASSED_TENSECONDS;
		}
		else if (throwawaycondition == "twentyseconds")
		{
			throwaway = FSMC_TIMEPASSED_TWENTYSECONDS;
		}
		else
		{
			std::cout << "invalid FSM condition" << std::endl;
			throwaway = FSMC_TIMEPASSED_ONESECOND;
		}

		FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
			CLuaInterface::GetInstance()->getFieldString("currentstate"),
			std::pair<std::string, FSM_Condition>(
				CLuaInterface::GetInstance()->getFieldString("nextstate"),
				throwaway
				)));
	}

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);

	//FSM_State = "patrol";
//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
//	"patrol",
//	std::pair<std::string, FSM_Condition>(
//		"chase",
//		FSMC_NEARPLAYER
//		) ));
//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
//	"patrol",
//	std::pair<std::string, FSM_Condition>(
//		"idle",
//		FSMC_TIMEPASSED_TWENTYSECONDS
//		)));
//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
//	"idle",
//	std::pair<std::string, FSM_Condition>(
//		"chase",
//		FSMC_NEARPLAYER
//		)));
//FSMTransitionManager.push_back(std::pair<std::string, std::pair<std::string, FSM_Condition>>(
//	"chase",
//	std::pair<std::string, FSM_Condition>(
//		"patrol",
//		FSMC_AWAYPLAYER
//		)));
}