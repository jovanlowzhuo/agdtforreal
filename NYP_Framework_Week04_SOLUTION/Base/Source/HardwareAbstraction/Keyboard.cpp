#include "Keyboard.h"
#include <iostream>
using namespace std;

#include "KeyboardController.h"
#include "../PlayerInfo/PlayerInfo.h"
#include "../Application.h"

const bool _CONTROLLER_KEYBOARD_DEBUG = false;

CKeyboard::CKeyboard()
{
}


CKeyboard::~CKeyboard()
{
}

// Create this controller
bool CKeyboard::Create(CPlayerInfo* thePlayerInfo)
{
	CController::Create(thePlayerInfo);
	if (_CONTROLLER_KEYBOARD_DEBUG)
		cout << "CKeyboard::Create()" << endl;
	return false;
}


// Read from the controller
int CKeyboard::Read(const float deltaTime)
{
	CController::Read(deltaTime);
	if (_CONTROLLER_KEYBOARD_DEBUG)
		cout << "CKeyboard::Read()" << endl;
	
	// Process the keys for customisation
	if (KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyW))
		Move_FrontBack(deltaTime, true);
	else if ((KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyW)) && (KeyboardController::GetInstance()->IsKeyDown(VK_SHIFT)))
		Move_FrontBack(deltaTime, true, 2.0f);
	else if (KeyboardController::GetInstance()->IsKeyReleased(Application::GetInstance().keyW))
		StopSway(deltaTime);
	if (KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyS))
		Move_FrontBack(deltaTime, false);
	else if (KeyboardController::GetInstance()->IsKeyReleased(Application::GetInstance().keyS))
		StopSway(deltaTime);

	if (KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyA))
		Move_LeftRight(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyD))
		Move_LeftRight(deltaTime, false);

	if (KeyboardController::GetInstance()->IsKeyDown(VK_UP))
		Look_UpDown(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_DOWN))
		Look_UpDown(deltaTime, false);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_LEFT))
		Look_LeftRight(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_RIGHT))
		Look_LeftRight(deltaTime, false);

	// If the user presses SPACEBAR, then make him jump
	if (KeyboardController::GetInstance()->IsKeyDown(VK_SPACE) &&
		thePlayerInfo->isOnGround())
		Jump(true);

	// Update the weapons
	if (KeyboardController::GetInstance()->IsKeyReleased(Application::GetInstance().keyReload))
		Reload(deltaTime);

	// If the user presses R key, then reset the view to default values
	if (KeyboardController::GetInstance()->IsKeyDown(Application::GetInstance().keyReset))
		Reset();

	return 0;
}
