#pragma once

#include "EntityBase.h"
#include "Vector3.h"
#include "Mesh.h"
#include <vector>
using namespace std;

#include "../LevelOfDetails.h"

//Include GLEW
#include <GL/glew.h>

class CGrid
{
public:
	enum SMeshRenderMode
	{
		WIRE,
		FILL,
		NUM_MODE
	};

	// Constructor
	CGrid(void);
	// Destructor
	~CGrid(void);

	// Init
	void Init(	const float xIndex, const float zIndex, 
				const int xGridSize, const int zGridSize,
				const float xOffset = 0, const float zOffset = 0, const int quadtreeCount = 0);

	void InitNode(const float xIndex, const float zIndex,
		const int xGridSize, const int zGridSize,
		const Vector3 _min = Vector3(), const Vector3 _max = Vector3(), const int quadtreeCount = 0);


	// Set a particular grid's Mesh
	void SetMesh(const std::string& _meshName);
	// Set Mesh's Render Mode
	void SetMeshRenderMode(CGrid::SMeshRenderMode meshRenderMode);
	// Get Mesh's Render Mode
	CGrid::SMeshRenderMode GetMeshRenderMode(void) const;

	// Update the grid
	void Update(vector<EntityBase*>* migrationList, const bool visitest = true);
	// Render the grid
	void Render(void);
	// RenderObjects
	void RenderObjects(const int RESOLUTION);

	// Add a new object to this grid
	void Add(EntityBase* theObject);
	// Remove but not delete all objects from this grid
	void Remove(void);
	// Remove but not delete an object from this grid
	bool Remove(EntityBase* theObject);

	// Check if an object is in this grid
	bool IsHere(EntityBase* theObject) const;

	// Get list of objects in this grid
	vector<EntityBase*> GetListOfObject(Vector3 position, const float radius = 0.0f, bool applyMark = false);

	void SetDetailLevel(const CLevelOfDetails::DETAIL_LEVEL theDetailLevel);

	// PrintSelf
	void PrintSelf(void);

	float CalculateDistanceSquare(Vector3* theCameraPosition, const int xPos, const int zPos)
	{
		float xDistance = xPos - theCameraPosition->x;
		float yDistance = zPos - theCameraPosition->z;

		return (float)(xDistance*xDistance + yDistance * yDistance);
	}

	bool IsVisible(Vector3 camPos, Vector3 camDir, const int xPos, const int zPos)
	{
		//float xDist = (sizeGrid.x * xIndex + (sizeGrid.x/2) - (size.x /2 )) - camPos.x;
		//float zDist = (sizeGrid.z * zIndex + (sizeGrid.z / 2) - (size.z /2 )) - camPos.z;

		float xDist = xPos - camPos.x;
		float zDist = zPos - camPos.z;

		if (xDist * xDist + zDist * zDist < (sizeGrid.x * sizeGrid.x + sizeGrid.z * sizeGrid.z))
		{
			return true;
		}

		Vector3 gridCenter(xDist, 0, zDist);
		if (camDir.Dot(gridCenter) < 0)
		{
			return false;
		}

		return true;
	}

	int layerOfTree = 0; //0 for surface-level grid stuff, 1-2 for deeper quadtree stuff. Stop at 2.
	CGrid* nodes; //have four of these, [0 for -x -z, 1 for +x -z, 2 for -x +z, 3 for +x + z]

	int CleanIfEmpty(vector<EntityBase*>* migrationList);

	int GetObjCount() const
	{
		int tempcount = 0;
		if (nodes != NULL)
		{
			for (int x = 0; x < 2; ++x)
			{
				for (int z = 0; z < 2; ++z)
				{
					tempcount += nodes[x * 2 + z].GetObjCount();
				}
			}
		}
		return ListOfObjects.size() + tempcount;
	}

	void DeleteAllThat()
	{
		if (nodes != NULL)
		{
			for (int x = 0; x < 2; ++x)
			{
				for (int z = 0; z < 2; ++z)
				{
					nodes[x * 2 + z].DeleteAllThat();
				}
			}
			delete[] nodes;
			nodes = NULL;
		}

	}

	bool RadialExplosion(Vector3 exploPos, float exploRange);

//protected:
	// We use a Vector3 to store the indices of this Grid within the Spatial Partition array/A grid's quadtree array.
	Vector3 index;
	// We use a Vector3 to store the size of this Grid within the Spatial Partition array.
	Vector3 size;

	//size of each grid
	Vector3 sizeGrid;

	// We use a Vector3 to store the x- and z-offset of this Grid.
	Vector3 offset;
	// AABB OF A GRID, IMPORTANT
	Vector3 min, max;

	// The mesh to represent the grid
	Mesh* theMesh;
	// Define the mesh render mode
	SMeshRenderMode meshRenderMode;

	// List of objects in this grid
	vector<EntityBase*> ListOfObjects;

	CLevelOfDetails::DETAIL_LEVEL detailLevel;
};
