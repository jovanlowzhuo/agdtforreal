#include "Grid.h"
#include "stdio.h"
#include "MeshBuilder.h"
#include "RenderHelper.h"
#include "GraphicsManager.h"
#include "../GenericEntity.h"
#include "SpatialPartition.h"
#include "ShaderProgram.h"
#include "../SceneGraph/SceneGraph.h"

static const int MAX_QUADTREE_LAYER = 6;

/********************************************************************************
Constructor
********************************************************************************/
CGrid::CGrid(void)
	: index(Vector3(-1, -1, -1))
	, size(Vector3(-1, -1, -1))
	, offset(Vector3(-1, -1, -1))
	, min(Vector3(-1, -1, -1))
	, max(Vector3(-1, -1, -1))
	, theMesh(NULL)
	, ListOfObjects(NULL)
	, meshRenderMode(FILL)
	, detailLevel(CLevelOfDetails::NO_DETAILS)
{
}

/********************************************************************************
Destructor
********************************************************************************/
CGrid::~CGrid(void)
{
	if (theMesh)
	{
		// Do not delete the Mesh here as MeshBuilder will take care of them.
		//delete theMesh;
		theMesh = NULL;
	}
	Remove();
}

/********************************************************************************
Initialise this grid
********************************************************************************/
void CGrid::Init(const float xIndex, const float zIndex,
					const int xGridSize, const int zGridSize,
					const float xOffset, const float zOffset, const int quadtreeCount)
{
	index.Set((float)xIndex, 0.0f, (float)zIndex);
	size.Set((float)xGridSize, 0.0f, (float)zGridSize);
	sizeGrid.Set((float)xGridSize/2, 0.0f, (float)zGridSize/2);
	offset.Set(xOffset, 0.0f, zOffset);
	min.Set(index.x * size.x - offset.x, 0.0f, index.z * size.z - offset.z);
	max.Set(index.x * size.x - offset.x + xGridSize, 0.0f, index.z * size.z - offset.z + zGridSize);
	layerOfTree = quadtreeCount;
	nodes = NULL;
}

void CGrid::InitNode(const float xIndex, const float zIndex,
	const int xGridSize, const int zGridSize,
	const Vector3 _min, const Vector3 _max, const int quadtreeCount)
{
	index.Set((float)xIndex, 0.0f, (float)zIndex);
	size.Set((float)xGridSize, 0.0f, (float)zGridSize);
	sizeGrid.Set((float)xGridSize / 2, 0.0f, (float)zGridSize / 2);
	//offset.Set(xOffset, 0.0f, zOffset);
	min = _min;
	max = _max;
	layerOfTree = quadtreeCount;
	nodes = NULL;
}

/********************************************************************************
 Set a particular grid's Mesh
********************************************************************************/
void CGrid::SetMesh(const std::string& _meshName)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh != nullptr)
	{
		theMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	}
}

/********************************************************************************
 Set Mesh's Render Mode
********************************************************************************/
void CGrid::SetMeshRenderMode(SMeshRenderMode meshRenderMode)
{
	this->meshRenderMode = meshRenderMode;
}

/********************************************************************************
 Get Mesh's Render Mode
********************************************************************************/
CGrid::SMeshRenderMode CGrid::GetMeshRenderMode(void) const
{
	return meshRenderMode;
}

int CGrid::CleanIfEmpty(vector<EntityBase*>* migrationList)
{
	int amountRemoved = 0;
	if (nodes != NULL)
	{
		for (int i = 0; i < 2; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				amountRemoved += nodes[i * 2 + j].CleanIfEmpty(migrationList);
			}
		}
	}

	if (nodes != NULL)
	{
		if (GetObjCount() + amountRemoved <= 1)
		{
			std::vector<EntityBase*>::iterator it;

			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 2; j++)
				{
					it = nodes[i * 2 + j].ListOfObjects.begin();
					while (it != nodes[i * 2 + j].ListOfObjects.end())
					{
						migrationList->push_back(*it);
						it = nodes[i * 2 + j].ListOfObjects.erase(it);
						++amountRemoved;
					}

					nodes[i * 2 + j].DeleteAllThat();
				}
			}
			//WARNING : MIGRATIONLIST!!!! IMPLEMENT.. also earlier on remember to call recursive

			delete[] nodes;
			nodes = NULL;
		}
	}
	return amountRemoved;
}

/********************************************************************************
Update the grid
********************************************************************************/
void CGrid::Update(vector<EntityBase*>* migrationList, const bool visitest)
{
	// Check each object to see if they are no longer in this grid

	if (nodes != NULL)
	{
		for (int i = 0; i < 2; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				nodes[i*2 + j].Update(migrationList, visitest);

				//if (!visitest || IsVisible(CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), CSpatialPartition::GetInstance()->theCamera->GetCameraTarget() - CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), i, j))
				//{
				//	//std::cout << "[" << i << "," << j << "],";
				//	float dist = CSpatialPartition::GetInstance()->CalculateDistanceSquare(&(CSpatialPartition::GetInstance()->theCamera->GetCameraPos()), i, j);
				//	if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[0])
				//	{
				//		nodes[i*2 + j].SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
				//	}
				//	else if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[1])
				//	{
				//		nodes[i*2 + j].SetDetailLevel(CLevelOfDetails::MID_DETAILS);
				//	}
				//	else
				//	{
				//		nodes[i*2 + j].SetDetailLevel(CLevelOfDetails::LOW_DETAILS);
				//	}
				//}
				//else
				//{
				//	nodes[i*2 + j].SetDetailLevel(CLevelOfDetails::NO_DETAILS);
				//}
				//std::cout << std::endl;
			}
		}
		//std::cout << GetObjCount() << std::endl;

	}
	//else if (/*visitest && */ListOfObjects.size() > 0)
	//{
	//	for (int i = 0; i < ListOfObjects.size(); ++i)
	//	{
	//		GenericEntity* go = (GenericEntity*)ListOfObjects[i];
	//		if (IsVisible(CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), CSpatialPartition::GetInstance()->theCamera->GetCameraTarget() - CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), ListOfObjects[i]->GetPosition().x, ListOfObjects[i]->GetPosition().z))
	//		{
	//			float dist = CalculateDistanceSquare(&(CSpatialPartition::GetInstance()->theCamera->GetCameraPos()), ListOfObjects[i]->GetPosition().x, ListOfObjects[i]->GetPosition().z);
	//			if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[0])
	//			{
	//				go->SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
	//			}
	//			else if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[1])
	//			{
	//				go->SetDetailLevel(CLevelOfDetails::MID_DETAILS);
	//			}
	//			else
	//			{
	//				go->SetDetailLevel(CLevelOfDetails::LOW_DETAILS);
	//			}
	//		}
	//		else
	//		{
	//			go->SetDetailLevel(CLevelOfDetails::NO_DETAILS);
	//		}
	//	}
	//}

	if (!CSpatialPartition::GetInstance()->LevelOfDetailEnable)
	{
		SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
	}
	else if (!visitest || IsVisible(CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), CSpatialPartition::GetInstance()->theCamera->GetCameraTarget() - CSpatialPartition::GetInstance()->theCamera->GetCameraPos(), min.x + (max.x - min.x) / 2, min.z + (max.z - min.z) / 2))
	{
		//std::cout << "[" << i << "," << j << "],";
		float dist = CalculateDistanceSquare(&(CSpatialPartition::GetInstance()->theCamera->GetCameraPos()), min.x + (max.x - min.x) / 2, min.z + (max.z - min.z) / 2);
		if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[0])
		{
			SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
		}
		else if (dist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[1])
		{
			SetDetailLevel(CLevelOfDetails::MID_DETAILS);
		}
		else
		{
			SetDetailLevel(CLevelOfDetails::LOW_DETAILS);
		}

		if (ListOfObjects.size() > 0)
		{
			Vector3 tempcampos = CSpatialPartition::GetInstance()->theCamera->GetCameraPos();
			if (min.x <= tempcampos.x && tempcampos.x < max.x && min.z <= tempcampos.z && tempcampos.z < max.z)
			{
				GenericEntity* aGenericEntity = NULL;
				for (int a = 0; a < ListOfObjects.size(); ++a)
				{
					aGenericEntity = (GenericEntity*)ListOfObjects[a];
					if (aGenericEntity->GetLODStatus())
					{
						float mydist = powf(aGenericEntity->GetPosition().x - tempcampos.x, 2) + powf(aGenericEntity->GetPosition().z - tempcampos.z, 2);
						if (mydist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[0])
						{
							aGenericEntity->SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
						}
						else if (mydist < CSpatialPartition::GetInstance()->LevelOfDetails_Distances[1])
						{
							aGenericEntity->SetDetailLevel(CLevelOfDetails::MID_DETAILS);
						}
						else
						{
							aGenericEntity->SetDetailLevel(CLevelOfDetails::LOW_DETAILS);
						}
					}
				}

			}
		}
	}
	else
	{
		SetDetailLevel(CLevelOfDetails::NO_DETAILS);
	}

	std::vector<EntityBase*>::iterator it;
	it = ListOfObjects.begin();
	while (it != ListOfObjects.end())
	{
		//float nodeX, nodeY, nodeZ;
		//CSceneGraph::GetInstance()->GetNode(*it)->GetCompleteTranslate(nodeX, nodeY, nodeZ);
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(*it)->TheUltimateTransformation();
		Vector3 position = /*(*it)->GetPosition() + */Vector3(trn.a[12], trn.a[13], trn.a[14]);

		//std::cout << position << std::endl;

		if (((min.x <= position.x) && (position.x < max.x)) &&
			((min.z <= position.z) && (position.z < max.z)))
		{
			// Move on otherwise
			++it;
		}
		else
		{
			migrationList->push_back(*it);

			// Remove from this Grid
			it = ListOfObjects.erase(it);
		}
	}

	if (ListOfObjects.size() > 1 && layerOfTree < MAX_QUADTREE_LAYER * CSpatialPartition::GetInstance()->quadTreeEnable && nodes == NULL)
	{
		nodes = new CGrid[2 * 2];
		theMesh = MeshBuilder::GetInstance()->GetMesh("GRIDMESH");
		// Initialise the array of grids
		for (int x = 0; x < 2; x++)
		{
			for (int z = 0; z < 2; z++)
			{
				if (x == 0 && z == 0) nodes[x * 2 + z].InitNode(x, z, sizeGrid.x, sizeGrid.z, min, max - Vector3(sizeGrid.x, 0, sizeGrid.z), layerOfTree + 1);
				else if (x == 1 && z == 0) nodes[x * 2 + z].InitNode(x, z, sizeGrid.x, sizeGrid.z, min + Vector3(sizeGrid.x, 0, 0), max - Vector3(0, 0, sizeGrid.z), layerOfTree + 1);
				else if (x == 0 && z == 1) nodes[x * 2 + z].InitNode(x, z, sizeGrid.x, sizeGrid.z, min + Vector3(0, 0, sizeGrid.z), max - Vector3(sizeGrid.x, 0, 0), layerOfTree + 1);
				else if (x == 1 && z == 1) nodes[x * 2 + z].InitNode(x, z, sizeGrid.x, sizeGrid.z, min + Vector3(sizeGrid.x, 0, sizeGrid.z), max, layerOfTree + 1);
				nodes[x * 2 + z].SetMesh("GRIDMESH");
			}
		}

		it = ListOfObjects.begin();
		while (it != ListOfObjects.end())
		{
			migrationList->push_back(*it);
			it = ListOfObjects.erase(it);
		}

	}
	else if (layerOfTree >= MAX_QUADTREE_LAYER * CSpatialPartition::GetInstance()->quadTreeEnable)
	{
		//delete all that
		it = ListOfObjects.begin();
		while (it != ListOfObjects.end())
		{
			migrationList->push_back(*it);
			it = ListOfObjects.erase(it);
		}
		if (nodes != NULL)
		{
			DeleteAllThat();
		}
	}

}

/********************************************************************************
Render
********************************************************************************/
void CGrid::Render(void)
{
	if (theMesh)
	{
		if (nodes != NULL)
		{
			
			
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 2; j++)
				{
					//modelStack.PushMatrix();
					////modelStack.Translate((float)(sizeGrid.x*i - (size.x / 2) + (sizeGrid.x / 2)), 50 * layerOfTree, (float)(sizeGrid.z*j - (size.z / 2) + (sizeGrid.z / 2)));
					//modelStack.Translate( (i - 0.5) * sizeGrid.x , 50 * layerOfTree, (j - 0.5) * sizeGrid.z);
					//modelStack.PushMatrix();
					////modelStack.Scale((float)sizeGrid.x, 1.0f, (float)sizeGrid.z);
					////modelStack.Scale(0.5, 1.0f, 0.5);
					//nodes[i*2 + j].Render();
					//modelStack.PopMatrix();
					//modelStack.PopMatrix();

					nodes[i * 2 + j].Render();
				}
			}

		}
		else if (ListOfObjects.size() > 0 && CSpatialPartition::GetInstance()->orangeTileEnable)
		{
			MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
			modelStack.PushMatrix();
			modelStack.Translate(min.x + (max.x - min.x) / 2, CSpatialPartition::GetInstance()->yOffset, min.z + (max.z - min.z)/2);
			modelStack.Rotate(-90, 1, 0, 0);
			modelStack.Scale(size.x, size.z, 1);

			// Set to wire render mode if wire render mode is required
			if (meshRenderMode == WIRE)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			RenderHelper::RenderMesh(theMesh);
			//if (detailLevel != CLevelOfDetails::NO_DETAILS)
			//{
			//	if (detailLevel == CLevelOfDetails::HIGH_DETAILS)
			//	{
			//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_HighLOD"));
			//	}
			//	else if (detailLevel == CLevelOfDetails::MID_DETAILS)
			//	{
			//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_MidLOD"));
			//	}
			//	else if (detailLevel == CLevelOfDetails::LOW_DETAILS)
			//	{
			//		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_LowLOD"));
			//	}
			//}
			// Set back to fill render mode if wire render mode is required
			if (meshRenderMode == WIRE)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			modelStack.PopMatrix();
		}
		else
		{
			MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
			modelStack.PushMatrix();
			modelStack.Translate(min.x + (max.x - min.x) / 2, CSpatialPartition::GetInstance()->yOffset, min.z + (max.z - min.z) / 2);
			modelStack.Rotate(-90, 1, 0, 0);
			modelStack.Scale(size.x, size.z, 1);

			// Set to wire render mode if wire render mode is required
			if (meshRenderMode == WIRE)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("EMPTYMESH"));
			ShaderProgram* currProg = GraphicsManager::GetInstance()->GetActiveShader();
			currProg->UpdateFloat("X_MIN", (min.x + CSpatialPartition::GetInstance()->GetxSize() * 0.5) / CSpatialPartition::GetInstance()->GetxSize() );
			//currProg->UpdateFloat("X_MAX", (max.x + CSpatialPartition::GetInstance()->GetxSize() * 0.5) / CSpatialPartition::GetInstance()->GetxSize());
			currProg->UpdateFloat("Y_MIN", /*0.75-*/(min.z + CSpatialPartition::GetInstance()->GetzSize() * 0.5) / CSpatialPartition::GetInstance()->GetzSize());
			//currProg->UpdateFloat("Y_MAX", (max.z + CSpatialPartition::GetInstance()->GetzSize() * 0.5) / CSpatialPartition::GetInstance()->GetzSize());
			currProg->UpdateFloat("stretch", size.z / CSpatialPartition::GetInstance()->GetzSize());

			if (detailLevel != CLevelOfDetails::NO_DETAILS)
			{
				if (detailLevel == CLevelOfDetails::HIGH_DETAILS)
				{
					RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_HighLOD"));
				}
				else if (detailLevel == CLevelOfDetails::MID_DETAILS)
				{
					RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_MidLOD"));
				}
				else if (detailLevel == CLevelOfDetails::LOW_DETAILS)
				{
					RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Floor_LowLOD"));
				}
			}
			currProg->UpdateFloat("X_MIN", 0);
			currProg->UpdateFloat("X_MAX", 1);
			currProg->UpdateFloat("Y_MIN", 0);
			currProg->UpdateFloat("Y_MAX", 1);
			currProg->UpdateFloat("stretch", 1);
			RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("EMPTYMESH"));
			// Set back to fill render mode if wire render mode is required
			if (meshRenderMode == WIRE)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			modelStack.PopMatrix();
		}
	}
}

/********************************************************************************
RenderObjects
********************************************************************************/
void CGrid::RenderObjects(const int RESOLUTION)
{
	/*
	glPushAttrib(GL_ENABLE_BIT);
	// Draw the Grid and its list of objects
	for (int i=0; i<(int)ListOfObjects.size(); i++)
	{
	ListOfObjects[i]->Render(RESOLUTION);
	}
	glPopAttrib();
	*/
}

/********************************************************************************
Add a new object to this grid
********************************************************************************/
void CGrid::Add(EntityBase* theObject)
{
	if (nodes != NULL)
	{
		//int xIndex = (((int)theObject->GetPosition().x - (-size.x / 2)) / (size.x / 2));
		//int zIndex = (((int)theObject->GetPosition().z - (-size.z / 2)) / (size.z / 2));

		//// Add them to each grid
		//if (((xIndex >= 0) && (xIndex < 2)) && ((zIndex >= 0) && (zIndex < 2)))
		//{
		//	nodes[xIndex * 2 + zIndex].Add(theObject);
		//}

		//float nodeX, nodeY, nodeZ;
		//CSceneGraph::GetInstance()->GetNode(*it)->GetCompleteTranslate(nodeX, nodeY, nodeZ);
		Vector3 position;
		if (CSceneGraph::GetInstance()->GetNode(theObject) != NULL)
		{
			Mtx44 trn = CSceneGraph::GetInstance()->GetNode(theObject)->TheUltimateTransformation();
			position = /*(*it)->GetPosition() + */Vector3(trn.a[12], trn.a[13], trn.a[14]);
		}
		else
		{
			position = theObject->GetPosition();
		}

		bool inserted = false;
		for (int x = 0; x < 2; x++)
		{
			for (int z = 0; z < 2; z++)
			{
				Vector3 tMin;
				Vector3 tMax;

				if (x == 0 && z == 0)
				{
					tMin = min;
					tMax = max - Vector3(sizeGrid.x, 0, sizeGrid.z);
				}
				else if (x == 1 && z == 0)
				{
					tMin = min + Vector3(sizeGrid.x, 0, 0);
					tMax = max - Vector3(0, 0, sizeGrid.z);
				}
				else if (x == 0 && z == 1)
				{
					tMin = min + Vector3(0, 0, sizeGrid.z);
					tMax = max - Vector3(sizeGrid.x, 0, 0);
				}
				else if (x == 1 && z == 1)
				{
					tMin = min + Vector3(sizeGrid.x, 0, sizeGrid.z);
					tMax = max;
				}

				if (((tMin.x <= position.x) && (position.x < tMax.x)) &&
					((tMin.z <= position.z) && (position.z < tMax.z)))
				{
					nodes[x * 2 + z].Add(theObject);
					inserted = true;
				}
				if (inserted) break;
			}
		}

	}
	else
	{
		for (int i = 0; i < (int)ListOfObjects.size(); ++i)
		{
			if (ListOfObjects[i] == theObject)
				return;
		}
		ListOfObjects.push_back(theObject);
	}
}

/********************************************************************************
 Remove but not delete object from this grid
********************************************************************************/
void CGrid::Remove(void)
{
	for (int i = 0; i < (int)ListOfObjects.size(); i++)
	{
		// We can delete the entities here.
		// If you delete them here, then do not delete in Scene Graph or SceneText
		delete ListOfObjects[i];
		ListOfObjects[i] = NULL;
	}
	ListOfObjects.clear();
}

/********************************************************************************
 Remove but not delete an object from this grid
********************************************************************************/
bool CGrid::Remove(EntityBase* theObject)
{
	if (nodes != NULL)
	{
		return nodes[0].Remove(theObject) + nodes[1].Remove(theObject) + nodes[2].Remove(theObject) + nodes[3].Remove(theObject);
	}
	else
	{
		//for (int i = 0; i < (int)ListOfObjects.size(); ++i)
		//{
		//	if (ListOfObjects[i] == theObject)
		//		return true;
		//}
		//return false;

		// Clean up entities that are done
		std::vector<EntityBase*>::iterator it, end;
		it = ListOfObjects.begin();
		end = ListOfObjects.end();
		while (it != end)
		{
			if ((*it) == theObject)
			{
				it = ListOfObjects.erase(it);
				return true;
			}
			else
			{
				// Move on otherwise
				++it;
			}
		}
		return false;
	}
}

/********************************************************************************
 Check if an object is in this grid
********************************************************************************/
bool CGrid::IsHere(EntityBase* theObject) const
{
	if (nodes != NULL)
	{
		return nodes[0].IsHere(theObject) + nodes[1].IsHere(theObject) + nodes[2].IsHere(theObject) + nodes[3].IsHere(theObject);
	}
	else
	{
		for (int i = 0; i < (int)ListOfObjects.size(); ++i)
		{
			if (ListOfObjects[i] == theObject)
				return true;
		}
		return false;
	}
}

/********************************************************************************
Get list of objects in this grid
********************************************************************************/
vector<EntityBase*> CGrid::GetListOfObject(Vector3 position, const float radius, bool applyMark)
{
	vector<EntityBase*> ObjectsInGrid;

	if (nodes != NULL)
	{
		for (int x = 0; x < 2; x++)
		{
			for (int z = 0; z < 2; z++)
			{
				float tempoffset = radius * 3;
				if (((nodes[x * 2 + z].min.x - tempoffset <= position.x) && (position.x < nodes[x * 2 + z].max.x + tempoffset)) &&
					((nodes[x * 2 + z].min.z - tempoffset <= position.z) && (position.z < nodes[x * 2 + z].max.z + tempoffset)))
				{
					vector<EntityBase*> childList = nodes[x * 2 + z].GetListOfObject(position, radius, applyMark);
					std::vector<EntityBase*>::iterator it;
					it = childList.begin();
					while (it != childList.end())
					{
						ObjectsInGrid.push_back(*it);
						it = childList.erase(it);
					}
				}
			}
		}
		return ObjectsInGrid;
	}
	else
	{
		if (applyMark)
		{
			GenericEntity* newgo = Create::Entity("ProjTile", Vector3((max.x + min.x) / 2, -9, (max.z + min.z) / 2), Vector3(size.x, size.z, 1));
			//newgo->rotationAngle = 90;
			//newgo->rotationAxis.Set(1, 0, 0);
			CSpatialPartition::GetInstance()->GridDisplayer.push_back(std::pair<GenericEntity*, double>(newgo, 0.1));
		}

		// if the radius of inclusion is not specified, or illegal value specified
		// then return all entities in this grid
		if (radius <= 0.0f)
		{
			return ListOfObjects;
		}

		vector<EntityBase*> theListOfObjects;
		for (int i = 0; i < (int)ListOfObjects.size(); ++i)
		{
			// Calculate the distance between the object and the supplied position
			// And check if it is within the radius

					//float nodeX, nodeY, nodeZ;
		//CSceneGraph::GetInstance()->GetNode(*it)->GetCompleteTranslate(nodeX, nodeY, nodeZ);
			Mtx44 trn = CSceneGraph::GetInstance()->GetNode(ListOfObjects[i])->TheUltimateTransformation();

			if ((/*ListOfObjects[i]->GetPosition() +*/ Vector3(trn.a[12], trn.a[13], trn.a[14]) - position).LengthSquared() < radius * radius)
			{
				theListOfObjects.push_back(ListOfObjects[i]);
			}
		}
		return theListOfObjects;
	}
}

/********************************************************************************
 PrintSelf
 ********************************************************************************/
void CGrid::PrintSelf(void)
{
	if (ListOfObjects.size() > 0)
	{
		cout << "CGrid::PrintSelf()" << endl;
		cout << "\tIndex\t:\t" << index << "\t\tOffset\t:\t" << offset << endl;
		cout << "\tMin\t:\t" << min << "\tMax\t:\t" << max << endl;
		cout << "\tList of objects in this grid:" << endl;
		cout << "\t------------------------------------------------------------------------" << endl;

		for (int i = 0; i < (int)ListOfObjects.size(); ++i)
		{
			cout << "\t" << i << "\t:\t" << ListOfObjects[i]->GetPosition() << endl;
			GenericEntity* go = (GenericEntity*)ListOfObjects[i];
			cout << go->GetLODStatus() << endl;
			if (go->GetLODStatus())
			{
				cout << "\t\t\t\t*** LOD Level: " << go->GetDetailLevel() << std::endl;
			}
		}
		cout << "\t------------------------------------------------------------------------" << endl;
	}
	else
	{
		cout << "\tThis grid has no entities." << endl;
	}
	cout << "********************************************************************************" << endl;
}


void CGrid::SetDetailLevel(const CLevelOfDetails::DETAIL_LEVEL theDetailLevel)
{
	detailLevel = theDetailLevel;

	GenericEntity* aGenericEntity = NULL;
	for (int i = 0; i < (int)ListOfObjects.size(); ++i)
	{
		aGenericEntity = (GenericEntity*)ListOfObjects[i];
		if (aGenericEntity->GetLODStatus())
		{
			aGenericEntity->SetDetailLevel(theDetailLevel);
		}
	}

}

bool CGrid::RadialExplosion(Vector3 exploPos, float exploRange)
{
	if (nodes != NULL)
	{
		bool cando[2 * 2] = {false, false, false, false};
		for (int x = 0; x < 2; ++x)
		{
			for (int z = 0; z < 2; ++z)
			{
				Vector3 centreTemp = Vector3(nodes[x * 2 + z].min.x + (nodes[x * 2 + z].max.x - nodes[x * 2 + z].min.x) / 2, 0, nodes[x * 2 + z].min.z + (nodes[x * 2 + z].max.z - nodes[x * 2 + z].min.z) / 2);
				float xTempLength = Math::FAbs((exploPos - centreTemp).Dot(Vector3(1, 0, 0)));
				float zTempLength = Math::FAbs((exploPos - centreTemp).Dot(Vector3(0, 0, 1)));
				if (xTempLength <= (max.x - min.x) * 0.5 + exploRange && zTempLength <= (max.z - min.z) * 0.5 + exploRange)
				{
					cando[x * 2 + z] = true;
				}
			}
		}
		
		int returnvalue = 0;
		for (int x = 0; x < 2; ++x)
		{
			for (int z = 0; z < 2; ++z)
			{
				if (cando[x * 2 + z])
				{
					returnvalue += nodes[x * 2 + z].RadialExplosion(exploPos, exploRange);
				}
			}
		}
		return (bool)returnvalue;

	}
	else
	{
	bool anyExploded = false;
	//theMesh = MeshBuilder::GetInstance()->GetMesh("EXPLOMESH");

	if (CSpatialPartition::GetInstance()->orangeTileEnable)
	{
		GenericEntity* newgo = Create::Entity("ExploIndi", Vector3((max.x + min.x) / 2, -8.5, (max.z + min.z) / 2), Vector3(size.x, size.z, 1));
		//newgo->rotationAngle = 90;
		//newgo->rotationAxis.Set(1, 0, 0);
		CSpatialPartition::GetInstance()->GridDisplayer.push_back(std::pair<GenericEntity*, double>(newgo, 5));
	}

	GenericEntity* go = NULL;
	for (int i = 0; i < (int)ListOfObjects.size(); ++i)
	{
		go = (GenericEntity*)ListOfObjects[i];
		Mtx44 trn = CSceneGraph::GetInstance()->GetNode(go)->TheUltimateTransformation();
		Vector3 temppos = /*(*it)->GetPosition() + */Vector3(trn.a[12], trn.a[13], trn.a[14]);

		if (sqrt(pow(temppos.x - exploPos.x, 2) + pow(temppos.z - exploPos.z, 2)) <= exploRange)
		{
			if (go->CanItBeSetToDone())
			{
				go->SetIsDone(true);
				CSpatialPartition::GetInstance()->Remove(go);
				CSceneGraph::GetInstance()->DeleteNode(go);
				anyExploded = true;
			}
		}
	}

	return anyExploded;
	}
}