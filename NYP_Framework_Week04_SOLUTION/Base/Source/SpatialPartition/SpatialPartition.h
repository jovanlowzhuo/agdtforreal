#pragma once

#include "Vector3.h"
#include "Grid.h"
#include "EntityBase.h"
#include "../GenericEntity.h"
#include "SingletonTemplate.h"
#include "../FPSCamera.h"

class CSpatialPartition : public Singleton<CSpatialPartition>
{
	friend Singleton<CSpatialPartition>;
public:
	// Destructor
	virtual ~CSpatialPartition();

	// Destroy the Singleton instance
	void Destroy();

	// Initialise the spatial partition
	bool Init(	const int xGridSize, const int zGridSize, 
				const int xNumOfGrid, const int zNumOfGrid, 
				const float yOffset = -9.9f);

	// Set Mesh's Render Mode
	void SetMeshRenderMode(CGrid::SMeshRenderMode meshRenderMode);
	// Get Mesh's Render Mode
	CGrid::SMeshRenderMode GetMeshRenderMode(void) const;

	// Set a particular grid's Mesh
	void SetMesh(const std::string& _meshName);

	// ApplyMesh
	void ApplyMesh(void);

	// Update the spatial partition
	void Update(void);
	// Render the spatial partition
	void Render(Vector3* theCameraPosition = NULL, Vector3* theCameraDirection = NULL);

	// Get xSize of the entire spatial partition
	int GetxSize(void) const;
	// Get zSize of the entire spatial partition
	int GetzSize(void) const;
	// Get xSize
	int GetxGridSize(void) const;
	// Get zNumOfGrid
	int GetzGridSize(void) const;
	// Get xNumOfGrid
	int GetxNumOfGrid(void) const;
	// Get zNumOfGrid
	int GetzNumOfGrid(void) const;

	// Get a particular grid
	CGrid GetGrid(const int xIndex, const int zIndex) const;

	// Get vector of objects from this Spatial Partition
	vector<EntityBase*> GetObjects(Vector3 position, const float radius, bool applyMark = false);

	// Add a new object
	void Add(EntityBase* theObject);
	// Remove but not delete object from this grid
	void Remove(EntityBase* theObject);

	// Calculate the squared distance from camera to a grid's centrepoint
	float CalculateDistanceSquare(Vector3* theCameraPosition, const int xIndex, const int zIndex);

	//PrintSelf
	void PrintSelf() const;

	void SetCamera(FPSCamera* _cameraPtr)
	{
		theCamera = _cameraPtr;
	}
	void RemoveCamera()
	{
		theCamera = nullptr;
	}
	void SetLevelOfDetails(const float distance_HighTOMid, const float distance_MidTOLow)
	{
		LevelOfDetails_Distances[0] = distance_HighTOMid;
		LevelOfDetails_Distances[1] = distance_MidTOLow;
	}

	//check if cGrid is visible to camera
	bool IsVisible(Vector3 camPos, Vector3 camDir, const int xIndex, const int zIndex)
	{
		float xDist = (xGridSize * xIndex + (xGridSize >> 1) - (xSize >> 1)) - camPos.x;
		float zDist = (zGridSize * zIndex + (zGridSize >> 1) - (zSize >> 1)) - camPos.z;
	
		if (xDist * xDist + zDist * zDist < (xGridSize * xGridSize + zGridSize * zGridSize))
		{
			return true;
		}

		Vector3 gridCenter(xDist, 0, zDist);
		if (camDir.Dot(gridCenter) < 0)
		{
			return false;
		}

		return true;
	}

	// The vector of objects due for migration to another grid
	vector<EntityBase*> MigrationList;

	FPSCamera* theCamera;
	float LevelOfDetails_Distances[2]; //transition points for LOD obj model change

	float yOffset; //only for rendering, not important

	bool RadialExplosion(Vector3 exploPos, float exploRange)
	{
		if (CSpatialPartition::GetInstance()->orangeTileEnable)
		{
			GenericEntity* newgo = Create::Entity("ExploRad", exploPos - Vector3(0, 6, 0), Vector3(exploRange * 2, exploRange * 2, 1));
			//newgo->rotationAngle = 90;
			//newgo->rotationAxis.Set(1, 0, 0);
			CSpatialPartition::GetInstance()->GridDisplayer.push_back(std::pair<GenericEntity*, double>(newgo, 5));
		}
		return theGrid[0].RadialExplosion(exploPos, exploRange);
	}

	bool quadTreeEnable = true;
	
	bool orangeTileEnable = true;
	bool LevelOfDetailEnable = false;

	int EnemySpeedMultiplier = 1;

	vector<std::pair<GenericEntity*, double>> GridDisplayer;

protected:
	// Constructor
	CSpatialPartition(void);

	// Variables
	CGrid* theGrid; //array to store the grids, Important! grids should have their own array for quadtree woo
	int xSize;//total xlength
	int zSize;//total zlength
	int xGridSize;//x-length of each grid
	int zGridSize;
	int xNumOfGrid;//x-amount of grids
	int zNumOfGrid;
	
	std::string _meshName; // Name of the mesh to render, eventually needed to showcase Quadtree

	// Define the mesh render mode
	CGrid::SMeshRenderMode meshRenderMode;//only for wireframe mode, not too important
};
