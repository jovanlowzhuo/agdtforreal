#include "SceneShop.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"
#include "RenderHelper.h"
#include "FPSCounter.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"
#include "SceneGraph\SceneGraph.h"
#include "SceneGraph\UpdateTransformation.h"
#include "SpatialPartition\SpatialPartition.h"

#include <iostream>
using namespace std;

#include "ShopThings.h"

SceneShop* SceneShop::sInstance = new SceneShop(SceneManager::GetInstance());

SceneShop::SceneShop()
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
}

SceneShop::SceneShop(SceneManager* _sceneMgr)
	: theMinimap(NULL)
	, theCameraEffects(NULL)
	, theMouse(NULL)
	, theKeyboard(NULL)
{
	_sceneMgr->AddScene("Shop", this);
}

SceneShop::~SceneShop()
{
	if (Box == NULL)
	{
		delete Box;
		Box = NULL;
	}
	if (Buttons[0] != NULL)
	{
		delete Buttons[0];
		Buttons[0] = NULL;
	}
	if (Buttons[1] != NULL)
	{
		delete Buttons[1];
		Buttons[1] = NULL;
	}
	if (Buttons[2] != NULL)
	{
		delete Buttons[2];
		Buttons[2] = NULL;
	}
	if (ExitButton != NULL)
	{
		delete ExitButton;
		ExitButton = NULL;
	}
	//if (theMouse)
	//{
	//	delete theMouse;
	//	theMouse = NULL;
	//}
	//if (theKeyboard)
	//{
	//	delete theKeyboard;
	//	theKeyboard = NULL;
	//}
	//// Delete the scene graph
	//CSceneGraph::GetInstance()->Destroy();
	//// Delete the Spatial Partition
	//CSpatialPartition::GetInstance()->Destroy();
	//EntityManager::GetInstance()->Destroy();
}

void SceneShop::Init()
{

	playerInfo = CPlayerInfo::GetInstance();
	orthoCam.Init(Vector3(0, 120, 0), Vector3(0, -10, 0), Vector3(0, 0, 100));
	//Create::Sprite2DObject("button_pressed", Vector3(0.0f, 0.0f, 0.0f), Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()), true);

	Box = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_pressed"));
	Box->SetPosition(Vector3(0, 500, 0));
	//Box->SetScale(Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight()));
	Box->SetScale(Vector3(1000, 1000));

	Buttons[0] = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_notpressed"));
	Buttons[0]->SetPosition(Vector3(0.25 * Application::GetInstance().GetWindowWidth(), 510, 0.35 * Application::GetInstance().GetWindowHeight()));
	Buttons[0]->SetScale(Vector3(0.75 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()));
	Buttons[1] = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_notpressed"));
	Buttons[1]->SetPosition(Vector3(0.25 * Application::GetInstance().GetWindowWidth(), 510, 0 * Application::GetInstance().GetWindowHeight()));
	Buttons[1]->SetScale(Vector3(0.75 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()));
	Buttons[2] = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("button_notpressed"));
	Buttons[2]->SetPosition(Vector3(0.25 * Application::GetInstance().GetWindowWidth(), 510, -0.35 * Application::GetInstance().GetWindowHeight()));
	Buttons[2]->SetScale(Vector3(0.75 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()));

	ExitButton = new SpriteEntity(MeshBuilder::GetInstance()->GetMesh("closebutton"));
	ExitButton->SetPosition(Vector3(-0.45 * Application::GetInstance().GetWindowWidth(), 510, -0.45 * Application::GetInstance().GetWindowHeight()));
	ExitButton->SetScale(Vector3(0.2 * Application::GetInstance().GetWindowWidth(), 0.1 * Application::GetInstance().GetWindowHeight()));

	ShopThings::GetInstance()->Shop_ReadLua();

	//// Setup the 2D entities
	//float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	//float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	//float fontSize = 100.0f;
	//float halfFontSize = fontSize / 2.0f;
	//textObj[0] = Create::Text2DObject("text", Vector3(0, 0, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[1] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.5, -halfWindowHeight * 0.8, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[2] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.5, -halfWindowHeight * 0.8, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	//textObj[0]->SetText("Exit the game?");
	//textObj[1]->SetText("Yes!");
	//textObj[2]->SetText("No!!!");

}

void SceneShop::Update(double dt)
{
	double MousePositionX = 0, MousePositionY = 0;
	MousePositionX = Application::GetInstance().GetMousePos().first;
	MousePositionY = Application::GetInstance().GetMousePos().second;

	MousePositionX = MousePositionX - 0.5 * (double)SceneManager::GetInstance()->windowWidth;
	MousePositionY = MousePositionY - 0.5 * (double)SceneManager::GetInstance()->windowHeight; //? idk if order correct
	MousePositionY *= -1;

	float subwidth = maxScreenAABB.x - minScreenAABB.x;
	float subheight = maxScreenAABB.y - minScreenAABB.y;
	float centerX = minScreenAABB.x + 0.5 * subwidth;
	float centerY = minScreenAABB.y + 0.5 * subheight;

	for (int i = 0; i < 3 && CurrentScreen; ++i)
	{

		Vector3 minAABB_button = -Buttons[i]->GetScale() * 0.5 + Vector3(Buttons[i]->GetPosition().x, Buttons[i]->GetPosition().z);
		minAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		minAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();
		Vector3 maxAABB_button = Buttons[i]->GetScale() * 0.5 + Vector3(Buttons[i]->GetPosition().x, Buttons[i]->GetPosition().z);
		maxAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		maxAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();

		minAABB_button.x *= subwidth;
		minAABB_button.y *= subheight;
		maxAABB_button.x *= subwidth;
		maxAABB_button.y *= subheight;

		minAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		minAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;
		maxAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		maxAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;

		if (MousePositionX >= minAABB_button.x && MousePositionY >= minAABB_button.y && MousePositionX <= maxAABB_button.x && MousePositionY <= maxAABB_button.y)
		{
			if (i == 0)
			{
				if (!ShopThings::GetInstance()->OmegacannonBought && ShopThings::GetInstance()->Currency >= ShopThings::GetInstance()->OmegacannonCost)
				{
					Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");
					if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
					{
						ShopThings::GetInstance()->Currency -= ShopThings::GetInstance()->OmegacannonCost;
						ShopThings::GetInstance()->OmegacannonBought = true;
					}
				}
			}
			else if (i == 1)
			{
				if (!ShopThings::GetInstance()->RobotBought && ShopThings::GetInstance()->Currency >= ShopThings::GetInstance()->RobotCost)
				{
					Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");
					if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
					{
						ShopThings::GetInstance()->Currency -= ShopThings::GetInstance()->RobotCost;
						ShopThings::GetInstance()->RobotBought = true;
					}
				}
			}
			else if (i == 2)
			{
				if (!ShopThings::GetInstance()->ScannerBought && ShopThings::GetInstance()->Currency >= ShopThings::GetInstance()->ScannerCost)
				{
					Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");
					if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
					{
						ShopThings::GetInstance()->Currency -= ShopThings::GetInstance()->ScannerCost;
						ShopThings::GetInstance()->ScannerBought = true;
					}
				}
			}

		}
		else
		{
			Buttons[i]->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_notpressed");
		}
	}

	{

		Vector3 minAABB_button = -ExitButton->GetScale() * 0.5 + Vector3(ExitButton->GetPosition().x, ExitButton->GetPosition().z);
		minAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		minAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();
		Vector3 maxAABB_button = ExitButton->GetScale() * 0.5 + Vector3(ExitButton->GetPosition().x, ExitButton->GetPosition().z);
		maxAABB_button.x *= (double)SceneManager::GetInstance()->windowWidth / (double)Application::GetInstance().GetWindowWidth();
		maxAABB_button.y *= (double)SceneManager::GetInstance()->windowHeight / (double)Application::GetInstance().GetWindowHeight();

		minAABB_button.x *= subwidth;
		minAABB_button.y *= subheight;
		maxAABB_button.x *= subwidth;
		maxAABB_button.y *= subheight;

		minAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		minAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;
		maxAABB_button.x += (centerX - 0.5) * (double)SceneManager::GetInstance()->windowWidth;
		maxAABB_button.y += (centerY - 0.5) * (double)SceneManager::GetInstance()->windowHeight;

		if (MousePositionX >= minAABB_button.x && MousePositionY >= minAABB_button.y && MousePositionX <= maxAABB_button.x && MousePositionY <= maxAABB_button.y)
		{

			ExitButton->modelMesh = MeshBuilder::GetInstance()->GetMesh("button_pressed");

				if (MouseController::GetInstance()->IsButtonPressed(MouseController::LMB))
				{
					//SceneManager::GetInstance()->RemoveSubScene("Shop");
					SceneManager::GetInstance()->RemoveSubScene_Queue("Shop");
				}

		}
		else
		{
			ExitButton->modelMesh = MeshBuilder::GetInstance()->GetMesh("closebutton");
		}
	}

}

void SceneShop::Render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	glEnable(GL_BLEND);

	Vector3 windowMinAABB = Vector3(minScreenAABB.x * 1000 - 500, minScreenAABB.y * 1000 - 500);
	Vector3 windowMaxAABB = Vector3(maxScreenAABB.x * 1000 - 500, maxScreenAABB.y * 1000 - 500);

	Vector3 windowCenter = windowMinAABB + (windowMaxAABB - windowMinAABB) * 0.5;

	GraphicsManager::GetInstance()->SetOrthographicProjection(-500, 500, -500, 500, -1000, 1000);

	GraphicsManager::GetInstance()->AttachCamera(&orthoCam);

	RenderHelper::PreRenderMesh();

	modelStack.PushMatrix();
	modelStack.Translate(-windowCenter.x, 0, windowCenter.y);
	modelStack.Scale(maxScreenAABB.x - minScreenAABB.x, 1, maxScreenAABB.y - minScreenAABB.y);

	modelStack.PushMatrix();
	modelStack.Translate(Box->GetPosition().x, Box->GetPosition().y, Box->GetPosition().z);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Scale(Box->GetScale().x, Box->GetScale().y, Box->GetScale().z);
	RenderHelper::RenderMesh(Box->modelMesh);
	modelStack.PopMatrix();

	for (int i = 0; i < 3; ++i)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-Buttons[i]->GetPosition().x, Buttons[i]->GetPosition().y, Buttons[i]->GetPosition().z);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Scale(Buttons[i]->GetScale().x, Buttons[i]->GetScale().y, Buttons[i]->GetScale().z);
		RenderHelper::RenderMesh(Buttons[i]->modelMesh);
		modelStack.PopMatrix();
	}

	modelStack.PushMatrix();
	modelStack.Translate(-ExitButton->GetPosition().x, ExitButton->GetPosition().y, ExitButton->GetPosition().z);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Scale(ExitButton->GetScale().x, ExitButton->GetScale().y, ExitButton->GetScale().z);
	RenderHelper::RenderMesh(ExitButton->modelMesh);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(450, 510, 450);
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Rotate(180, 0, 0, 1);
	modelStack.Scale(100, 100, 100);
	RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("money"));
	modelStack.PopMatrix();


	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;

	if (ShopThings::GetInstance()->OmegacannonBought)
	{

	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(halfWindowWidth * 0.8, 510, halfWindowHeight * 0.7);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Rotate(180, 0, 0, 1);
		modelStack.Scale(100, 100, 100);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("omega"));
		modelStack.PopMatrix();
	}
	if (ShopThings::GetInstance()->RobotBought)
	{

	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(halfWindowWidth * 0.8, 510, 0);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Rotate(180, 0, 0, 1);
		modelStack.Scale(100, 100, 100);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("robot"));
		modelStack.PopMatrix();
	}
	if (ShopThings::GetInstance()->ScannerBought)
	{

	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(halfWindowWidth * 0.8, 510, -halfWindowHeight * 0.7);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Rotate(180, 0, 0, 1);
		modelStack.Scale(100, 100, 100);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("scanner"));
		modelStack.PopMatrix();
	}

	RenderHelper::PostRenderMesh();

	modelStack.PopMatrix();

	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	// PreRenderText
	RenderHelper::PreRenderText();

	modelStack.PushMatrix();
	modelStack.Translate(windowCenter.x, windowCenter.y, 0);
	modelStack.Scale(maxScreenAABB.x - minScreenAABB.x, maxScreenAABB.y - minScreenAABB.y, 1);

	modelStack.PushMatrix();
	modelStack.Translate(-halfWindowWidth * 0.9, halfWindowHeight * 0.9, 10.0f);
	modelStack.Scale(50, 50, 50);
	RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), std::to_string(ShopThings::GetInstance()->Currency), Color(1.0f, 0.0f, 0.0f));
	modelStack.PopMatrix();


	if (ShopThings::GetInstance()->OmegacannonBought)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, halfWindowHeight * 0.7 - halfWindowHeight * 0.2, 10.0f);
		modelStack.Scale(50, 50, 50);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Sold out!", Color(1.0f, 0.0f, 0.0f));
		modelStack.PopMatrix();
	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, halfWindowHeight * 0.7 - halfWindowHeight * 0.2, 10.0f);
		modelStack.Scale(50, 50, 50);
		Color textcoltemp = Color(0, 1, 0);
		if (ShopThings::GetInstance()->Currency < ShopThings::GetInstance()->OmegacannonCost)
			textcoltemp = Color(1, 0, 0);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), std::to_string(ShopThings::GetInstance()->OmegacannonCost), textcoltemp);
		modelStack.PopMatrix();
	}
	if (ShopThings::GetInstance()->RobotBought)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, 0, 10.0f);
		modelStack.Scale(50, 50, 50);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Sold out!", Color(1.0f, 0.0f, 0.0f));
		modelStack.PopMatrix();
	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, 0, 10.0f);
		modelStack.Scale(50, 50, 50);
		Color textcoltemp = Color(0, 1, 0);
		if (ShopThings::GetInstance()->Currency < ShopThings::GetInstance()->RobotCost)
			textcoltemp = Color(1, 0, 0);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), std::to_string(ShopThings::GetInstance()->RobotCost), textcoltemp);
		modelStack.PopMatrix();
	}
	if (ShopThings::GetInstance()->ScannerBought)
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, -halfWindowHeight * 0.7 + halfWindowHeight * 0.2, 10.0f);
		modelStack.Scale(50, 50, 50);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Sold out!", Color(1.0f, 0.0f, 0.0f));
		modelStack.PopMatrix();
	}
	else
	{
		modelStack.PushMatrix();
		modelStack.Translate(-halfWindowWidth * 0.5, -halfWindowHeight * 0.7 + halfWindowHeight * 0.2, 10.0f);
		modelStack.Scale(50, 50, 50);
		Color textcoltemp = Color(0, 1, 0);
		if (ShopThings::GetInstance()->Currency < ShopThings::GetInstance()->ScannerCost)
			textcoltemp = Color(1, 0, 0);
		RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), std::to_string(ShopThings::GetInstance()->ScannerCost), textcoltemp);
		modelStack.PopMatrix();
	}

	//modelStack.PushMatrix();
	//modelStack.Translate(-halfWindowWidth * 0.3, 0, 10.0f);
	//modelStack.Scale(100, 125, 100);
	//RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Exit?", Color(1.0f, 0.0f, 0.0f));
	//modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(-halfWindowWidth * 0.4, -halfWindowHeight * 0.5, 10.0f);
	//modelStack.Scale(100, 125, 100);
	//RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "Yes!", Color(1.0f, 0.0f, 0.0f));
	//modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(halfWindowWidth * 0.3 + 100, -halfWindowHeight * 0.5, 10.0f);
	//modelStack.Scale(100, 125, 100);
	//RenderHelper::RenderText(MeshBuilder::GetInstance()->GetMesh("text"), "No!!", Color(1.0f, 0.0f, 0.0f));
	//modelStack.PopMatrix();

	modelStack.PopMatrix();

	RenderHelper::PostRenderText();

	// PostRenderMesh
	RenderHelper::PostRenderMesh();

	// Disable blend mode
	glDisable(GL_BLEND);
}

void SceneShop::Exit()
{
	if (Box != NULL)
	{
		delete Box;
		Box = NULL;
	}
	if (Buttons[0] != NULL)
	{
		delete Buttons[0];
		Buttons[0] = NULL;
	}
	if (Buttons[1] != NULL)
	{
		delete Buttons[1];
		Buttons[1] = NULL;
	}
	if (Buttons[2] != NULL)
	{
		delete Buttons[2];
		Buttons[2] = NULL;
	}
	if (ExitButton != NULL)
	{
		delete ExitButton;
		ExitButton = NULL;
	}

	ShopThings::GetInstance()->Shop_OverwriteLua();

	//	// Detach camera from other entities
	//	GraphicsManager::GetInstance()->DetachCamera();
	//	playerInfo->DetachCamera();
	//
	//	if (playerInfo->DropInstance() == false)
	//	{
	//#if _DEBUGMODE==1
	//		cout << "Unable to drop PlayerInfo class" << endl;
	//#endif
	//	}
	//
	//	GraphicsManager::GetInstance()->RemoveLight("lights[0]");
	//	GraphicsManager::GetInstance()->RemoveLight("lights[1]");
	//	CSceneGraph::GetInstance()->Destroy();
	//	// Delete the Spatial Partition
	//	CSpatialPartition::GetInstance()->Destroy();
	//	EntityManager::GetInstance()->Destroy();
}
