#include "Application.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "MeshBuilder.h"
#include "GL\glew.h"
#include "LoadTGA.h"
#include "ShaderProgram.h"

//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

//only 'Main Scenes' need to be in here, but subscenes must be altered a bit below

#include "SceneText.h"
#include "SceneText2.h"
#include "SceneMenu.h"
#include "SceneSplash.h"
#include "FPSCounter.h"
#include "LuaInterface.h"

GLFWwindow* m_window;
const unsigned char FPS = 60; // FPS of this game
const unsigned int frameTime = 1000 / FPS; // time for each frame

std::pair<double, double> Application::GetMousePos()
{
	double mouseX = 0, mouseY = 0;
	glfwGetCursorPos(m_window, &mouseX, &mouseY);
	return std::pair<double, double>(mouseX, mouseY);

}

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h);
}

bool Application::IsKeyPressed(unsigned short key)
{
    return ((GetAsyncKeyState(key) & 0x8001) != 0);
}

Application::Application()
{
}

Application::~Application()
{
}

void Application::HideCursor()
{
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}

void Application::ShowCursor()
{
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Application::Init()
{
	//if (CLuaManager::GetInstance()->Init("Image//DM2240.lua", "Image//DM2240_LuaFunctions.lua", true) == false)
	//{
	//	cout << "Unable to initialise the Lua system. Quitting the program now." << endl;
	//	return;
	//}

	//// Get the OpenGL resolution
	//int m_window_width = CLuaManager::GetInstance()->get<int>("WindowInfo.size.width");
	//int m_window_height = CLuaManager::GetInstance()->get<int>("WindowInfo.size.height");

	//float m_window_posX = CLuaManager::GetInstance()->get<float>("WindowInfo.position.x");
	//float m_window_posY = CLuaManager::GetInstance()->get<float>("WindowInfo.position.y");
	//Vector3 playerDirection = CLuaManager::GetInstance()->get<Vector3>("PlayerInfo.direction");
	//string m_sCallsign = CLuaManager::GetInstance()->get<string>("PlayerInfo.callsign");
	//int hp = CLuaManager::GetInstance()->get<int>("PlayerInfo.HP");

	//cout << "Position X = " << m_window_posX << ", Y = " << m_window_posY << endl;
	//cout << "Player Direction = " << playerDirection << endl;
	//cout << "CallSign:" << m_sCallsign << endl;
	//cout << "HP:" << hp << endl;

	//std::vector<int> v = CLuaManager::GetInstance()->getIntVector("Array");
	//std::cout << "Content of Array:";
	//for (std::vector<int>::iterator it = v.begin(); it != v.end(); it++)
	//{
	//	std::cout << *it << ",";
	//}
	//std::cout << std::endl;

	//// Getting HighScore table
	//std::vector<string> names;
	//std::vector<int> scores;
	//CLuaManager::GetInstance()->getStringVector("HighScore", names, scores);
	//cout << "Content of HighScore:";
	//for (int i = 0; i < (int)names.size(); i++)
	//{
	//	std::cout << names[i] << " : " << scores[i] << endl;
	//}
	//std::cout << std::endl;

	//float a = 1.0f, b = 2.0f, c = 3.0f, d = 4.0f;
	//int minValue = -1, maxValue = -1, avgValue = -1, numValues = 0;
	//CLuaManager::GetInstance()->getVariableValues("GetMinMax",
	//											minValue, maxValue, avgValue, numValues,
	//											4, a, b, c, d);
	//cout << "minValue : " << minValue <<
	//		", maxValue : " << maxValue <<
	//		", avgValue : " << avgValue <<
	//		", numValues : " << numValues << endl;

		// Initialise the Lua system
	CLuaInterface::GetInstance()->Init();
	//Get the OpenGL resolution
	CLuaInterface::GetInstance()->SetLuaFile("Image//UserConfig.lua", CLuaInterface::GetInstance()->theLuaState);
	m_window_width = CLuaInterface::GetInstance()->getIntValue("width");
	m_window_height = CLuaInterface::GetInstance()->getIntValue("height");
	CLuaInterface::GetInstance()->SetLuaFile("Image//SP3.lua", CLuaInterface::GetInstance()->theLuaState);
	CLuaInterface::GetInstance()->Run();

	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(m_window_width, m_window_height, "NYP Framework", NULL, NULL);

	SceneManager::GetInstance()->windowWidth = m_window_width;
	SceneManager::GetInstance()->windowHeight = m_window_height;

	//If the window couldn't be created
	if (!m_window)
	{
		fprintf( stderr, "Failed to open GLFW window.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	// Set windows position
	glfwSetWindowPos(m_window, 10, 30);

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	//glfwSetKeyCallback(m_window, key_callback);
	glfwSetWindowSizeCallback(m_window, resize_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK) 
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}

	// Hide the cursor
	
	//glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN); //hides cursor

	glfwSetMouseButtonCallback(m_window, &Application::MouseButtonCallbacks);
	glfwSetScrollCallback(m_window, &Application::MouseScrollCallbacks);

	// Init systems
	GraphicsManager::GetInstance()->Init();


	{
		ShaderProgram* currProg = GraphicsManager::GetInstance()->LoadShader("default", "Shader//comg.vertexshader", "Shader//comg.fragmentshader");
		// Tell the shader program to store these uniform locations
		currProg->AddUniform("MVP");
		currProg->AddUniform("MV");
		currProg->AddUniform("MV_inverse_transpose");
		currProg->AddUniform("material.kAmbient");
		currProg->AddUniform("material.kDiffuse");
		currProg->AddUniform("material.kSpecular");
		currProg->AddUniform("material.kShininess");
		currProg->AddUniform("lightEnabled");
		currProg->AddUniform("numLights");
		currProg->AddUniform("lights[0].type");
		currProg->AddUniform("lights[0].position_cameraspace");
		currProg->AddUniform("lights[0].color");
		currProg->AddUniform("lights[0].power");
		currProg->AddUniform("lights[0].kC");
		currProg->AddUniform("lights[0].kL");
		currProg->AddUniform("lights[0].kQ");
		currProg->AddUniform("lights[0].spotDirection");
		currProg->AddUniform("lights[0].cosCutoff");
		currProg->AddUniform("lights[0].cosInner");
		currProg->AddUniform("lights[0].exponent");
		currProg->AddUniform("lights[1].type");
		currProg->AddUniform("lights[1].position_cameraspace");
		currProg->AddUniform("lights[1].color");
		currProg->AddUniform("lights[1].power");
		currProg->AddUniform("lights[1].kC");
		currProg->AddUniform("lights[1].kL");
		currProg->AddUniform("lights[1].kQ");
		currProg->AddUniform("lights[1].spotDirection");
		currProg->AddUniform("lights[1].cosCutoff");
		currProg->AddUniform("lights[1].cosInner");
		currProg->AddUniform("lights[1].exponent");
		currProg->AddUniform("colorTextureEnabled");
		currProg->AddUniform("colorTexture");
		currProg->AddUniform("textEnabled");
		currProg->AddUniform("textColor");

		currProg->AddUniform("X_MIN");
		currProg->AddUniform("X_MAX");
		currProg->AddUniform("Y_MIN");
		currProg->AddUniform("Y_MAX");
		currProg->AddUniform("stretch");

		// Tell the graphics manager to use the shader we just loaded
		GraphicsManager::GetInstance()->SetActiveShader("default");
	}


	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_DATA.lua", CLuaInterface::GetInstance()->theLuaState);
	{

		MeshBuilder::GetInstance()->GenerateCrossHair(CLuaInterface::GetInstance()->getStringValue("Crosshair"));
		
		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "text");
		MeshBuilder::GetInstance()->GenerateText(CLuaInterface::GetInstance()->getFieldString("Name"), CLuaInterface::GetInstance()->getFieldFloat("row"), CLuaInterface::GetInstance()->getFieldFloat("column"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->material.kAmbient.Set(CLuaInterface::GetInstance()->getFieldVector("kAmbient"));

		#pragma region Generate Quad

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "quad");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "bluebox");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "menuscreen");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "splashscreen");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "button_notpressed");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "button_pressed");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GRASS_DARKGREEN");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GEO_GRASS_LIGHTGREEN");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_FRONT");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_BACK");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_LEFT");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_RIGHT");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_TOP");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "SKYBOX_BOTTOM");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Floor_HighLOD");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Floor_MidLOD");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "Floor_LowLOD");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "EMPTYMESH");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "GRIDMESH");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "ExploIndi");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "ExploRad");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "ProjTile");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "EXPLOMESH");		//supposedly unused idk what you been doing
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "CAMERAEFFECTS_BLOODSCREEN");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "CAMERAEFFECTS_SCOPESCREEN");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "MINIMAP");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "MINIMAPAVATAR");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "money");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "closebutton");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "scanner");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "robot");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "omega");
		MeshBuilder::GetInstance()->GenerateQuad(CLuaInterface::GetInstance()->getFieldString("Name"), Color(CLuaInterface::GetInstance()->getFieldVector("Color")), CLuaInterface::GetInstance()->getFieldFloat("length"));
		MeshBuilder::GetInstance()->GetMesh(CLuaInterface::GetInstance()->getFieldString("Name"))->textureID = LoadTGA(CLuaInterface::GetInstance()->getFieldString("Texture").c_str());




		#pragma endregion


		MeshBuilder::GetInstance()->GenerateOBJ("Chair", "OBJ//chair.obj");
		MeshBuilder::GetInstance()->GetMesh("Chair")->textureID = LoadTGA("Image//chair.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Enemy_HighLOD", "OBJ//Scooter.obj");
		MeshBuilder::GetInstance()->GetMesh("Enemy_HighLOD")->textureID = LoadTGA("Image//floorHighLOD.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Enemy_MidLOD", "OBJ//scootermid.obj");
		MeshBuilder::GetInstance()->GetMesh("Enemy_MidLOD")->textureID = LoadTGA("Image//floorMidLOD.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Enemy_LowLOD", "OBJ//scooterlow.obj");
		MeshBuilder::GetInstance()->GetMesh("Enemy_LowLOD")->textureID = LoadTGA("Image//floorLowLOD.tga");

		MeshBuilder::GetInstance()->GenerateRing("ring", Color(1, 0, 1), 36, 1, 0.5f);

		MeshBuilder::GetInstance()->GenerateSphere("lightball", Color(1, 1, 1), 18, 36, 1.f);

		MeshBuilder::GetInstance()->GenerateSphere("sphere", Color(1, 0, 0), 18, 36, 1.f);

		MeshBuilder::GetInstance()->GenerateSphere("HighLOD", Color(1, 0, 0), 6, 12, 5.f);

		MeshBuilder::GetInstance()->GenerateSphere("MidLOD", Color(1, 1, 0), 6, 12, 5.f);

		MeshBuilder::GetInstance()->GenerateSphere("LowLOD", Color(0, 1, 0), 6, 12, 5.f);

		MeshBuilder::GetInstance()->GenerateSphere("PlayerPoint", Color(0, 0, 1), 6, 12, 5.f);

		MeshBuilder::GetInstance()->GenerateCone("cone", Color(0.5f, 1, 0.3f), 36, 10.f, 10.f);
		MeshBuilder::GetInstance()->GetMesh("cone")->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);
		MeshBuilder::GetInstance()->GetMesh("cone")->material.kSpecular.Set(0.f, 0.f, 0.f);

		MeshBuilder::GetInstance()->GenerateCube("cube", Color(1.0f, 1.0f, 0.0f), 1.0f);

		MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_HighLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("MantisSkin_HighLOD")->textureID = LoadTGA("Image//HighLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_MidLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("MantisSkin_MidLOD")->textureID = LoadTGA("Image//MidLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("MantisSkin_LowLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("MantisSkin_LowLOD")->textureID = LoadTGA("Image//LowLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_HighLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("SpiderSkin_HighLOD")->textureID = LoadTGA("Image//HighLODSpider.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_MidLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("SpiderSkin_MidLOD")->textureID = LoadTGA("Image//MidLODSpider.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("SpiderSkin_LowLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("SpiderSkin_LowLOD")->textureID = LoadTGA("Image//LowLODSpider.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Metal_HighLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("Metal_HighLOD")->textureID = LoadTGA("Image//HighLODMetal.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Metal_MidLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("Metal_MidLOD")->textureID = LoadTGA("Image//MidLODMetal.tga");

		MeshBuilder::GetInstance()->GenerateOBJ("Metal_LowLOD", "OBJ//specialCube.obj");
		MeshBuilder::GetInstance()->GetMesh("Metal_LowLOD")->textureID = LoadTGA("Image//LowLODMetal.tga");

		MeshBuilder::GetInstance()->GenerateSphere("BallMetal_HighLOD", Color(1, 0, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GetMesh("BallMetal_HighLOD")->textureID = LoadTGA("Image//HighLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateSphere("BallMetal_MidLOD", Color(1, 0, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GetMesh("BallMetal_MidLOD")->textureID = LoadTGA("Image//MidLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateSphere("BallMetal_LowLOD", Color(1, 0, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GetMesh("BallMetal_LowLOD")->textureID = LoadTGA("Image//LowLODMantis.tga");

		MeshBuilder::GetInstance()->GenerateSphere("reds", Color(1, 0, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GenerateSphere("yellows", Color(1, 1, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GenerateSphere("greens", Color(0, 1, 0), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GenerateSphere("blues", Color(0, 0, 1), 18, 36, 1.f);
		MeshBuilder::GetInstance()->GenerateSphere("purples", Color(1, 0, 1), 18, 36, 1.f);

		MeshBuilder::GetInstance()->GenerateRay("laser", Color(0, 1, 0), 10.0f);

		MeshBuilder::GetInstance()->GenerateOBJ("Target", "OBJ//target.obj");
		MeshBuilder::GetInstance()->GetMesh("Target")->textureID = LoadTGA("Image//target.tga");

		MeshBuilder::GetInstance()->GenerateCircle("MINIMAPBORDER", Color(1, 1, 1), 1.05f);
		MeshBuilder::GetInstance()->GenerateCircle("MINIMAP_STENCIL", Color(1, 1, 1), 1.0f);

		MeshBuilder::GetInstance()->GenerateQuad("money", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("money")->textureID = LoadTGA("Image//currency.tga");
		MeshBuilder::GetInstance()->GenerateQuad("closebutton", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("closebutton")->textureID = LoadTGA("Image//stopsign.tga");

		MeshBuilder::GetInstance()->GenerateQuad("scanner", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("scanner")->textureID = LoadTGA("Image//scanner.tga");
		MeshBuilder::GetInstance()->GenerateQuad("robot", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("robot")->textureID = LoadTGA("Image//robot.tga");
		MeshBuilder::GetInstance()->GenerateQuad("omega", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("omega")->textureID = LoadTGA("Image//omega.tga");

		MeshBuilder::GetInstance()->GenerateQuad("waypoint", Color(1, 1, 1), 1.f);
		MeshBuilder::GetInstance()->GetMesh("waypoint")->textureID = LoadTGA("Image//waypoint.tga");
		MeshBuilder::GetInstance()->GenerateQuad("line", Color(0, 1, 0), 1.f);

	}

	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_FSMDATA.lua", CLuaInterface::GetInstance()->theLuaState);
	SceneManager::GetInstance()->totalNormalEnemy = CLuaInterface::GetInstance()->getIntValue("amountofentities");
	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);

	SetInputs();
}

void Application::Run()
{
	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_SCENEDATA.lua", CLuaInterface::GetInstance()->theLuaState);

	SceneManager::GetInstance()->SetActiveScene(CLuaInterface::GetInstance()->getStringValue("activescene"));

	for (int i = 0; i < 7; ++i)
	{
		std::string id = "s" + std::to_string(i);
		std::string SceneName = "";
		Vector3 SceneMinAABB, SceneMaxAABB;

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "allscenes");
		SceneName = CLuaInterface::GetInstance()->getFieldString(id.c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "mouseable");
		SceneManager::GetInstance()->EditSceneMouseable(SceneName, CLuaInterface::GetInstance()->getFieldFloat(id.c_str()));

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "elevation");
		SceneManager::GetInstance()->EditSubSceneElevation(SceneName, CLuaInterface::GetInstance()->getFieldFloat(id.c_str()));

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "minAABB");
		SceneMinAABB = CLuaInterface::GetInstance()->getFieldVector(id.c_str());

		lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "maxAABB");
		SceneMaxAABB = CLuaInterface::GetInstance()->getFieldVector(id.c_str());

		SceneManager::GetInstance()->EditSceneAABB(SceneName, SceneMinAABB.x, SceneMinAABB.y, SceneMaxAABB.x, SceneMaxAABB.y);
	}

	//SceneManager::GetInstance()->SetActiveScene("Splash");

	//SceneManager::GetInstance()->EditSceneMouseable("Splash", true);
	//SceneManager::GetInstance()->EditSceneMouseable("Menu", true);
	//SceneManager::GetInstance()->EditSceneMouseable("ExitNotif", true);
	//SceneManager::GetInstance()->EditSceneMouseable("Shop", true);
	//SceneManager::GetInstance()->EditSceneMouseable("Start", false);
	//SceneManager::GetInstance()->EditSceneMouseable("Start2", false);
	//SceneManager::GetInstance()->EditSceneMouseable("Pause", true);

	//SceneManager::GetInstance()->EditSubSceneElevation("Start2", -500);
	//SceneManager::GetInstance()->EditSubSceneElevation("ExitNotif", 200);
	//SceneManager::GetInstance()->EditSubSceneElevation("Pause", 200);
	//SceneManager::GetInstance()->EditSubSceneElevation("Shop", 100);

	//SceneManager::GetInstance()->EditSceneAABB("Start2", 0.0, 0, 0.4, 0.5);
	//SceneManager::GetInstance()->EditSceneAABB("ExitNotif", 0.1, 0.4, 0.4, 0.6);
	//SceneManager::GetInstance()->EditSceneAABB("Pause", 0.2, 0.2, 0.8, 0.8);
	//SceneManager::GetInstance()->EditSceneAABB("Shop", 0.6, 0, 1.0, 1.0);

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);

	m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame

	// Initialise the FPS counter
	CFPSCounter::GetInstance()->Init();
	double dElapsedTime = 0.0;

	while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE) && !Terminate)
	{
		glfwPollEvents();
		UpdateInput();

		int *windowCurrwidth = new int, *windowCurrheight = new int;
		glfwGetWindowSize(m_window, windowCurrwidth, windowCurrheight);
		SceneManager::GetInstance()->windowWidth = *windowCurrwidth;
		SceneManager::GetInstance()->windowHeight = *windowCurrheight;
		////std::cout << *windowCurrwidth << std::endl;
		//resize_callback_butthisismine(m_window, 0.1 * (*windowCurrwidth), 0.1* (*windowCurrheight), 0.8* (*windowCurrwidth), 0.8* (*windowCurrheight));
		//
		delete windowCurrwidth;
		delete windowCurrheight;

		dElapsedTime = m_timer.getElapsedTime();
		// Update the FPS counter
		CFPSCounter::GetInstance()->Update(dElapsedTime);
		SceneManager::GetInstance()->Update(dElapsedTime);
		SceneManager::GetInstance()->Render();

		//Swap buffers
		glfwSwapBuffers(m_window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...

        m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   
		
		PostInputUpdate();

		if (KeyboardController::GetInstance()->IsKeyDown(keyFocus))
		{
			if (SceneManager::GetInstance()->focusedScene != NULL)
			{
				SceneManager::GetInstance()->focusedScene->CurrentScreen = false;
				SceneManager::GetInstance()->focusedScene = NULL;
			}
		}

		if (KeyboardController::GetInstance()->IsKeyDown(keyValueSet)) //update scene variables
		{
			std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
			CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_SCENEDATA.lua", CLuaInterface::GetInstance()->theLuaState);
			for (int i = 0; i < 7; ++i)
			{
				std::string id = "s" + std::to_string(i);
				std::string SceneName = "";
				Vector3 SceneMinAABB, SceneMaxAABB;

				lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "allscenes");
				SceneName = CLuaInterface::GetInstance()->getFieldString(id.c_str());

				lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "mouseable");
				SceneManager::GetInstance()->EditSceneMouseable(SceneName, CLuaInterface::GetInstance()->getFieldFloat(id.c_str()));

				lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "elevation");
				SceneManager::GetInstance()->EditSubSceneElevation(SceneName, CLuaInterface::GetInstance()->getFieldFloat(id.c_str()));

				lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "minAABB");
				SceneMinAABB = CLuaInterface::GetInstance()->getFieldVector(id.c_str());

				lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "maxAABB");
				SceneMaxAABB = CLuaInterface::GetInstance()->getFieldVector(id.c_str());

				SceneManager::GetInstance()->EditSceneAABB(SceneName, SceneMinAABB.x, SceneMinAABB.y, SceneMaxAABB.x, SceneMaxAABB.y);
			}

			CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);

		}

		if (KeyboardController::GetInstance()->IsKeyDown(keySetCurrent)) //set current scene based on lua file
		{
			std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
			CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_SCENEDATA.lua", CLuaInterface::GetInstance()->theLuaState);
			SceneManager::GetInstance()->SetActiveScene(CLuaInterface::GetInstance()->getStringValue("activescene"));
			CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);
		}

		if (MouseController::GetInstance()->IsButtonDown(MouseController::LMB) && SceneManager::GetInstance()->focusedScene == NULL)
		{
			SceneManager::GetInstance()->SetFocusScene(GetMousePos().first, GetMousePos().second);
		}

		if (SceneManager::GetInstance()->showcursor)
			ShowCursor();
		else
			HideCursor();
			
	}
	SceneManager::GetInstance()->Exit();
}

void Application::Exit()
{
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}

void Application::UpdateInput()
{
	// Update Mouse Position
	double mouse_currX, mouse_currY;
	glfwGetCursorPos(m_window, &mouse_currX, &mouse_currY);
	MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);

	// Update Keyboard Input
	for (int i = 0; i < KeyboardController::MAX_KEYS; ++i)
		KeyboardController::GetInstance()->UpdateKeyboardStatus(i, IsKeyPressed(i));
}

void Application::resize_callback_butthisismine(GLFWwindow* window, int w0, int h0, int w1, int h1)
{
	glViewport(w0, h0, w1, h1);
}

void Application::PostInputUpdate()
{
	// If mouse is centered, need to update the center position for next frame
	bool dontallowmouse = true;
	if (SceneManager::GetInstance()->focusedScene != NULL)
	{
		if (SceneManager::GetInstance()->focusedScene->AllowMouseToAppear)
		{
			dontallowmouse = false;
		}
	}
	else dontallowmouse = false;

	if (MouseController::GetInstance()->GetKeepMouseCentered() && dontallowmouse)
	{
		double mouse_currX, mouse_currY;
		mouse_currX = m_window_width >> 1;
		mouse_currY = m_window_height >> 1;
		MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);
		glfwSetCursorPos(m_window, mouse_currX, mouse_currY);
	}

	// Call input systems to update at end of frame
	MouseController::GetInstance()->EndFrameUpdate();
	KeyboardController::GetInstance()->EndFrameUpdate();
}

void Application::MouseButtonCallbacks(GLFWwindow* window, int button, int action, int mods)
{
	// Send the callback to the mouse controller to handle
	if (action == GLFW_PRESS)
		MouseController::GetInstance()->UpdateMouseButtonPressed(button);
	else
		MouseController::GetInstance()->UpdateMouseButtonReleased(button);
}

void Application::MouseScrollCallbacks(GLFWwindow* window, double xoffset, double yoffset)
{
	MouseController::GetInstance()->UpdateMouseScroll(xoffset, yoffset);
}

int Application::GetWindowHeight()
{
	return m_window_height;
}

int Application::GetWindowWidth()
{
	return m_window_width;
}

void Application::Resize(int w0, int h0, int w1, int h1)
{
	int *windowCurrwidth = new int, *windowCurrheight = new int;
	glfwGetWindowSize(m_window, windowCurrwidth, windowCurrheight);
	resize_callback_butthisismine(m_window, w0 * (*windowCurrwidth), h0 * (*windowCurrheight), w1 * (*windowCurrwidth), h1 * (*windowCurrheight));
	delete windowCurrwidth;
	delete windowCurrheight;
}

void Application::SetInputs()
{
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_INPUT.lua", CLuaInterface::GetInstance()->theLuaState);

	keyW = CLuaInterface::GetInstance()->getCharValue("W");
	keyS = CLuaInterface::GetInstance()->getCharValue("S");
	keyA = CLuaInterface::GetInstance()->getCharValue("A");
	keyD = CLuaInterface::GetInstance()->getCharValue("D");

	keyReload = CLuaInterface::GetInstance()->getCharValue("Reload");
	keyReset = CLuaInterface::GetInstance()->getCharValue("Reset");
	keyFocus = CLuaInterface::GetInstance()->getCharValue("Focus");
	keyValueSet = CLuaInterface::GetInstance()->getCharValue("ValueSet");
	keySetCurrent = CLuaInterface::GetInstance()->getCharValue("SetCurrent");
	keyPause = CLuaInterface::GetInstance()->getCharValue("Pause");
	keyShop = CLuaInterface::GetInstance()->getCharValue("Shop");
	keyScene2On = CLuaInterface::GetInstance()->getCharValue("Scene2On");
	keyCreateBoss = CLuaInterface::GetInstance()->getCharValue("CreateBoss");
	keyCreateEnemies = CLuaInterface::GetInstance()->getCharValue("CreateEnemies");
	keyToggleSpeed = CLuaInterface::GetInstance()->getCharValue("ToggleSpeed");
	keyToggleTopView = CLuaInterface::GetInstance()->getCharValue("ToggleTopView");
	keyToggleQuadtree = CLuaInterface::GetInstance()->getCharValue("ToggleQuadtree");
	keyToggleQuadMarker = CLuaInterface::GetInstance()->getCharValue("ToggleQuadMarker");
	keyGenerateLua = CLuaInterface::GetInstance()->getCharValue("GenerateLua");
	keyLuaModifyFSM = CLuaInterface::GetInstance()->getCharValue("LuaModifyFSM");
	keyScene2Off = CLuaInterface::GetInstance()->getCharValue("Scene2Off");

}