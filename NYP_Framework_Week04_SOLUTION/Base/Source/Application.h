#ifndef APPLICATION_H
#define APPLICATION_H

#include "timer.h"
#include <irrKlang.h>
#include <iostream>

using namespace irrklang;

struct GLFWwindow;

class Application
{
public:
	static Application& GetInstance()
	{
		static Application app;
		return app;
	}
	void Init();
	void Run();
	void Exit();

	void UpdateInput();
	void PostInputUpdate();
	
	void HideCursor();
	void ShowCursor();

	std::pair<double, double> GetMousePos();

	static void MouseButtonCallbacks(GLFWwindow* window, int button, int action, int mods);
	static void MouseScrollCallbacks(GLFWwindow* window, double xoffset, double yoffset);

	void Resize(int w0, int h0, int w1, int h1);

	void resize_callback_butthisismine(GLFWwindow* window, int w0, int h0, int w1, int h1);

	int GetWindowHeight();
	int GetWindowWidth();

	void SetInputs();

	bool Terminate = false;

	#pragma region Control and Inputs

	char keyW;
	char keyS;
	char keyA;
	char keyD;

	char keyReload;
	char keyReset;
	char keyFocus;

	char keyValueSet;
	char keySetCurrent;
	char keyPause;
	char keyShop;
	char keyScene2On;
	char keyCreateBoss;
	char keyCreateEnemies;
	char keyToggleSpeed;
	char keyToggleTopView;
	char keyToggleQuadtree;
	char keyToggleQuadMarker;
	char keyGenerateLua;
	char keyLuaModifyFSM;
	char keyScene2Off;

	#pragma endregion

private:
	Application();
	~Application();

	static bool IsKeyPressed(unsigned short key);

	// Should make these not hard-coded :P
	int m_window_width;
	int m_window_height;

	//Declare a window object
	StopWatch m_timer;



};

#endif