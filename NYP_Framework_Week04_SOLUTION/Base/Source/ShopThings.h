#ifndef SHOP_THINGS_H
#define SHOP_THINGS_H

#include "SingletonTemplate.h"
#include <list>
#include "Vector3.h"

class ShopThings : public Singleton<ShopThings>
{
	friend Singleton<ShopThings>;
public:

	int Currency = 75;
	bool ScannerBought = false; //25 cost
	bool OmegacannonBought = false; //100 cost, unattainable
	bool RobotBought = false; //40 cost

	int ScannerCost = 25;
	int OmegacannonCost = 100;
	int RobotCost = 40;

	ShopThings()
	{
		Currency = 75;
	}
	virtual ~ShopThings()
	{

	}

	void Shop_ReadLua();
	void Shop_OverwriteLua();

};

#endif