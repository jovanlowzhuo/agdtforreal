#include "WaypointGraph.h"

#include <fstream>
#include <string>

#include "Lua/LuaInterface.h"

Graph::Graph()
{

}

Graph::~Graph()
{
	int edgesize;
	edgesize = m_nodes.size();
	for (std::map<int, Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		delete (*it).second;
	}
	m_nodes.clear();
	edgesize = m_edges.size();
	for (int i = 0; i < edgesize; ++i)
	{
		delete m_edges[i];
	}
	m_edges.clear();
}

void Graph::AddNode_File(int ID, Node* _node)
{
	m_nodes.insert(std::pair<int, Node*>(ID, _node));
}

void Graph::AddNode(Vector3 pos)
{
	int ID = Math::RandIntMinMax(-999999, 999999);
	while (m_nodes.count(ID))
	{
		ID = Math::RandIntMinMax(-999999, 999999);
	}
	m_nodes.insert(std::pair<int, Node*>(ID, new Node(pos)));
}

void Graph::AddEdge(std::pair<int, Node*> start, std::pair<int, Node*> end)
{
	//Edge* edge;
	//edge->from = start;
	//edge->to = end;
	//edge->cost = (end->pos - start->pos).Length();
	//start->edges.push_back(edge->pos);
	//end->edges.push_back(edge);
	//m_edges.push_back(edge);

	Edge* edge = new Edge();
	edge->from.first = start.first;
	edge->to.first = end.first;
	edge->from.second = start.second->pos;
	edge->to.second = end.second->pos;
	edge->cost = (end.second->pos - start.second->pos).Length();
	start.second->edges.push_back(edge);
	end.second->edges.push_back(edge);
	m_edges.push_back(edge);
}

void Graph::Generate(unsigned key, int allNodes, int totalEdgesPerNode)
{

	srand(key);

	for (int i = 0; i < allNodes; ++i)
	{
		AddNode(Vector3(Math::RandFloatMinMax(0, 100), Math::RandFloatMinMax(0, 100), 0));
	}

	for (std::map<int, Node *>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
	{
		for (std::map<int, Node *>::iterator it2 = it; it2 != m_nodes.end(); ++it2)
		{
			if ((*it) != (*it2))
			{
				if ((*it).second->edges.size() < totalEdgesPerNode && (*it2).second->edges.size() < totalEdgesPerNode)
				{
					int addOrNot = Math::RandIntMinMax(0, 10);
					if (addOrNot >= 9)
					{
						//AddEdge((*it).second, (*it2).second);
						AddEdge((*it), (*it2));
					}
				}
			}
		}
	}

}

void Graph::LoadCSV(const std::string filename)
{
	std::ifstream file;
	file.open(filename);

	while (file.peek() != 'N')
	{
		file.ignore(1);
	}
	while (!file.eof() && file.peek() != ' ')
	{
		std::string readLine = "";
		std::getline(file, readLine, ',');
		//std::cout << readLine << std::endl;
		if (readLine == "Node")//get Pos and ID
		{
			std::string readPos[3];
			std::getline(file, readPos[0], ',');
			std::getline(file, readPos[1], ',');
			std::getline(file, readPos[2], ',');
			Node *newnode = new Node(Vector3(std::stof(readPos[0]), std::stof(readPos[1]), std::stof(readPos[2])));
			std::getline(file, readLine);
			AddNode_File(std::stoi(readLine), newnode);
		}
		else if (readLine == "Edge")//get the 2 IDs and create an Edge
		{
			std::string readID[2];
			std::getline(file, readID[0], ',');
			std::getline(file, readID[1]);

			if (m_nodes.count(std::stoi(readID[0])) > 0 && m_nodes.count(std::stoi(readID[1])) > 0)
			{
				bool CanAddEdge = true;
				std::vector<Edge*> edgevector;
				edgevector = m_nodes.at(std::stoi(readID[0]))->edges;
				for (int i = 0; i < edgevector.size(); ++i)
				{
					if (edgevector[i]->from.first == std::stoi(readID[1]) || edgevector[i]->to.first == std::stoi(readID[1]))
					{
						CanAddEdge = false;
						break;
					}
				}
				if (CanAddEdge)
				{
					edgevector = m_nodes.at(std::stoi(readID[1]))->edges;
					for (int i = 0; i < edgevector.size(); ++i)
					{
						if (edgevector[i]->from.first == std::stoi(readID[0]) || edgevector[i]->to.first == std::stoi(readID[0]))
						{
							CanAddEdge = false;
							break;
						}
					}
				}

				if (CanAddEdge)
				{
					AddEdge(std::pair<int, Node*>(std::stoi(readID[0]), m_nodes.at(std::stoi(readID[0]))), std::pair<int, Node*>(std::stoi(readID[1]), m_nodes.at(std::stoi(readID[1]))));
				}
			}

		}
		else if (readLine == "Preset")
		{
			std::string readID[2];
			std::getline(file, readID[0], ',');
			std::getline(file, readID[1]);

			if (m_nodes.count(std::stoi(readID[0])) != 0)
			{
				m_nodes.at(std::stoi(readID[0]))->nextNode = std::stoi(readID[1]);
			}

		}
		else
		{
			std::getline(file, readLine);
		}

	}

}

std::vector<Vector3> Graph::PathFinding(const Vector3& curr, const Vector3& destination)
{
	std::vector<Vector3> finalpath;

	std::map<Node*, Node*> prevNode; //Node, then previous node from it

	std::list<std::pair<Node*, int>> AStarList; //Node + Cost

	Node* startNode = GetNearestNode(curr);
	Node* endNode = GetNearestNode(destination);

	std::map<Node*, bool> visitedNodes;

	Node* currentNode = startNode;
	int CurrentCost = 0;

	AStarList.push_front(std::pair<Node*, int>(currentNode, CurrentCost));
	visitedNodes.insert(std::pair<Node*, bool>(currentNode, true));

	while (AStarList.size() > 0)
	{
		//Step 1 : Arrange AStarList in highest->lowest cost order (lowest will be pushed first)
		std::vector<std::pair<Node*, int>> templist;
		std::list<std::pair<Node*, int>> tempqueue;
		for (std::list<std::pair<Node*, int>>::iterator it = AStarList.begin(); it != AStarList.end(); ++it)
		{
			templist.push_back(*it);
		}

		//sort highest->lowest cost
		for (int iter = 1; iter < AStarList.size(); iter++)
		{
			for (int index = 0; index < AStarList.size() - iter; index++)
			{
				if (templist[index].second < templist[index + 1].second)
				{
					std::pair<Node*, int> temp = templist[index];
					templist[index] = templist[index + 1];
					templist[index + 1] = temp;
				}
			}
		}

		for (std::vector<std::pair<Node*, int>>::iterator it = templist.begin(); it != templist.end(); ++it)
		{
			tempqueue.push_back(*it);
		}

		AStarList = tempqueue;

		templist.clear();
		tempqueue.clear();

		//Step 2 : Push queue
		currentNode = AStarList.back().first;
		CurrentCost = AStarList.back().second;
		AStarList.pop_back();

		//Step 3 : Check if you're at destination already
		if (currentNode == endNode)
		{
			finalpath.push_back(destination);
			while (!(currentNode == startNode))
			{
				finalpath.push_back(currentNode->pos);
				currentNode = prevNode.at(currentNode);
			}
			finalpath.push_back(currentNode->pos); //?
			return finalpath;
		}

		//Step 4 : If not at destination yet, make sure to keep on searching!
		for (int i = 0; i < currentNode->edges.size(); ++i)
		{
			Node* otherNode = NULL;

			if (m_nodes.at(currentNode->edges[i]->from.first) != currentNode)
			{
				otherNode = m_nodes.at(currentNode->edges[i]->from.first);
			}
			else if (m_nodes.at(currentNode->edges[i]->to.first) != currentNode)
			{
				otherNode = m_nodes.at(currentNode->edges[i]->to.first);
			}

			if (otherNode != NULL)
			{
				if (visitedNodes.count(otherNode) == 0) //havent visited before
				{
					prevNode.insert(std::pair<Node*, Node*>(otherNode, currentNode));
					AStarList.push_front(std::pair<Node*, int>(otherNode, CurrentCost + currentNode->edges[i]->cost));
					visitedNodes.insert(std::pair<Node*, bool>(otherNode, true));
				}
				else //visited before
				{
					bool DoneAStarMagic = false;

					for (std::list<std::pair<Node*, int>>::iterator it = AStarList.begin(); it != AStarList.end(); ++it)
					{
						if ((*it).first == otherNode)
						{						
							
							if (CurrentCost + currentNode->edges[i]->cost < (*it).second)
							{
								(*it).second = CurrentCost + currentNode->edges[i]->cost;
								DoneAStarMagic = true;
							}
							break;
						}
					}

					if (DoneAStarMagic)
					{
						//std::cout << "xd" << std::endl;
						prevNode.at(otherNode) = currentNode;
					}

				}
			}

		}


	}

	return finalpath; //size 0, failed
}

void Graph::Generate_Lua()
{
	{
		int edgesize;
		edgesize = m_nodes.size();
		for (std::map<int, Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
		{
			delete (*it).second;
		}
		m_nodes.clear();
		edgesize = m_edges.size();
		for (int i = 0; i < edgesize; ++i)
		{
			delete m_edges[i];
		}
		m_edges.clear();
	}

	std::string prevfile = CLuaInterface::GetInstance()->GetFileName();
	CLuaInterface::GetInstance()->SetLuaFile("Image//AGDEV_WAYPOINTDATA.lua", CLuaInterface::GetInstance()->theLuaState);
	
	int totalLuaGraphNodes = CLuaInterface::GetInstance()->getIntValue("totalnodes");
	int totalLuaGraphEdges = CLuaInterface::GetInstance()->getIntValue("totaledges");

	Vector3 throwaway;
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "nodes");
	for (int i = 0; i < totalLuaGraphNodes; ++i)
	{
		Node *newnode = new Node(CLuaInterface::GetInstance()->getFieldVector(("n" + std::to_string(i)).c_str()));
		AddNode_File(i, newnode);
	}

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "edges");
	for (int i = 0; i < totalLuaGraphEdges; ++i)
	{
		throwaway = CLuaInterface::GetInstance()->getFieldVector(("e" + std::to_string(i)).c_str());

		if (m_nodes.count((int)throwaway.x) > 0 && m_nodes.count((int)throwaway.y) > 0)
		{
			bool CanAddEdge = true;
			std::vector<Edge*> edgevector;
			edgevector = m_nodes.at((int)throwaway.x)->edges;
			for (int i = 0; i < edgevector.size(); ++i)
			{
				if (edgevector[i]->from.first == (int)throwaway.y || edgevector[i]->to.first == (int)throwaway.y)
				{
					CanAddEdge = false;
					break;
				}
			}
			if (CanAddEdge)
			{
				edgevector = m_nodes.at((int)throwaway.y)->edges;
				for (int i = 0; i < edgevector.size(); ++i)
				{
					if (edgevector[i]->from.first == (int)throwaway.x || edgevector[i]->to.first == (int)throwaway.x)
					{
						CanAddEdge = false;
						break;
					}
				}
			}

			if (CanAddEdge)
			{
				AddEdge(std::pair<int, Node*>((int)throwaway.x, m_nodes.at((int)throwaway.x)), std::pair<int, Node*>((int)throwaway.y, m_nodes.at((int)throwaway.y)));
			}
		}

	}

	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "presets");
	for (int i = 0; i < totalLuaGraphNodes; ++i)
	{
		throwaway = CLuaInterface::GetInstance()->getFieldVector(("p" + std::to_string(i)).c_str());

		if (m_nodes.count((int)throwaway.x) != 0)
		{
			m_nodes.at((int)throwaway.x)->nextNode = (int)throwaway.y;
		}
	}

	CLuaInterface::GetInstance()->SetLuaFile(prevfile, CLuaInterface::GetInstance()->theLuaState);
}