defaultstate="idle"

transitioncount=4

t0={
	currentstate="idle",
	nextstate="escape",
	condition="nearplayer"
}
t1={
	currentstate="idle",
	nextstate="chase",
	condition="awayplayer"
}
t2={
	currentstate="chase",
	nextstate="idle",
	condition="tenseconds"
}
t3={
	currentstate="escape",
	nextstate="idle",
	condition="tenseconds"
}