

activescene="Splash"

allscenes={
	s0="Splash",
	s1="Menu",
	s2="ExitNotif", 
	s3="Shop",
	s4="Start",
	s5="Start2",
	s6="Pause"
}

mouseable={
	s0=1,
	s1=1,
	s2=1, 
	s3=1,
	s4=0,
	s5=0,
	s6=1
}

elevation={
	s0=0,
	s1=0,
	s2=200, 
	s3=100,
	s4=0,
	s5=-500,
	s6=200
}

minAABB={
	s0 = {0,0,0},
	s1 = {0,0,0},
	s2 = {0.1,0.4,0}, 
	s3 = {0.6,0,0},
	s4 = {0,0,0},
	s5 = {0,0,0},
	s6 = {0.2,0.2,0}
}

maxAABB={
	s0 = {1,1,0},
	s1 = {1,1,0},
	s2 = {0.4,0.6,0}, 
	s3 = {1,1,0},
	s4 = {1,1,0},
	s5 = {0.4,0.5,0},
	s6 = {0.8,0.8,0}
}

