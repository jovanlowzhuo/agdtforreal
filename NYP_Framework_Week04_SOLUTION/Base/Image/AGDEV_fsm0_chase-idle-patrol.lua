defaultstate="patrol"

transitioncount=4

t0={
	currentstate="patrol",
	nextstate="chase",
	condition="nearplayer"
}
t1={
	currentstate="patrol",
	nextstate="idle",
	condition="twentyseconds"
}
t2={
	currentstate="idle",
	nextstate="chase",
	condition="nearplayer"
}
t3={
	currentstate="chase",
	nextstate="patrol",
	condition="awayplayer"
}
