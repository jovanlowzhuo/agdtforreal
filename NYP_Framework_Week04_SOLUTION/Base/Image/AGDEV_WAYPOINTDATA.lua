totalnodes=12
totaledges=19

nodes={
	n0 = {0,0,0},
	n1 = {150,0,150},
	n2 = {150,0,-150}, 
	n3 = {-150,0,150},
	n4 = {-150,0,-150},
	n5 = {350,0,350},
	n6 = {350,0,-350},
	n7 = {-350,0,350},
	n8 = {-350,0,-350}, 
	n9 = {-350,0,0},
	n10 = {0,0,350},
	n11 = {0,0,-350}
}

edges={
	e0 = {0,1,999},
	e1 = {0,2,999},
	e2 = {0,3,999},
	e3 = {0,4,999},
	e4 = {1,2,999},
	e5 = {1,3,999},
	e6 = {1,5,999},
	e7 = {2,4,999},
	e8 = {2,6,999},
	e9 = {3,4,999},
	e10 = {3,7,999},
	e11 = {4,8,999},
	e12 = {5,6,999},
	e13 = {5,10,999},
	e14 = {6,11,999},
	e15 = {7,9,999},
	e16 = {7,10,999},
	e17 = {8,9,999},
	e18 = {8,11,999}
}

presets = {
	p0 = {0,1,999},
	p1 = {1,2,999},
	p2 = {2,4,999},
	p3 = {4,3,999},
	p4 = {3,7,999},
	p5 = {7,9,999},
	p6 = {9,8,999},
	p7 = {8,11,999},
	p8 = {11,6,999},
	p9 = {6,5,999},
	p10 = {5,1,999},
	p11 = {10,5,999}
}

function CalculateDistanceSquare(x1,y1,z1,x2,y2,z2)
	local distanceSquare = (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1)
	return distanceSquare
end

function WaypointCheck (e, __x, __y, __z)

local actualX;
local actualY;
local actualZ;

local tempX;
local tempY;
local tempZ;

local _nearestDist = 999999;
local _placeholderDist = 999999;

for i, v in next, e do
    for n, k in next, v do
        if (n == 1) then
		tempX = k
		elseif (n == 2) then
		tempY = k	
		elseif (n == 3) then
		tempZ = k
		end
    end
	_placeholderDist = CalculateDistanceSquare(__x, __y, __z, tempX, tempY, tempZ)
	
	if (_placeholderDist < _nearestDist) then
	_nearestDist = _placeholderDist
	actualX = tempX
	actualY = tempY
	actualZ = tempZ
	end
	
end

return actualX, actualY, actualZ
end

function LuaGetNearestWaypoint(_x, _y, _z)
	local x;
	local y;
	local z;
		x, y, z = WaypointCheck(nodes, _x, _y, _z)
		
	return string.format("%d | %i | %i", x, y, z) 
end
