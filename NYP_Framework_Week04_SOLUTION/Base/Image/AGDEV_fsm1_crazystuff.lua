defaultstate="patrol"

transitioncount=5

t0={
	currentstate="patrol",
	nextstate="chase",
	condition="onesecond"
}
t1={
	currentstate="chase",
	nextstate="idle",
	condition="onesecond"
}
t2={
	currentstate="idle",
	nextstate="escape",
	condition="onesecond"
}
t3={
	currentstate="escape",
	nextstate="pathing",
	condition="onesecond"
}
t4={
	currentstate="pathing",
	nextstate="patrol",
	condition="onesecond"
}

