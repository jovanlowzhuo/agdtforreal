Crosshair="crosshair"

text={
	Name="text",
	row=16,
	column=16, 
	Texture="Image//calibri.tga",
	kAmbient={
		1.0,
		0.0,
		0.0
	}
}

quad={
	Name="quad",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//calibri.tga"
}

bluebox={
Name="bluebox", 
	Color={
		0.0,
		0.0,
		0.5
	}, 
	length=1.0
}

menuscreen={
	Name="menuscreen", 
	Color={
		1.0,
		1.0,
		1.0
	}, 
	length=1.0, 
	Texture="Image//screen.tga"
}

splashscreen={
	Name="splashscreen", 
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//splash.tga"
}

button_notpressed={
	Name="button_notpressed", 
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//button_notpressed.tga"
}

button_pressed={
	Name="button_pressed", 
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//button_pressed.tga"
}

GRASS_DARKGREEN={
	Name="GRASS_DARKGREEN", 
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//grass_darkgreen.tga"
}

GEO_GRASS_LIGHTGREEN={
	Name="GEO_GRASS_LIGHTGREEN",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//grass_lightgreen.tga"
}

SKYBOX_FRONT={
	Name="SKYBOX_FRONT",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_ft.tga"
}

SKYBOX_BACK={
	Name="SKYBOX_BACK",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_bk.tga"
}

SKYBOX_LEFT={
	Name="SKYBOX_LEFT",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_lf.tga"
}

SKYBOX_RIGHT={
	Name="SKYBOX_RIGHT",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_rt.tga"
}

SKYBOX_TOP={
	Name="SKYBOX_TOP",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_up.tga"
}

SKYBOX_BOTTOM={
	Name="SKYBOX_BOTTOM",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//SkyBox//darkcity_dn.tga"
}

Floor_HighLOD={
	Name="Floor_HighLOD",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//floorHighLOD.tga"
}

Floor_MidLOD={
	Name="Floor_MidLOD",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//floorMidLOD.tga"
}

Floor_LowLOD={
	Name="Floor_LowLOD",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//floorLowLOD.tga"
}

EMPTYMESH={
	Name="EMPTYMESH",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//basetile.tga"
}

GRIDMESH={
	Name="GRIDMESH",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//tile.tga"
}

ExploIndi={
	Name="ExploIndi",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//exploIndicator.tga"
}

ExploRad={
	Name="exploRad",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//exploRing.tga"
}

ProjTile={
	Name="ProjTile",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//projIndicator.tga"
}

EXPLOMESH={
	Name="EXPLOMESH",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//tileExploded.tga"
}

CAMERAEFFECTS_BLOODSCREEN={
	Name="CAMERAEFFECTS_BLOODSCREEN",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//CameraEffects_Blood.tga"
}

CAMERAEFFECTS_SCOPESCREEN={
	Name="CAMERAEFFECTS_SCOPESCREEN",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//scope.tga"
}

MINIMAP={
	Name="MINIMAP",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//minimap_square.tga"
}

MINIMAPAVATAR={
	Name="MINIMAPAVATAR",
	Color={
		1.0,
		1.0,
		0.0
	},
	length=1.0,
	Texture="Image//Avatar.tga"
}

money={
	Name="money",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//currency.tga"
}

closebutton={
	Name="closebutton",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//stopsign.tga"
}

scanner={
	Name="scanner",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//scanner.tga"
}

robot={
	Name="robot",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//robot.tga"
}

omega={
	Name="omega",
	Color={
		1.0,
		1.0,
		1.0
	},
	length=1.0,
	Texture="Image//omega.tga"
}





















