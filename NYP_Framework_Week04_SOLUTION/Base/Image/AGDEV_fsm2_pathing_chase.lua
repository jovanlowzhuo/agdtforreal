defaultstate="pathing"

transitioncount=2

t0={
	currentstate="pathing",
	nextstate="chase",
	condition="nearplayer"
}
t1={
	currentstate="chase",
	nextstate="pathing",
	condition="awayplayer"
}