--WindowInfo
WindowInfo = {
	title = "DM2240 - Week 14 Scripting",
	size = {
		width = 800,
		height = 600
	},
	position = {
		x = 100,
		y = 100
	}
}

--PlayerInfo
PlayerInfo = {
	position = {
		x = 32.5, y = 20.0
	},
	direction = {0, 10.0, 20.0},
	callsign = "DaNoob",
	HP = 300
}

-- Array
Array = {1, 2, 3, 4, 5}

-- HighScore
HighScore = { 
	["ABC"] = 100, 
	["DEF"] = 80, 
	["GHI"] = 60, 
	["JKL"] = 40 
}

-- Volume Control
volumeLevel = 100
