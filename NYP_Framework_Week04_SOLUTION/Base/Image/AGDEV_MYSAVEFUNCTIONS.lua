function SaveShopInfo(outputString, overwrite)
   print("SaveShopInfo...")
   local f;						-- The file
   if overwrite == 1 then		-- Wipe the contents with new data
      f = assert(io.open("Image/AGDEV_SHOPDATA_VARIABLES.lua", "w"))
   elseif overwrite == 0 then	-- Append with new data
      f = assert(io.open("Image/AGDEV_SHOPDATA_VARIABLES.lua", "a"))
   end
   -- Write to the file
   f:write(outputString)
   -- Close the file
   f:close()
   print("OK")
end


